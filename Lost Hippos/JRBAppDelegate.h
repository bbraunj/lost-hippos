//
//  JRBAppDelegate.h
//  Lost Hippos
//
//  Created by Josh Braun on 7/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JRBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
