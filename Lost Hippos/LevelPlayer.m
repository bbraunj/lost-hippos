//
//  LevelPlayer.m
//  Lost Hippos
//
//  Created by Josh Braun on 11/15/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "LevelPlayer.h"
#import "SKSpriteNodeButton.h"
#import "MainMenu.h"
#import "LevelInfoStore.h"
#import "CGPointExtension.h"
#import "LevelCreator.h"
#import "BMGlyphLabel.h"
#import "BMGlyphFont.h"
#import "PhysicsCategories.h"

@implementation LevelPlayer
@synthesize levelCreator;

+(LevelPlayer *)sceneWithSize:(CGSize)size creatorMode:(BOOL)creatorMode scaleLevel:(float)scale littleHippos:(NSArray *)littleHippos mamaHippo:(SKSpriteNode *)mama stationaryObjects:(NSArray *)stationary moveableObjects:(NSArray *)moveable dropObjects:(NSArray *)dropObjects backgrounds:(NSArray *)bgs
{
    LevelPlayer *playerScene = [[LevelPlayer alloc] initWithSize: size creatorMode: creatorMode scaleLevel: scale littleHippos: littleHippos mamaHippo: mama stationaryObjects: stationary moveableObjects: moveable dropObjects: dropObjects backgrounds: bgs];
    return playerScene;
}

-(id)initWithSize:(CGSize)size creatorMode:(BOOL)creator scaleLevel:(float)scale littleHippos:(NSArray *)little mamaHippo:(SKSpriteNode *)mama stationaryObjects:(NSArray *)stationary moveableObjects:(NSArray *)moveable dropObjects:(NSMutableArray *)drop backgrounds:(NSArray *)bgs
{
    self = [super initWithSize: size];
    if (self) {
        // Initialize everything
        winSize = self.frame.size;
        creatorMode = creator;
        scaleLevel = scale;
        littleHippos = little;
        mamaHippo = mama;
        moveableObjects = moveable;
        stationaryObjects = stationary;
        dropObjects = drop;
        backgrounds = bgs;
        droppedObjects = [[NSMutableArray alloc] init];

        // Have the app load the right atlas
        atlas = [SKTextureAtlas atlasNamed: @"Images"];
        
        canLittleHippoRotate = YES;
        levelComplete = NO;
        canLetObjectGo = YES;
        objectLetGo = NO;

        maxDistance = 85 * scaleLevel;
        if (IS_IPAD)
            maxDistance = 150 * scaleLevel;
        if (littleHippos.count > 1)
            maxDistance = maxDistance * 0.75 * littleHippos.count;

        // Make the ground physicsBody
        int groundY = 3;
        if (IS_IPAD)
            groundY = 10;
        [self setPhysicsBody: [SKPhysicsBody bodyWithEdgeFromPoint: ccp(-winSize.width, groundY) toPoint: ccp(winSize.width*2, groundY)]];
        [self.physicsBody setRestitution: 0];

        // Score
        int fontSize = 20;
        int yPosition = winSize.height - winSize.height * 0.075;
        if (IS_IPAD) {
            fontSize = 40;
            yPosition = winSize.height - winSize.height * 0.06;
        }
        
        SKLabelNode *score = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [score setName: @"Score"];
        [score setFontColor: [UIColor whiteColor]];
        [score setFontSize: fontSize];
        [score setText: @"Score:"];
        [score setAlpha: 0.9];
        [score setPosition: ccp(winSize.width * 0.06, yPosition)];
        [score setZPosition: 1];
        [self addChild: score];
        
        scoreLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [scoreLabel setFontColor: [UIColor whiteColor]];
        [scoreLabel setFontSize: fontSize];
        [scoreLabel setText: @"500"];
        [scoreLabel setAlpha: 0.9];
        [scoreLabel setPosition: ccp(winSize.width * 0.14, yPosition)];
        [scoreLabel setZPosition: 1];
        [self addChild: scoreLabel];
        
        // Start counting down the score
        [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
        
        
        // Buttons
        pauseButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PauseButton.png"];
        [pauseButton setPosition: ccp(winSize.width - 18.5, winSize.height - 20)];
        if (IS_IPAD) [pauseButton setPosition: ccp(winSize.width - 35, winSize.height - 40)];
        [pauseButton setZPosition: 1];
        [pauseButton addTarget: self action: @selector(pauseButtonPressed)];
        [self addChild: pauseButton];
        
        restartButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartButton.png"];
        [restartButton setPosition: ccp(winSize.width - 20, winSize.height - 60)];
        if (IS_IPAD) [restartButton setPosition: ccp(winSize.width - 37.5, winSize.height - 120)];
        [restartButton setZPosition: 1];
        [restartButton addTarget: self action: @selector(restartButtonPressed)];
        [self addChild: restartButton];

        
        // Pause Menu
        dimmer = [[SKShapeNode alloc] init];
        [dimmer setPath: CGPathCreateWithRect(self.frame, NULL)];
        [dimmer setFillColor: [UIColor blackColor]];
        [dimmer setStrokeColor: [UIColor clearColor]];
        [dimmer setAlpha: 0];
        [dimmer setZPosition: -1];
        [self addChild: dimmer];
        
        pauseMenu = [SKSpriteNode spriteNodeWithImageNamed: @"PauseMenu.png"];
        [pauseMenu setPosition: ccp(winSize.width/2, winSize.height/2)];
        [self addChild: pauseMenu];
        
        playButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PausePlayButton.png"];
        [playButton setPosition: ccp(-pauseMenu.frame.size.width * 0.26, pauseMenu.frame.size.height * 0.15)];
        [playButton addTarget: self action: @selector(playButtonPressed)];
        [pauseMenu addChild: playButton];
        
        restartPauseButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartLevelButton.png"];
        [restartPauseButton setPosition: ccp(0, pauseMenu.frame.size.height * 0.15)];
        [restartPauseButton addTarget: self action: @selector(restartButtonPressed)];
        [pauseMenu addChild: restartPauseButton];
        
        // SoundButton
        if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"enabled"])
            soundButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SoundOnButton.png"];
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"disabled"])
            soundButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SoundOffButton.png"];

        [soundButton setPosition: ccp(0, -pauseMenu.frame.size.height * 0.23)];
        [soundButton addTarget: self action: @selector(soundButtonPressed)];
        [pauseMenu addChild: soundButton];
        
        if (!creatorMode) {
            levelSelectButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelSelectButton.png"];
            [levelSelectButton addTarget: self action: @selector(levelSelectButtonPressed)];
        } else {
            levelSelectButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelCreatorPauseButton.png"];
            [levelSelectButton addTarget: self action: @selector(levelCreatorButtonPressed)];
        }
        [levelSelectButton setPosition: ccp(pauseMenu.frame.size.width * 0.3, pauseMenu.frame.size.height * 0.15)];
        [pauseMenu addChild: levelSelectButton];
        
        [pauseMenu setScale: 0];

        
        // Score Menu
        scoreMenu = [SKSpriteNode spriteNodeWithImageNamed: @"ScoreMenu.png"];
        [scoreMenu setPosition: ccp(winSize.width/2, winSize.height/2)];
        [scoreMenu setZPosition: -10];
        [self addChild: scoreMenu];
        
        // Buttons
        SKSpriteNodeButton *restartLevelButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartLevelScoreButton.png"];
        [restartLevelButton setPosition: ccp(-scoreMenu.frame.size.width * 0.25, -scoreMenu.frame.size.height * 0.2)];
        if (creatorMode) [restartLevelButton setPosition: ccp(-scoreMenu.frame.size.width * 0.25, -scoreMenu.frame.size.height * 0.3)];
        [restartLevelButton addTarget: self action: @selector(restartButtonPressed)];
        [scoreMenu addChild: restartLevelButton];
        
        SKSpriteNodeButton *nextLevelButton;
        if (!creatorMode) {
            nextLevelButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"NextLevelButton.png"];
            [nextLevelButton addTarget: self action: @selector(nextLevelButtonPressed)];
            [nextLevelButton setPosition: ccp(scoreMenu.frame.size.width * 0.27, -scoreMenu.frame.size.height * 0.2)];
        }
        else {
            nextLevelButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelCreatorPauseButton.png"];
            [nextLevelButton addTarget: self action: @selector(levelCreatorButtonPressed)];
            [nextLevelButton setPosition: ccp(scoreMenu.frame.size.width * 0.27, -scoreMenu.frame.size.height * 0.3)];
        }
        [scoreMenu addChild: nextLevelButton];
        
        if (!creatorMode) {
            SKSpriteNodeButton *levelSelectScoreButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelSelectScoreButton.png"];
            [levelSelectScoreButton setPosition: ccp(0, -scoreMenu.frame.size.height * 0.35)];
            [levelSelectScoreButton addTarget: self action: @selector(levelSelectButtonPressed)];
            [scoreMenu addChild: levelSelectScoreButton];
        }

        
        // Drop zone
        dropZone = [SKSpriteNode spriteNodeWithImageNamed: @"DropZone.png"];
        [dropZone setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.175)];
        [dropZone setZPosition: -1.5];
        [self addChild: dropZone];
        
        
        // Add the backgrounds to the scene
        for (SKSpriteNode *background in backgrounds)
            [self addChild: background];
        
        // Add the mama hippo and its joints to the scene
        [self addChild: mamaHippo];

        SKSpriteNode *body = (SKSpriteNode *)[mamaHippo childNodeWithName: @"body"];
        SKSpriteNode *leg1 = (SKSpriteNode *)[mamaHippo childNodeWithName: @"leg1"];
        SKSpriteNode *leg2 = (SKSpriteNode *)[mamaHippo childNodeWithName: @"leg2"];
        
        // Give the body and legs some denisty (automatically adjusts mass)
        [body.physicsBody setDensity: 2];
        [leg1.physicsBody setDensity: 2];
        [leg2.physicsBody setDensity: 2];
        
        SKPhysicsJointSliding *leg1Joint = [SKPhysicsJointSliding jointWithBodyA: leg1.physicsBody bodyB: body.physicsBody anchor: ccp(leg1.position.x, leg1.position.y - 4.5 * scaleLevel) axis: CGVectorMake(0, 1)];
        [leg1Joint setShouldEnableLimits: YES];
        [leg1Joint setUpperDistanceLimit: 2.25 * scaleLevel];
        [leg1Joint setLowerDistanceLimit: -(2.25 * scaleLevel)];
        [self.physicsWorld addJoint: leg1Joint];
        SKPhysicsJointSliding *leg2Joint = [SKPhysicsJointSliding jointWithBodyA: leg2.physicsBody bodyB: body.physicsBody anchor: ccp(leg2.position.x, leg2.position.y - 4.5 * scaleLevel) axis: CGVectorMake(0, 1)];
        [leg2Joint setShouldEnableLimits: YES];
        [leg2Joint setUpperDistanceLimit: 2.25 * scaleLevel];
        [leg2Joint setLowerDistanceLimit: -(2.25 * scaleLevel)];
        [self.physicsWorld addJoint: leg2Joint];

        // Add the little hippos to the scene
        for (SKSpriteNode *hippo in littleHippos) {
            [hippo setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: NO] forKey: @"canRotate"]];
            [self addChild: hippo];
        }
        
        // Add the moveable objects to the scene
        for (SKSpriteNode *obj in moveableObjects)
            [self addChild: obj];
        
        // Add the stationary objects to the scene
        for (SKSpriteNode *obj in stationaryObjects)
            [self addChild: obj];
        
        
        // Add the drop objects
        CGSize containerSize = CGSizeMake(148, 48);
        if (IS_IPAD) containerSize = CGSizeMake(187, 60);
        dropQueue = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed: @"Transparency"] size: containerSize];
        [dropQueue setPosition: ccp(winSize.width * 0.1, winSize.height - 40)];
        if (IS_IPAD) [dropQueue setPosition: ccp(winSize.width * 0.1, winSize.height - 60)];
        [self addChild: dropQueue];
        
        [self updateDropQueue];
    }
    return self;
}

-(void)updateDropQueue
{
    // Remove all drop objects from their parent
    for (SKNode *obj in dropObjects) {
        [obj removeFromParent];
        [obj removeAllActions];
        for (SKNode *child in obj.children)
            [child removeAllActions];
    }
    for (SKNode *obj in dropQueue.children)
        [obj removeFromParent];
    
    if (dropObjects.count > 0) {
        SKSpriteNode *firstDropObject = (SKSpriteNode *)[dropObjects firstObject];
        [firstDropObject setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.2)];
        [firstDropObject setScale: scaleLevel];
        [firstDropObject setAlpha: 0.6];
        [self addChild: firstDropObject];
        
        for (SKNode *child in firstDropObject.children)
            [child setAlpha: 0.6];
        
        // Fade the alpha of the drop object
        SKAction *repeat = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction fadeAlphaTo: 0.8 duration: 1], [SKAction fadeAlphaTo: 0.6 duration: 1]]]];
        [firstDropObject runAction: repeat];
        
        for (SKNode *child in firstDropObject.children)
            [child runAction: repeat];
    }
    
    // Move back the objects already in the drop queue
    if (dropObjects.count > 1) {
        for (int i = (int)(dropObjects.count - (dropObjects.count - 1)); i < dropObjects.count; i++) {
            SKSpriteNode *obj = [dropObjects objectAtIndex: i];
            [obj removeAllActions];
            [obj setAlpha: 0.5];
            [obj setScale: 0.2];
            if (IS_IPAD)
                [obj setScale: 0.15];
            [dropQueue addChild: obj];
            if (i == dropObjects.count - (dropObjects.count - 1))
                [obj setPosition: ccp(dropQueue.frame.size.width * 0.25, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 2))
                [obj setPosition: ccp(dropQueue.frame.size.width * 0.0, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 3))
                [obj setPosition: ccp(-dropQueue.frame.size.width * 0.25, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 4)) {
                // Doesn't fit. remove it from the queue and add
                // a '...' on the left side of the queue
                [obj removeFromParent];
                
                if (![dropQueue childNodeWithName: @"dotDotDot"]) {
                    SKLabelNode *dotDotDot = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
                    [dotDotDot setName: @"dotDotDot"];
                    [dotDotDot setFontColor: [UIColor whiteColor]];
                    [dotDotDot setFontSize: 10];
                    if (IS_IPAD)
                        [dotDotDot setFontSize: 10];
                    [dotDotDot setText: @"..."];
                    [dotDotDot setPosition: ccp(-dropQueue.frame.size.width * 0.395, -dropQueue.frame.size.height * 0.18)];
                    [dropQueue addChild: dotDotDot];
                }
            } else
                [obj removeFromParent];
        }
    }
}

-(void)didMoveToView:(SKView *)view
{
    [self startAnimatingHippos];
}

- (float)randomTimeBetween:(float)minimum and:(float)maximum
{
    // Random time between minimum and maximum time in seconds
    float diff = maximum - minimum;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + minimum;
}

#pragma mark - Hippo Animations
-(void)startAnimatingHippos
{
    // Moves the body up and down 4.5 pts (8.1 for iPad)
    float movement = 4.5;
    if (IS_IPAD) movement = 8.1;
    SKAction *moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -(movement * scaleLevel) duration: 0.2], [SKAction moveByX: 0 y: movement * scaleLevel duration: 0.2]]]];
    
    // Same goes for the head, but give it
    // some extra motion for a more 3d effect
    float headMovement = 6;
    if (IS_IPAD) headMovement = 10.8;
    SKAction *headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: headMovement * scaleLevel duration: 0.2], [SKAction moveByX: 0 y: -(headMovement * scaleLevel) duration: 0.2]]]];
    
    
    // Animate the mama hippo
    [[mamaHippo childNodeWithName: @"body"] runAction: moveBody];
    [[[mamaHippo childNodeWithName: @"body"] childNodeWithName: @"head"] runAction: headMotion withKey: @"headMotion"];
    
    // Make the mama blink
    SKSpriteNode *mamaHead = (SKSpriteNode *)[[mamaHippo childNodeWithName: @"body"] childNodeWithName: @"head"];
    
    NSArray *blinkTextures = [NSArray arrayWithObjects: mamaHead.texture, [mamaHead.userData objectForKey: @"blinkTexture"], mamaHead.texture, nil];
    SKAction *blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    SKAction *repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 1.5], blink]];
    [mamaHead runAction: [SKAction repeatActionForever: repeatBlink]];
    
    
    // Make the littleHippos blink
    for (SKSpriteNode *littleHippo in littleHippos) {
        SKSpriteNode *littleHead = (SKSpriteNode *)[[littleHippo childNodeWithName: @"body"] childNodeWithName: @"head"];
        
        NSArray *blinkTextures = [NSArray arrayWithObjects: littleHead.texture, [littleHead.userData objectForKey: @"blinkTexture"], littleHead.texture, nil];
        SKAction *blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
        SKAction *repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 3], blink]];
        [littleHead runAction: [SKAction repeatActionForever: repeatBlink]];
    }
}

#pragma mark - Buttons
-(void)pauseButtonPressed
{
    // Pause both the score countdown and the
    // littleHippo in range countdown
    [NSObject cancelPreviousPerformRequestsWithTarget: self];
    
    // Pause the mamaHippo's bouncing and blinking
    [mamaHippo setPaused: YES];
    
    // Pause littleHippos' blinking
    for (SKSpriteNode *hippo in littleHippos)
        [hippo setPaused: YES];
    
    // Pause the physics simulations
    [[self physicsWorld] setSpeed: 0];
    
    // Bring the dimmer and pauseMenu to the front
    [dimmer setZPosition: 1.9];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.2]];
    [pauseMenu setZPosition: 2];
    [pauseMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.15], [SKAction scaleTo: 1 duration: 0.1]]]];
    
    // All touches for gameplay are now ignored
    firstTouch = [[UITouch alloc] init];
    
    // Hide the pause/restart buttons on the scene
    [pauseButton setAlpha: 0];
    [restartButton setAlpha: 0];
}

-(void)restartButtonPressed
{
    if (!creatorMode) {
        
    } else if (levelCreator){
        // Tell our level creator to restart the player
        [levelCreator playLevel];
    }
}

#pragma mark - Pause Menu Buttons
-(void)playButtonPressed
{
    // Unpause the score countdown
    [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
    
    // Unpause the littleHippo in range countdown if it's on
    if ([self childNodeWithName: @"numberLabel"]) {
        SKLabelNode *counterLabel = (SKLabelNode *)[self childNodeWithName: @"numberLabel"];
        int countDownNumber = [[counterLabel text] intValue];
        [self countDown: [NSNumber numberWithInt: countDownNumber]];
    }
    
    
    // Unpause the mamaHippo's bouncing and blinking
    [mamaHippo setPaused: NO];
    
    // Unpause littleHippos' blinking
    for (SKSpriteNode *hippo in littleHippos)
        [hippo setPaused: NO];
    
    // Pause the physics simulations
    [[self physicsWorld] setSpeed: 1];
    
    // Fade out the dimmer and scale down the pause menu
    [dimmer runAction: [SKAction fadeAlphaTo: 0 duration: 0.3] completion: ^{
        [dimmer setZPosition: -1];
    }];
    [pauseMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.1], [SKAction scaleTo: 0 duration: 0.15]]] completion: ^{
        [pauseMenu setZPosition: -1];
    }];
    
    // All touches for gameplay are now recognized
    firstTouch = nil;
    
    // Hide the pause/restart buttons on the scene
    [pauseButton setAlpha: 1];
    [restartButton setAlpha: 1];
}

-(void)levelSelectButtonPressed
{
    // Create the scene and navigate to the levelPage of this level
    MainMenu *mainMenu = [MainMenu sceneWithSize: winSize];
    [mainMenu setScaleMode: SKSceneScaleModeAspectFit];
    [mainMenu playButtonPressed];
    [mainMenu levelPackButtonPressed: mainMenu.levels1_20];
    [self.view presentScene: mainMenu transition: [SKTransition fadeWithDuration: 0.8]];
}

-(void)levelCreatorButtonPressed
{
    [self.view presentScene: (SKScene *)levelCreator transition: [SKTransition fadeWithDuration: 0.8]];
    [self setLevelCreator: nil];
}

-(void)soundButtonPressed
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"enabled"]) {
        [soundButton setTexture: [SKTexture textureWithImageNamed: @"SoundOffButton.png"]];
        [[NSUserDefaults standardUserDefaults] setObject: @"disabled" forKey: @"soundEnabled"];
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"disabled"]) {
        [soundButton setTexture: [SKTexture textureWithImageNamed: @"SoundOnButton.png"]];
        [[NSUserDefaults standardUserDefaults] setObject: @"enabled" forKey: @"soundEnabled"];
    }
}


#pragma mark - The Game
-(void)countScoreDown
{
    int newScore = [[scoreLabel text] intValue] - 1;
    if (newScore > 0) {
        [scoreLabel setText: [NSString stringWithFormat: @"%i", newScore]];
        [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
    } else if (newScore == 0)
        [self restartButtonPressed];
    // Restart the level if we reach 0
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!firstTouch) {
        firstTouch = [touches anyObject];
        
        // Only start if the touch is in the drop zone
        if (dropObjects.count > 0 && [dropZone containsPoint: [firstTouch locationInNode: self]]) {
            SKSpriteNode *firstObject = dropObjects.firstObject;
            [firstObject removeAllActions];
            [firstObject setPosition: [firstTouch locationInNode: self]];
            
            // Check if the boulder intersects any scene objects
            for (SKSpriteNode *hippo in littleHippos) {
                if ([hippo intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }
            
            for (SKSpriteNode *obj in moveableObjects) {
                if ([obj intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }

            for (SKSpriteNode *obj in stationaryObjects) {
                if ([obj intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }

            if ([mamaHippo intersectsNode: firstObject])
                canLetObjectGo = NO;
            else
                canLetObjectGo = YES;
            
            
            // Change the color based on whether the drop object can be let go
            if (canLetObjectGo) {
                [firstObject setAlpha: 1];
                [firstObject setColorBlendFactor: 0];
            } else {
                [firstObject setAlpha: 0.7];
                [firstObject setColor: [UIColor redColor]];
                [firstObject setColorBlendFactor: 1];
            }
        } else // Touch isn't in the dropZone
            canLetObjectGo = NO;
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch && dropObjects.count > 0 && [dropZone containsPoint: [firstTouch locationInNode: self]]) {
            SKSpriteNode *firstObject = dropObjects.firstObject;
            [firstObject removeAllActions];
            [firstObject setPosition: [firstTouch locationInNode: self]];
            
            // Check if the boulder intersects any scene objects
            for (SKSpriteNode *hippo in littleHippos) {
                if ([hippo intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }
            
            for (SKSpriteNode *obj in moveableObjects) {
                if ([obj intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }
            
            for (SKSpriteNode *obj in stationaryObjects) {
                if ([obj intersectsNode: firstObject])
                    canLetObjectGo = NO;
                else
                    canLetObjectGo = YES;
            }
            
            if ([mamaHippo intersectsNode: firstObject])
                canLetObjectGo = NO;
            else
                canLetObjectGo = YES;
            
            
            // Change the color based on whether the drop object can be let go
            if (canLetObjectGo) {
                [firstObject setAlpha: 1];
                [firstObject setColorBlendFactor: 0];
            } else {
                [firstObject setAlpha: 0.7];
                [firstObject setColor: [UIColor redColor]];
                [firstObject setColorBlendFactor: 1];
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch) {
            firstTouch = nil;
            
            // If we can and haven't yet let the boulder go
            if (dropObjects.count > 0 && canLetObjectGo) {
                SKSpriteNode *firstDropObject = dropObjects.firstObject;
                
                // Physics body
                if (![firstDropObject.name isEqual: @"boulder"]) {
                    [firstDropObject setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: firstDropObject.frame.size]];
                    [firstDropObject.physicsBody setDensity: 1];
                } else {
                    [firstDropObject setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: firstDropObject.frame.size.width/2]];
                    [firstDropObject.physicsBody setDensity: 3];
                }
                [firstDropObject.physicsBody setDynamic: YES];
                [firstDropObject.physicsBody setCategoryBitMask: DropObjectCategory];
                [firstDropObject.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | DropObjectCategory | LegCategory];
                [firstDropObject.physicsBody setLinearDamping: 1];
                
                // Move the object to the droppedObjects array and update the drop queue
                [dropObjects removeObject: firstDropObject];
                [droppedObjects addObject: firstDropObject];
                [self updateDropQueue];
                canLetObjectGo = NO;
            }
        }
    }
}


-(void)countDown:(NSNumber *)number
{
    // If the number's over 0
    if ([number intValue] > 0) {
        // Remove any old numberLabels
        if ([self childNodeWithName: @"numberLabel"])
            [[self childNodeWithName: @"numberLabel"] removeFromParent];
        
        // Show the numberLabel
        int fontSize = 120;
        if (IS_IPAD)
            fontSize = 240;
        
        SKLabelNode *numberLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [numberLabel setName: @"numberLabel"];
        [numberLabel setText: [NSString stringWithFormat: @"%i", [number intValue]]];
        [numberLabel setFontColor: [UIColor whiteColor]];
        [numberLabel setFontSize: fontSize];
        [numberLabel setPosition: ccp(winSize.width/2, winSize.height/2)];
        [numberLabel setAlpha: 0];
        [self addChild: numberLabel];
        
        // Fade in/scale down the number
        [numberLabel runAction: [SKAction group: @[[SKAction scaleTo: 0.5 duration: 0.6], [SKAction fadeAlphaTo: 1 duration: 0.7]]]];
        [self performSelector: @selector(countDown:) withObject: [NSNumber numberWithInt: [number intValue] - 1] afterDelay: 1];
    } else if ([number intValue] == 0) {
        // Remove any old numberLabels
        if ([self childNodeWithName: @"numberLabel"])
            [[self childNodeWithName: @"numberLabel"] removeFromParent];
        
        [self showScoreMenu];
    }
}

-(void)update:(NSTimeInterval)currentTime
{
    // Check if all the little hippos are within the max distance of the mama
    numberOfLittleHipposWithinDistance = 0;
    for (SKSpriteNode *hippo in littleHippos) {
        CGPoint littleHippoPosition = [hippo convertPoint: [hippo childNodeWithName: @"body"].position toNode: self];
        CGPoint mamaHippoPosition = [mamaHippo convertPoint: [mamaHippo childNodeWithName: @"body"].position toNode: self];
        
        if (ccpDistance(littleHippoPosition, mamaHippoPosition) < maxDistance)
            numberOfLittleHipposWithinDistance++;
    }
    
    if (numberOfLittleHipposWithinDistance == littleHippos.count && ![self childNodeWithName: @"numberLabel"] && !levelComplete) {
        // All littleHippos are in range. Start counting down
        [self countDown: [NSNumber numberWithInt: 5]];
        for (SKNode *node in littleHippos)
            [node.userData setObject: [NSNumber numberWithBool: YES] forKey: @"canRotate"];
    }
    else if (numberOfLittleHipposWithinDistance != littleHippos.count && [self childNodeWithName: @"numberLabel"]) {
        // LittleHippo too far from mamaHippo, so stop counting down and remove the label
        NSNumber *number = [NSNumber numberWithInt: [[(SKLabelNode *)[self childNodeWithName: @"numberLabel"] text] intValue] - 1];
        [NSObject cancelPreviousPerformRequestsWithTarget: self selector: @selector(countDown:) object: number];
        [[self childNodeWithName: @"numberLabel"] removeFromParent];
        for (SKNode *node in littleHippos)
            [node.userData setObject: [NSNumber numberWithBool: NO] forKey: @"canRotate"];
    }
    
    
    // If the littleHippo's not moving much and all littleHippos are near
    // the mamaHippo, then rotate it and make it bounce
    for (SKSpriteNode *hippo in littleHippos) {
        if (hippo.physicsBody.angularVelocity < 1.5 && hippo.physicsBody.angularVelocity > -1.5 && [[hippo.userData objectForKey: @"canRotate"] boolValue]) {
            [hippo.userData setObject: [NSNumber numberWithBool: NO] forKey: @"canRotate"];
            [hippo runAction: [SKAction waitForDuration: 2] completion: ^{
                if (hippo.physicsBody.angularVelocity < 1.5 && hippo.physicsBody.angularVelocity > -1.5 && numberOfLittleHipposWithinDistance == littleHippos.count) {
                    SKSpriteNode *body = (SKSpriteNode *)[hippo childNodeWithName: @"body"];
                    
                    // Give the littleHippo a nonDynamic body
                    SKPhysicsBody *physicsBody = [SKPhysicsBody bodyWithCircleOfRadius: hippo.frame.size.width/2 - hippo.frame.size.width * 0.15];
                    [physicsBody setDynamic: NO];
                    [physicsBody setCategoryBitMask: LittleHippoCategory];
                    [physicsBody setCollisionBitMask: ObjectCategory];
                    [body setPhysicsBody: physicsBody];
                    
                    // Less movement for the little hippo
                    float bodyMovement = 2 * scaleLevel;
                    if (IS_IPAD) bodyMovement = 3.6 * scaleLevel;
                    float headMovement = 3 * scaleLevel;
                    if (IS_IPAD) headMovement = 5.4 * scaleLevel;
                    SKAction *moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -bodyMovement duration: 0.16], [SKAction moveByX: 0 y: bodyMovement duration: 0.16]]]];
                    SKAction *legsSamePosition = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: bodyMovement duration: 0.16], [SKAction moveByX: 0 y: -bodyMovement duration: 0.16]]]];
                    SKAction *headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: headMovement duration: 0.16], [SKAction moveByX: 0 y: -headMovement duration: 0.16]]]];
                    
                    // The y position we want the littleHippo to move by
                    float littleHippoY = 3.4 * scaleLevel;
                    // iphone ratio
                    if (IS_IPAD)
                        littleHippoY = 7.8 * scaleLevel;
                    // ipad ratio
                    
                    [body runAction: [SKAction group: @[[SKAction rotateToAngle: 0 duration: 0.3 shortestUnitArc: YES], [SKAction moveByX: 0 y: littleHippoY duration: 0.3]]] completion: ^{
                        // Move the littleHippo
                        [body runAction: moveBody withKey: @"moveBody"];
                        [[body childNodeWithName: @"leg1"] runAction: legsSamePosition withKey: @"samePosition"];
                        [[body childNodeWithName: @"leg2"] runAction: legsSamePosition withKey: @"samePosition"];
                        [[body childNodeWithName: @"head"] runAction: headMotion withKey: @"headMotion"];
                    }];
                    
                    // Move the legs out
                    float legSpacingVertical = body.frame.size.height * 0.08;
                    [[body childNodeWithName: @"leg1"] runAction: [SKAction moveByX: 0 y: -legSpacingVertical duration: 0.3]];
                    [[body childNodeWithName: @"leg2"] runAction: [SKAction moveByX: 0 y: -legSpacingVertical duration: 0.3]];
                }
            }];
        }
    }
}

#pragma mark - Level Completed
-(void)showScoreMenu
{
    levelComplete = YES;
    BOOL newHighScore = NO;
    
    // Save the score and furthestLevelReached
    if (!creatorMode) {
        if ([[LevelInfoStore sharedStore] scoreForLevel: 1] < [scoreLabel.text intValue]) {
            newHighScore = YES;
            [[LevelInfoStore sharedStore] setScore: [scoreLabel.text intValue] forLevel: 1];
        }
    
        if ([[LevelInfoStore sharedStore] furthestLevelReached] == 1)
            [[LevelInfoStore sharedStore] setFurthestLevelReached: 2];
    
        [[LevelInfoStore sharedStore] saveLevelInfo];
    } else
        newHighScore = YES;
    
    // Stop counting the score down
    [NSObject cancelPreviousPerformRequestsWithTarget: self selector: @selector(countScoreDown) object: nil];
    
    // Fade out the scoreLabels / pause/restart buttons
    [[self childNodeWithName: @"Score"] runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [scoreLabel runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [pauseButton runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [restartButton runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    
    // If there's a new high score, show the new high score sprite
    if (newHighScore) {
        // Create the crop node
        SKCropNode *newHighScore = [[SKCropNode alloc] init];
        [newHighScore setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"NewHighScoreMask.png"]];
        [newHighScore setPosition: ccp(0, scoreMenu.frame.size.height * 0.08)];
        [newHighScore.maskNode setPosition: ccp(-newHighScore.maskNode.frame.size.width, 0)];
        [scoreMenu addChild: newHighScore];
        
        // Create the actual new high score sprite
        SKSpriteNode *newHighScoreSprite = [SKSpriteNode spriteNodeWithImageNamed: @"NewHighScore.png"];
        [newHighScoreSprite setPosition: ccp(0, 0)];
        [newHighScore addChild: newHighScoreSprite];
        
        // Create the particle emitter
        SKEmitterNode *emitterNode = [NSKeyedUnarchiver unarchiveObjectWithFile: [[NSBundle mainBundle] pathForResource: @"Sparky" ofType: @"sks"]];
        [emitterNode setPosition: ccp(newHighScoreSprite.frame.origin.x, newHighScoreSprite.frame.origin.y + scoreMenu.frame.size.height * 0.12)];
        [emitterNode setZPosition: 3];
        [scoreMenu addChild: emitterNode];
        
        // Set the scale of the emitter
        if (!IS_IPAD)
            [emitterNode setScale: 0.2];
        else
            [emitterNode setScale: 0.5];
        
        // Save the birthrate for later to start the emitter
        float birthrate = emitterNode.particleBirthRate;
        [emitterNode setParticleBirthRate: 0];
        
        // Create the path for the emitter to follow
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter: ccp(scoreMenu.frame.size.width * 0.3, -scoreMenu.frame.size.height * 0.31) radius: newHighScore.maskNode.frame.size.width * 0.7 startAngle: 2.5 endAngle: 0.6 clockwise: NO];
        
        // Start the emitter, follow the path, fade out the emitter, and remove from parent
        [emitterNode runAction: [SKAction waitForDuration: 0.7] completion: ^{
            [emitterNode setParticleBirthRate: birthrate];
            [emitterNode runAction: [SKAction sequence: @[[SKAction group: @[[SKAction followPath: path.CGPath asOffset: YES orientToPath: NO duration: 0.6], [SKAction sequence: @[[SKAction waitForDuration: 0.4], [SKAction fadeOutWithDuration: 0.2]]]]], [SKAction removeFromParent]]]];
        }];
        
        
        // Move the mask to reveal the sprite
        [newHighScore.maskNode runAction: [SKAction sequence: @[[SKAction waitForDuration: 0.7], [SKAction moveToX: 0 duration: 0.6]]]];
    }
    
    
    // Score Label
    BMGlyphFont *font = [BMGlyphFont fontWithName: @"ScoreFont"];
    BMGlyphLabel *scoreResultLabel = [BMGlyphLabel labelWithText: scoreLabel.text font: font];
    [scoreResultLabel setPosition: ccp(-scoreMenu.frame.size.width * 0.03, scoreMenu.frame.size.height)];
    [scoreResultLabel setZPosition: 1];
    [scoreResultLabel setScale: 2];
    [scoreResultLabel setAlpha: 0];
    [scoreMenu addChild: scoreResultLabel];
    
    // If it's a new high score, the score labels will need to move down a little further
    int lableMovement = 0;
    if (newHighScore)
        lableMovement = scoreMenu.frame.size.height * 0.07;
    
    // Move the score labels down from the top
    SKAction *moveScore = [SKAction group: @[[SKAction scaleTo: 1 duration: 0.3], [SKAction moveTo: ccp(-scoreMenu.frame.size.width * 0.03, scoreMenu.frame.size.height * 0.02 - lableMovement) duration: 0.3]]];
    [moveScore setTimingMode: SKActionTimingEaseOut];
    [scoreResultLabel runAction: [SKAction waitForDuration: 0.3] completion: ^{
        [scoreResultLabel setAlpha: 1];
        [scoreResultLabel runAction: moveScore];
    }];
    
    // Bring the dimmer to the front and fade it in
    [dimmer setZPosition: 1.9];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.2]];
    
    // Scale up the scoreMenu
    [scoreMenu setZPosition: 2];
    [scoreMenu setScale: 0];
    [scoreMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.15], [SKAction scaleTo: 1 duration: 0.1]]]];
}


-(void)nextLevelButtonPressed
{
 //   Level2 *nextLevel = [Level2 sceneWithSize: winSize];
   // [nextLevel setScaleMode: SKSceneScaleModeAspectFit];
    //[self.view presentScene: nextLevel transition: [SKTransition fadeWithDuration: 0.8]];
}

@end
