//
//  LevelCreator.m
//  Lost Hippos
//
//  Created by Josh Braun on 10/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "LevelCreator.h"
#import "SKLabelNodeButton.h"
#import "SKSpriteNodeButton.h"
#import "CGPointExtension.h"
#import "MainMenu.h"
#import "LevelPlayer.h"
#import "PhysicsCategories.h"

@implementation LevelCreator
-(id)initWithSize:(CGSize)size
{
    self = [super initWithSize: size];
    if (self) {
        winSize = size;
        scaleLevel = 1.0;
        rotatingSceneObject = NO;
        objectsNeedPhysicsBodies = NO;
        
        // Set the scene's physics body
        groundY = 5;
        if (IS_IPAD)
            groundY = 10;
        [self setPhysicsBody: [SKPhysicsBody bodyWithEdgeLoopFromRect: CGRectMake(0, groundY, winSize.width, winSize.height - groundY)]];
        [self.physicsBody setCategoryBitMask: GroundCategory];

        // Have the app load the right atlases
        atlas = [SKTextureAtlas atlasNamed: @"Images"];
        atlas2 = [SKTextureAtlas atlasNamed: @"Level Creator"];

        littleHippos = [[NSMutableArray alloc] init];
        dropObjects = [[NSMutableArray alloc] init];
        stationaryObjects = [[NSMutableArray alloc] init];
        moveableObjects = [[NSMutableArray alloc] init];
        backgrounds = [[NSMutableArray alloc] init];
        
        // Tabs
        lastOptionsTab = BackgroundTab;
        lastObjectTab = DropTab;
        lastDropCenterPage = 1;
        lastBackgroundCenterPage = 1;
        lastMoveableCenterPage = 1;
        lastStationaryCenterPage = 1;
        
        // Buttons
        pauseButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PauseButton.png"];
        [pauseButton setPosition: ccp(winSize.width - 18.5, winSize.height - 20)];
        [pauseButton setZPosition: 1];
        [pauseButton setZoomOnTouchDown: NO];
        [pauseButton setDarkenOnTouchDown: YES];
        [pauseButton addTarget: self action: @selector(pauseButtonPressed)];
        [self addChild: pauseButton];

        optionsButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"OptionsButton.png"];
        [optionsButton setPosition: ccp(85, winSize.height - 30)];
        [optionsButton setZPosition: 1];
        [optionsButton setZoomOnTouchDown: NO];
        [optionsButton setDarkenOnTouchDown: YES];
        [optionsButton addTarget:self action: @selector(hideShowPopoverOnButton:)];
        [self addChild: optionsButton];
        
        addObjectButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"AddObjectButton.png"];
        [addObjectButton setPosition: ccp(30, winSize.height - 30)];
        [addObjectButton setZPosition: 1];
        [addObjectButton setZoomOnTouchDown: NO];
        [addObjectButton setDarkenOnTouchDown: YES];
        [addObjectButton addTarget: self action: @selector(hideShowPopoverOnButton:)];
        [self addChild: addObjectButton];
        
        dropQueue = [SKSpriteNodeButton spriteNodeWithImageNamed: @"DropObjectsContainer.png"];
        [dropQueue setPosition: ccp(185, winSize.height - 30)];
        [dropQueue setZPosition: 1];
        [dropQueue setZoomOnTouchDown: NO];
        [dropQueue setDarkenOnTouchDown: YES];
        [dropQueue addTarget: self action: @selector(editDropQueue)];
        [self addChild: dropQueue];
        
        
        // Automatically add the littleHippo and mamaHippo to the scene
        SKSpriteNodeButton *littleHippo = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Transparency.png"];
        [littleHippo setPosition: ccp(winSize.width/2 - 25, 31.5)];
        [littleHippo setZoomOnTouchDown: NO];
        [littleHippo setDarkenOnTouchDown: YES];
        [littleHippo sendDelegate: self touchEvents: YES];
        [littleHippo addTarget: self action: @selector(sceneObjectTapped:)];
        [self addChild: littleHippo];
        [littleHippos addObject: littleHippo];
        
        SKSpriteNode *littleHippoBody = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoBody.png"];
        [littleHippoBody setName: @"body"];
        [littleHippoBody setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: littleHippoBody.frame.size.width/2]];
        [littleHippoBody.physicsBody setAffectedByGravity: NO];
        [littleHippoBody.physicsBody setAllowsRotation: NO];
        [littleHippoBody.physicsBody setRestitution: 0];
        [littleHippoBody.physicsBody setCategoryBitMask: LittleHippoCategory];
        [littleHippoBody.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        [littleHippo addChild: littleHippoBody];
        
        SKSpriteNode *littleHead = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoHead.png"];
        [littleHead setUserData: [NSMutableDictionary dictionaryWithObject: [atlas textureNamed: @"LittleHippoHeadBlink.png"] forKey: @"blinkTexture"]];
        [littleHead setName: @"head"];
        [littleHead setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: 1 * scaleLevel]];
        [littleHead.physicsBody setAffectedByGravity: NO];
        [littleHead setPosition: ccp(0, -1.5)];
        [littleHippo addChild: littleHead];
        
        float legSpacingHorozontol = littleHippoBody.frame.size.width / (4 * scaleLevel);
        float legSpacingVertical = littleHippoBody.frame.size.height / (2.25 * scaleLevel);
        
        SKSpriteNode *littleLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg1 setName: @"leg1"];
        [littleLeg1 setZPosition: 0.1];
        [littleLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [littleLeg1 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: littleLeg1.frame.size]];
        [littleLeg1.physicsBody setAffectedByGravity: NO];
        [littleLeg1.physicsBody setRestitution: 0];
        [littleLeg1.physicsBody setCategoryBitMask: LegCategory];
        [littleLeg1.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        [littleHippo addChild: littleLeg1];
        
        SKSpriteNode *littleLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg2 setName: @"leg2"];
        [littleLeg2 setZPosition: 0.1];
        [littleLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [littleLeg2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: littleLeg2.frame.size]];
        [littleLeg2.physicsBody setAffectedByGravity: NO];
        [littleLeg2.physicsBody setRestitution: 0];
        [littleLeg2.physicsBody setCategoryBitMask: LegCategory];
        [littleLeg2.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        [littleHippo addChild: littleLeg2];
        
        SKPhysicsJointFixed *littleLeg1Joint = [SKPhysicsJointFixed jointWithBodyA: littleLeg1.physicsBody bodyB: littleHippoBody.physicsBody anchor: littleLeg1.position];
        [self.physicsWorld addJoint: littleLeg1Joint];
        SKPhysicsJointFixed *littleLeg2Joint = [SKPhysicsJointFixed jointWithBodyA: littleLeg2.physicsBody bodyB: littleHippoBody.physicsBody anchor: littleLeg2.position];
        [self.physicsWorld addJoint: littleLeg2Joint];
        SKPhysicsJointFixed *littleHeadJoint = [SKPhysicsJointFixed jointWithBodyA: littleHead.physicsBody bodyB: littleHippoBody.physicsBody anchor: littleHead.position];
        [self.physicsWorld addJoint: littleHeadJoint];
        
        
        // MamaHippo
        mamaHippo = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Transparency.png"];
        [mamaHippo setPosition: ccp(winSize.width/2 - 150, 50)];
        [mamaHippo setZoomOnTouchDown: NO];
        [mamaHippo setDarkenOnTouchDown: YES];
        [mamaHippo setScale: scaleLevel];
        [mamaHippo sendDelegate: self touchEvents: YES];
        [mamaHippo addTarget: self action: @selector(sceneObjectTapped:)];
        [self addChild: mamaHippo];
        
        SKSpriteNode *mamaHippoBody = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoBody.png"];
        [mamaHippoBody setName: @"body"];
        [mamaHippoBody setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: mamaHippoBody.frame.size.width/2]];
        [mamaHippoBody.physicsBody setAffectedByGravity: NO];
        [mamaHippoBody.physicsBody setAllowsRotation: NO];
        [mamaHippoBody.physicsBody setRestitution: 0];
        [mamaHippoBody.physicsBody setCategoryBitMask: MamaHippoCategory];
        [mamaHippoBody.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        [mamaHippo addChild: mamaHippoBody];
        
        SKSpriteNode *mamaHead = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoHead.png"];
        [mamaHead setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: 1 * scaleLevel]];
        [mamaHead.physicsBody setAffectedByGravity: NO];
        [mamaHead setUserData: [NSMutableDictionary dictionaryWithObject: [atlas textureNamed: @"MamaHippoHeadBlink.png"] forKey: @"blinkTexture"]];
        [mamaHead setName: @"head"];
        [mamaHead setPosition: ccp(0, -4)];
        if (IS_IPAD) [mamaHead setPosition: ccp(0, -4 * 1.8)];
        [mamaHippo addChild: mamaHead];
        
        // Calculate the position of the legs
        legSpacingHorozontol = (mamaHippoBody.frame.size.width * scaleLevel) / (4 * scaleLevel);
        legSpacingVertical = (mamaHippoBody.frame.size.height * scaleLevel) / (2.1 * scaleLevel);
        
        SKSpriteNode *mamaLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg1 setName: @"leg1"];
        [mamaLeg1 setZPosition: 0.1];
        [mamaLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [mamaLeg1 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: mamaLeg1.frame.size]];
        [mamaLeg1.physicsBody setAffectedByGravity: NO];
        [mamaLeg1.physicsBody setRestitution: 0];
        [mamaLeg1.physicsBody setCategoryBitMask: LegCategory];
        [mamaLeg1.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | GroundCategory];
        [mamaHippo addChild: mamaLeg1];
        
        SKSpriteNode *mamaLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg2 setName: @"leg2"];
        [mamaLeg2 setZPosition: 0.1];
        [mamaLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [mamaLeg2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: mamaLeg2.frame.size]];
        [mamaLeg2.physicsBody setAffectedByGravity: NO];
        [mamaLeg2.physicsBody setRestitution: 0];
        [mamaLeg2.physicsBody setCategoryBitMask: LegCategory];
        [mamaLeg2.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | GroundCategory];
        [mamaHippo addChild: mamaLeg2];
        
        SKPhysicsJointFixed *mamaLeg1Joint = [SKPhysicsJointFixed jointWithBodyA: mamaLeg1.physicsBody bodyB: mamaHippoBody.physicsBody anchor: mamaLeg1.position];
        [self.physicsWorld addJoint: mamaLeg1Joint];
        SKPhysicsJointFixed *mamaLeg2Joint = [SKPhysicsJointFixed jointWithBodyA: mamaLeg2.physicsBody bodyB: mamaHippoBody.physicsBody anchor: mamaLeg2.position];
        [self.physicsWorld addJoint: mamaLeg2Joint];
        SKPhysicsJointFixed *mamaHeadJoint = [SKPhysicsJointFixed jointWithBodyA: mamaHead.physicsBody bodyB: mamaHippoBody.physicsBody anchor:mamaHead.position];
        [self.physicsWorld addJoint: mamaHeadJoint];
        
        
        // Backgrounds
        SKSpriteNode *sky = [SKSpriteNode spriteNodeWithImageNamed: @"BlueSky.png"];
        [sky setPosition: ccp(winSize.width/2, winSize.height/2)];
        [sky setZPosition: -2];
        [self addChild: sky];
        [backgrounds addObject: sky];
        
        SKSpriteNode *rocks = [SKSpriteNode spriteNodeWithImageNamed: @"Level1Rocks.png"];
        [rocks setPosition: ccp(winSize.width/2, winSize.height * 0.13)];
        [rocks setZPosition: -1.5];
        [self addChild: rocks];
        [backgrounds addObject: rocks];
        
        SKSpriteNode *grass = [SKSpriteNode spriteNodeWithImageNamed: @"Grass.png"];
        [grass setPosition: ccp(winSize.width/2, 9)];
        [grass setZPosition: -1.4];
        [self addChild: grass];
        [backgrounds addObject: grass];
        
        SKSpriteNode *grass2 = [SKSpriteNode spriteNodeWithImageNamed: @"Grass2.png"];
        [grass2 setName: @"grass2"];
        [grass2 setPosition: ccp(winSize.width/2, 2)];
        [grass2 setZPosition: -1.3];
        [self addChild: grass2];
        [backgrounds addObject: grass2];
        
        SKSpriteNode *orangeSky = [SKSpriteNode spriteNodeWithImageNamed: @"OrangeSky.png"];
        [orangeSky setPosition: ccp(winSize.width/2, winSize.height/2)];
        [orangeSky setZPosition: -2];
        [backgrounds addObject: orangeSky];
        
        
        // Dropzone
        SKSpriteNode *dropZone = [SKSpriteNode spriteNodeWithImageNamed: @"DropZone.png"];
        [dropZone setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.175)];
        [dropZone setZPosition: -1.5];
        [self addChild: dropZone];

        
        
        // Dimmer for the pause menu
        dimmer = [[SKShapeNode alloc] init];
        [dimmer setPath: CGPathCreateWithRect(self.frame, NULL)];
        [dimmer setFillColor: [UIColor blackColor]];
        [dimmer setStrokeColor: [UIColor clearColor]];
        [dimmer setAlpha: 0];
        [dimmer setZPosition: -1];
        [self addChild: dimmer];

        
        // Position adjustments for iPad
        if (IS_IPAD) {
            [grass setPosition: ccp(winSize.width/2, 27)];
            [grass2 setPosition: ccp(winSize.width/2, 10)];
            [pauseButton setPosition: ccp(winSize.width - 35, winSize.height - 40)];
            [optionsButton setPosition: ccp(153, winSize.height - 45)];
            [addObjectButton setPosition: ccp(54, winSize.height - 45)];
            [dropQueue setPosition: ccp(310, winSize.height - 45)];
            [mamaHippo setPosition: ccp(winSize.width/2 - 250, 95)];
            [littleHippo setPosition: ccp(winSize.width/2, 57.5)];
        }
    }
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    skView = view;
}

#pragma mark - Pause Menu
-(void)pauseButtonPressed
{
    // Create the pause menu
    SKSpriteNode *pauseMenu = [SKSpriteNode spriteNodeWithImageNamed: @"PauseMenu.png"];
    [pauseMenu setPosition: ccp(winSize.width + pauseMenu.frame.size.width/2, winSize.height/2)];
    [pauseMenu setName: @"PauseMenu"];
    [pauseMenu setZPosition: 2];
    [self addChild: pauseMenu];
    
    /*SKSpriteNodeButton *returnButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"ReturnButton.png"];
    [returnButton setPosition: ccp(-pauseMenu.frame.size.width * 0.3, 0)];
    [returnButton addTarget: self action: @selector(returnToLevel)];
    [pauseMenu addChild: returnButton];
    
    SKSpriteNodeButton *homeButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"HomeButton.png"];
    [homeButton setPosition: ccp(0, 0)];
    [homeButton addBlock: ^{
        [self showConfirmationWithText: @"Quit To Main Menu?" yesBlock: ^{
            // Quit to the main menu
            MainMenu *mainMenu = [MainMenu sceneWithSize: winSize];
            [mainMenu setScaleMode: SKSceneScaleModeAspectFit];
            [self.view presentScene: mainMenu transition: [SKTransition fadeWithDuration: 0.8]];
        }];
    }];
    [pauseMenu addChild: homeButton];
    
    SKSpriteNodeButton *restartButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartLevelButton.png"];
    [restartButton setPosition: ccp(pauseMenu.frame.size.width * 0.3, 0)];
    [restartButton addBlock: ^{
        [self showConfirmationWithText: @"Restart Level Creator?" yesBlock: ^{
            // Restart the level creator
            LevelCreator *levelCreator = [LevelCreator sceneWithSize: winSize];
            [levelCreator setScaleMode: SKSceneScaleModeAspectFit];
            [self.view presentScene: levelCreator transition: [SKTransition fadeWithDuration: 0.8]];
        }];
    }];
    [pauseMenu addChild: restartButton];
    
    SKSpriteNodeButton *soundButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SoundOnButton.png"];
    [soundButton setZoomOnTouchDown: NO];
    [soundButton setDarkenOnTouchDown: YES];
    [soundButton setPosition: ccp(pauseMenu.frame.size.width * 0.3, -pauseMenu.size.height * 0.065)];
    [pauseMenu addChild: soundButton];
     */
    
    // Bring the dimmer and scale up the pauseMenu
    [dimmer setZPosition: 1.9];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.2]];
    
    SKAction *moveInPauseMenu = [SKAction moveToX: winSize.width - pauseMenu.frame.size.width * 0.45 duration: 0.2];
    [moveInPauseMenu setTimingMode: SKActionTimingEaseInEaseOut];
    [pauseMenu runAction: moveInPauseMenu];
    
}

-(void)returnToLevel
{
    SKSpriteNode *pauseMenu = (SKSpriteNode *)[self childNodeWithName: @"PauseMenu"];
    
    // Fade out the dimmer and scale down the pause menu
    [dimmer runAction: [SKAction fadeAlphaTo: 0 duration: 0.3] completion: ^{
        [dimmer setZPosition: -1];
        if (popoverArrow || dropQueue)
            [dimmer setZPosition: 0.5];
    }];
    [pauseMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.1], [SKAction scaleTo: 0 duration: 0.15]]] completion: ^{
        [pauseMenu removeFromParent];
    }];
}

-(void)showConfirmationWithText:(NSString *)text yesBlock:(void(^)(void))block
{
    // Move the dimmer to the front and fade it's alpha to 0.9
    [dimmer setZPosition: 2.1];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.9 duration: 0.3]];
    
    // Show a confirmation to quit to main menu
    SKLabelNode *areYouSure = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [areYouSure setName: @"AreYouSure"];
    [areYouSure setFontColor: [UIColor whiteColor]];
    [areYouSure setFontSize: 30];
    if (IS_IPAD)
        [areYouSure setFontSize: 60];
    [areYouSure setText: text];
    [areYouSure setAlpha: 0];
    [areYouSure setPosition: ccp(winSize.width/2, winSize.height/2 + winSize.height * 0.1)];
    [areYouSure setZPosition: 2.2];
    [self addChild: areYouSure];
    
    SKLabelNode *progressLost = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [progressLost setName: @"ProgressLost"];
    [progressLost setFontColor: [UIColor whiteColor]];
    [progressLost setFontSize: 15];
    if (IS_IPAD)
        [progressLost setFontSize: 30];
    [progressLost setText: @"(All progress will be lost)"];
    [progressLost setAlpha: 0];
    [progressLost setPosition: ccp(winSize.width/2, winSize.height/2 + winSize.height * 0.03)];
    [progressLost setZPosition: 2.2];
    [self addChild: progressLost];
    
    // Fade in the text
    [areYouSure runAction: [SKAction fadeInWithDuration: 0.3]];
    [progressLost runAction: [SKAction fadeInWithDuration: 0.3]];
    
    
    // Make the yes and no buttons
    SKLabelNodeButton *noButton = [SKLabelNodeButton labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [noButton setName: @"noButton"];
    [noButton setFontColor: [UIColor whiteColor]];
    [noButton setFontSize: 30];
    if (IS_IPAD)
        [noButton setFontSize: 60];
    [noButton setText: @"No"];
    [noButton setAlpha: 0];
    [noButton setPosition: ccp(winSize.width/2 - winSize.width * 0.1, winSize.height/2 - winSize.height * 0.1)];
    [noButton setZPosition: 2.2];
    [self addChild: noButton];
    
    SKLabelNodeButton *yesButton = [SKLabelNodeButton labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [yesButton setName: @"yesButton"];
    [yesButton setFontColor: [UIColor whiteColor]];
    [yesButton setFontSize: 30];
    if (IS_IPAD)
        [yesButton setFontSize: 60];
    [yesButton setText: @"Yes"];
    [yesButton setAlpha: 0];
    [yesButton setPosition: ccp(winSize.width/2 + winSize.width * 0.1, winSize.height/2 - winSize.height * 0.1)];
    [yesButton setZPosition: 2.2];
    [self addChild: yesButton];
    
    // Fade in the buttons
    [noButton runAction: [SKAction fadeInWithDuration: 0.3]];
    [yesButton runAction: [SKAction fadeInWithDuration: 0.3]];
    
    // avoids retain cycle in the block
    __weak typeof(noButton) weakNoButton = noButton;
    [noButton addBlock: ^{
        // Move the dimmer back and fade to 0.6
        [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.3] completion: ^{
            [dimmer setZPosition: 1.9];
        }];
        
        // Fade out the labels and remove them
        [areYouSure runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.3], [SKAction removeFromParent]]]];
        [progressLost runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.3], [SKAction removeFromParent]]]];
        [weakNoButton runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.3], [SKAction removeFromParent]]]];
        [yesButton runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.3], [SKAction removeFromParent]]]];
    }];
    
    
    // Make the yes button's block
    [yesButton addBlock: block];
}

#pragma mark - Scrollers
-(void)saveScrollersCenterPages
{
    // Make sure to save the drop object scroller center page
    SKScrollLayer *dropObjScroller = (SKScrollLayer *)[[popoverBody childNodeWithName: @"dropObjects"] childNodeWithName: @"scrollLayer"];
    if (dropObjScroller)
        lastDropCenterPage = dropObjScroller.currentScreen;
    
    // Save the center pages of the stationary and moveable scrollers
    SKScrollLayer *stationaryScroller = (SKScrollLayer *)[[popoverBody childNodeWithName: @"stationaryObjects"] childNodeWithName: @"scrollLayer"];
    if (stationaryScroller)
        lastStationaryCenterPage = stationaryScroller.currentScreen;
    
    SKScrollLayer *moveableScroller = (SKScrollLayer *)[[popoverBody childNodeWithName: @"moveableObjects"] childNodeWithName: @"scrollLayer"];
    if (moveableScroller)
        lastMoveableCenterPage = moveableScroller.currentScreen;
    
    // Store the center page of the background scroller
    SKScrollLayer *backgrndScroller = (SKScrollLayer *)[[popoverBody childNodeWithName: @"backgroundScroller"] childNodeWithName: @"scrollLayer"];
    if (backgrndScroller)
        lastBackgroundCenterPage = backgrndScroller.currentScreen;
}

#pragma mark - Popovers
-(void)hideShowPopoverOnButton:(SKSpriteNodeButton *)button
{
    // No Popover being shown
    if (!popoverArrow) {
        if (dropObjectEditor) {
            // Drop Objects Editor is being shown, so remove it
            
            // Fade out the drop objects editor
            [dropObjectEditor runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
                [dropObjectEditor removeFromParent];
                dropObjectEditor = nil;
            }];
            
            // Update the queue, fade it in, and bring it to the front
            [self updateDropQueue];
            [dropQueue setZPosition: 1];
            [dropQueue runAction: [SKAction fadeInWithDuration: 0.2]];
            [dropQueue setEnabled: YES];
            
            // Fade in the last object and start the alpha animation
            [[dropObjects lastObject] runAction: [SKAction fadeInWithDuration: 0.2]];
            SKAction *repeat = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction fadeAlphaTo: 0.8 duration: 1], [SKAction fadeAlphaTo: 0.6 duration: 1]]]];
            [[dropObjects lastObject] runAction: repeat];
            
            // Move the dimmer to the back
            [dimmer setZPosition: -1];
        }
        
        // If there's any precision move arrows fade them out
        if ([self childNodeWithName: @"Arrows"]) {
            [[self childNodeWithName: @"Arrows"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
            selectedSceneObject = nil;
        }
        
        // If there's a popover on a scene object fade it out
        if ([self childNodeWithName: @"sceneArrow"]) {
            [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
            [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
            
            selectedSceneObject = nil;
        }
        
        // If there are moving scene objects, decolorize and deselect them
        if (movingSceneObjects) {
            for (SKSpriteNodeButton *obj in movingSceneObjects) {
                // Decolorize them
                [obj runAction: [SKAction colorizeWithColorBlendFactor: 0  duration: 0.2]];
                for (SKSpriteNode *child in obj.children)
                    [child runAction: [SKAction colorizeWithColorBlendFactor: 0  duration: 0.2]];
            }
            
            movingSceneObjects = nil;
        }
        
        
        // Make the popover
        popoverArrow = [SKSpriteNode spriteNodeWithImageNamed: @"PopoverArrow.png"];
        [popoverArrow setUserData: [NSMutableDictionary dictionaryWithObject: button forKey: @"Button"]];
        [popoverArrow setPosition: ccp(button.position.x, button.position.y - button.frame.size.height/2 - popoverArrow.frame.size.height*0.45)];
        [popoverArrow setAlpha: 0];
        [popoverArrow setZPosition: 1.5];
        [self addChild: popoverArrow];
        
        popoverBody = [SKSpriteNode spriteNodeWithImageNamed: @"PopoverBody.png"];
        [popoverBody setAlpha: 0];
        [popoverBody setZPosition: 1.6];
        [self addChild: popoverBody];
        
        // Add the views to the popover/set the position of the popover body
        if (button == optionsButton) {
            [self addOptionsViewsToPopoverBody];
            
            [popoverBody setPosition: ccp(150, winSize.height - 139)];
            if (IS_IPAD) [popoverBody setPosition: ccp(280, popoverArrow.position.y - popoverArrow.frame.size.height*0.4 - popoverBody.frame.size.height*0.4)];
        } else if (button == addObjectButton) {
            [self addObjectsViewsToPopoverBody];
            
            [popoverBody setPosition: ccp(150, winSize.height - 139)];
            if (IS_IPAD) [popoverBody setPosition: ccp(280, popoverArrow.position.y - popoverArrow.frame.size.height*0.4 - popoverBody.frame.size.height*0.4)];
        }
        
        [popoverArrow runAction: [SKAction fadeInWithDuration: 0.2]];
        [popoverBody runAction: [SKAction fadeInWithDuration: 0.2]];
        
        // Move the dimmer to the front so nothing else gets tapped
        [dimmer setZPosition: 0.5];
    } else if (button == [popoverArrow.userData objectForKey: @"Button"]) {
        // The popover's being shown on this button
        
        // Save the scroller's center page
        [self saveScrollersCenterPages];
        
        // Hide the popover
        [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            [popoverArrow removeFromParent];
            popoverArrow = nil;
        }];
        [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            [popoverBody removeFromParent];
            popoverBody = nil;
        }];
        
        // Move the dimmer to the back
        [dimmer setZPosition: -1];
        
    } else {
        // The popover's being shown on another button
        // so move it to this button
        
        // Save the scroller's center page
        [self saveScrollersCenterPages];
        
        // Move the arrow
        [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            [popoverArrow setPosition: ccp(button.position.x, button.position.y - button.frame.size.height/2 - popoverArrow.frame.size.height*0.45)];
            [popoverArrow setUserData: [NSMutableDictionary dictionaryWithObject: button forKey: @"Button"]];
            [popoverArrow runAction: [SKAction fadeInWithDuration: 0.2]];
        }];
        
        // Move the body
        [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            // Remove old children
            for (SKNode *child in popoverBody.children)
                [child removeFromParent];
            
            // Add the views to the popover and set the position of the popover body
            if (button == optionsButton) {
                [self addOptionsViewsToPopoverBody];
                
                [popoverBody setPosition: ccp(150, winSize.height - 139)];
                if (IS_IPAD) [popoverBody setPosition: ccp(280, popoverArrow.position.y - popoverArrow.frame.size.height*0.4 - popoverBody.frame.size.height*0.4)];
            } else if (button == addObjectButton) {
                [self addObjectsViewsToPopoverBody];
                
                [popoverBody setPosition: ccp(150, winSize.height - 139)];
                if (IS_IPAD) [popoverBody setPosition: ccp(280, popoverArrow.position.y - popoverArrow.frame.size.height*0.4 - popoverBody.frame.size.height*0.4)];
            }
            
            [popoverBody runAction: [SKAction fadeInWithDuration: 0.2]];
        }];
    }
}

#pragma mark - Add Objects
-(void)addObjectsViewsToPopoverBody
{
    if (!popoverBody)
        return;
    // Tabs
    dropTab = [SKSpriteNodeButton spriteNodeWithImageNamed: @"DropObjectsTab.png"];
    [dropTab setPosition: ccp(-popoverBody.frame.size.width * 0.3122, popoverBody.frame.size.height * 0.33)];
    [dropTab addTarget: self action: @selector(dropObjectsTabPressed)];
    [dropTab setZoomOnTouchDown: NO];
    [dropTab setDarkenOnTouchDown: YES];
    [popoverBody addChild: dropTab];
    
    moveableTab = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MoveableObjectsTab.png"];
    [moveableTab setPosition: ccp(0, popoverBody.frame.size.height * 0.33)];
    [moveableTab addTarget: self action: @selector(moveableTabPressed)];
    [moveableTab setZoomOnTouchDown: NO];
    [moveableTab setDarkenOnTouchDown: YES];
    [moveableTab setColor: [UIColor blackColor]];
    [moveableTab setColorBlendFactor: 0.5];
    [popoverBody addChild: moveableTab];
    
    stationaryTab = [SKSpriteNodeButton spriteNodeWithImageNamed: @"StationaryObjectsTab.png"];
    [stationaryTab setPosition: ccp(popoverBody.frame.size.width * 0.312, popoverBody.frame.size.height * 0.33)];
    [stationaryTab addTarget: self action: @selector(stationaryTabPressed)];
    [stationaryTab setZoomOnTouchDown: NO];
    [stationaryTab setDarkenOnTouchDown: YES];
    [stationaryTab setColor: [UIColor blackColor]];
    [stationaryTab setColorBlendFactor: 0.5];
    [popoverBody addChild: stationaryTab];
    
    // Select the correct tab
    if (lastObjectTab == DropTab)
        [self dropObjectsTabPressed];
    else if (lastObjectTab == MoveableTab)
        [self moveableTabPressed];
    else if (lastObjectTab == StationaryTab)
        [self stationaryTabPressed];
}

#pragma mark - - Drop Objects Tab
-(void)dropObjectsTabPressed
{
    if (selectedTab == dropTab) {
        // Drop tab's already selected, so keep the selected color
    } else {
        // Selected the drop tab from another tab
        // Darken the other tabs since they're not selected
        [moveableTab setColor: [UIColor blackColor]];
        [moveableTab setColorBlendFactor: 0.5];
        
        [stationaryTab setColor: [UIColor blackColor]];
        [stationaryTab setColorBlendFactor: 0.5];
        
        // Undarken the drop tab
        [dropTab setColorBlendFactor: 0];
        
        // Hide the last tab's views
        if (lastObjectTab == MoveableTab)
            [self hideShowMoveableObjectsViews];
        else if (lastObjectTab == StationaryTab)
            [self hideShowStationaryObjectViews];
        
        // Show the drop tab's views
        [self hideShowDropObjectsViews];
        
        selectedTab = dropTab;
        lastObjectTab = DropTab;
    }
}
-(void)hideShowDropObjectsViews
{
    if (!popoverBody)
        return;
    
    if (![popoverBody childNodeWithName: @"dropObjects"]) {
        // Create the drop objects scroller
        float objectScale = 0.8;
        if (IS_IPAD)
            objectScale = 0.6;
        
        SKSpriteNodeButton *wideWood = [SKSpriteNodeButton spriteNodeWithImageNamed: @"WoodLarge.png"];
        [wideWood setName: @"wideWood"];
        [wideWood setScale: objectScale];
        [wideWood setZoomOnTouchDown: NO];
        [wideWood setDarkenOnTouchDown: YES];
        [wideWood sendDelegate: nil touchEvents: YES];
        [wideWood addTarget: self action: @selector(addObjectToDropQueue:)];
        
        SKSpriteNodeButton *boulder = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Boulder.png"];
        [boulder setName: @"boulder"];
        [boulder setScale: objectScale];
        [boulder setZoomOnTouchDown: NO];
        [boulder setDarkenOnTouchDown: YES];
        [boulder sendDelegate: nil touchEvents: YES];
        [boulder addTarget: self action: @selector(addObjectToDropQueue:)];
        
        // Mask any objects past the body
        SKCropNode *cropNode = [[SKCropNode alloc] init];
        [cropNode setName: @"dropObjects"];
        [cropNode setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"PopoverBody"]];
        [cropNode.maskNode setXScale: 0.95];
        [cropNode setPosition: ccp(0, 0)];
        [popoverBody addChild: cropNode];
        
        // Scroller that the objects are on
        float widthOffset = 175;
        if (IS_IPAD) widthOffset = 350;
        SKScrollLayer *scroller = [SKScrollLayer nodeWithLayers: @[wideWood, boulder] widthOffset: widthOffset scene: self size: CGSizeMake(popoverBody.frame.size.width, popoverBody.frame.size.height - popoverBody.frame.size.height * 0.4) showPagesIndicator: NO];
        [scroller setPosition: ccp(-popoverBody.frame.size.width/2, -popoverBody.frame.size.height * 0.4)];
        [scroller setName: @"scrollLayer"];
        [scroller setInitialPosition: scroller.position];
        [scroller setDisableNonCenterButtons: NO];
        [scroller moveToPage: lastDropCenterPage animated: NO];
        [cropNode addChild: scroller];
    } else if (![popoverBody childNodeWithName: @"dropObjects"].hidden) {
        // Hide the scroller
        [[popoverBody childNodeWithName: @"dropObjects"] setZPosition: -1];
        [[popoverBody childNodeWithName: @"dropObjects"] setHidden: YES];
    } else if ([popoverBody childNodeWithName: @"dropObjects"].hidden) {
        // Unhide the scroller
        [[popoverBody childNodeWithName: @"dropObjects"] setZPosition: 0];
        [[popoverBody childNodeWithName: @"dropObjects"] setHidden: NO];
    }
}

-(void)addObjectToDropQueue:(SKSpriteNodeButton *)object
{
    // Make a drop object using the scroll object's texture
    SKSpriteNodeButton *dropObject = [object copy];
    [dropObject addTarget: self action: @selector(editDropQueue)];
    [dropObjects addObject: dropObject];
    
    if ([object.name isEqual: @"hippo"])
        [littleHippos addObject: object];
    
    // Update the drop queue
    [self updateDropQueue];
    
    // Save the scroller's center page
    [self saveScrollersCenterPages];
    
    // Fade out the popover
    [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
        [popoverArrow removeFromParent];
        popoverArrow = nil;
    }];
    [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
        [popoverBody removeFromParent];
        popoverBody = nil;
    }];
    
    // Move the dimmer to the back
    [dimmer setZPosition: -1];
}

-(void)updateDropQueue
{
    // Remove all drop objects from their parent
    for (SKNode *obj in dropObjects) {
        [obj removeFromParent];
        [obj removeAllActions];
        for (SKNode *child in obj.children)
            [child removeAllActions];
    }
    for (SKNode *obj in dropQueue.children)
        [obj removeFromParent];
    
    if (dropObjects.count > 0) {
        SKSpriteNode *firstDropObject = (SKSpriteNode *)[dropObjects firstObject];
        [firstDropObject setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.2)];
        [firstDropObject setScale: scaleLevel];
        [firstDropObject setAlpha: 0.6];
        [self addChild: firstDropObject];
        
        for (SKNode *child in firstDropObject.children)
            [child setAlpha: 0.6];
        
        // Fade the alpha of the drop object
        SKAction *repeat = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction fadeAlphaTo: 0.8 duration: 1], [SKAction fadeAlphaTo: 0.6 duration: 1]]]];
        [firstDropObject runAction: repeat];
        
        for (SKNode *child in firstDropObject.children)
            [child runAction: repeat];
    }
    
    // Move back the objects already in the drop queue
    if (dropObjects.count > 1) {
        for (int i = (int)(dropObjects.count - (dropObjects.count - 1)); i < dropObjects.count; i++) {
            SKSpriteNode *obj = [dropObjects objectAtIndex: i];
            [obj removeAllActions];
            [obj setAlpha: 0.5];
            [obj setScale: 0.2];
            if (IS_IPAD)
                [obj setScale: 0.15];
            [dropQueue addChild: obj];
            if (i == dropObjects.count - (dropObjects.count - 1))
                [obj setPosition: ccp(dropQueue.frame.size.width * 0.25, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 2))
                [obj setPosition: ccp(dropQueue.frame.size.width * 0.0, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 3))
                [obj setPosition: ccp(-dropQueue.frame.size.width * 0.25, 0)];
            else if (i == dropObjects.count - (dropObjects.count - 4)) {
                // Doesn't fit. remove it from the queue and add
                // a '...' on the left side of the queue
                [obj removeFromParent];
                
                if (![dropQueue childNodeWithName: @"dotDotDot"]) {
                    SKLabelNode *dotDotDot = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
                    [dotDotDot setName: @"dotDotDot"];
                    [dotDotDot setFontColor: [UIColor whiteColor]];
                    [dotDotDot setFontSize: 10];
                    if (IS_IPAD)
                        [dotDotDot setFontSize: 10];
                    [dotDotDot setText: @"..."];
                    [dotDotDot setPosition: ccp(-dropQueue.frame.size.width * 0.395, -dropQueue.frame.size.height * 0.18)];
                    [dropQueue addChild: dotDotDot];
                }
            } else
                [obj removeFromParent];
        }
    }
}

#pragma mark - - Moveable Objects Tab
-(void)moveableTabPressed
{
    if (selectedTab == moveableTab) {
        // Moveable tab's already selected, so keep the selected color
    } else {
        // Selected the moveable tab from another tab
        // Darken the other tabs since they're not selected
        [dropTab setColor: [UIColor blackColor]];
        [dropTab setColorBlendFactor: 0.5];
        
        [stationaryTab setColor: [UIColor blackColor]];
        [stationaryTab setColorBlendFactor: 0.5];
        
        // Undarken the moveable tab
        [moveableTab setColorBlendFactor: 0];

        // Hide the last tab's views
        if (lastObjectTab == DropTab)
            [self hideShowDropObjectsViews];
        else if (lastObjectTab == StationaryTab)
            [self hideShowStationaryObjectViews];

        // Unhide the moveable tab's views
        [self hideShowMoveableObjectsViews];
        
        selectedTab = moveableTab;
        lastObjectTab = MoveableTab;
    }
}

-(void)hideShowMoveableObjectsViews
{
    if (!popoverBody)
        return;
    
    if (![popoverBody childNodeWithName: @"moveableObjects"]) {
        // Create the scroller
        float objectScale = 0.4;
        if (IS_IPAD)
            objectScale = 0.5;
        
        SKSpriteNodeButton *longWood = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Wood.png"];
        [longWood setScale: objectScale];
        [longWood setZoomOnTouchDown: NO];
        [longWood setDarkenOnTouchDown: YES];
        [longWood sendDelegate: nil touchEvents: YES];
        [longWood addTarget: self action: @selector(addObjectToScene:)];
        
        SKSpriteNodeButton *wideWood = [SKSpriteNodeButton spriteNodeWithImageNamed: @"WoodLarge.png"];
        [wideWood setScale: objectScale];
        [wideWood setZoomOnTouchDown: NO];
        [wideWood setDarkenOnTouchDown: YES];
        [wideWood sendDelegate: nil touchEvents: YES];
        [wideWood addTarget: self action: @selector(addObjectToScene:)];
        
        
        SKSpriteNodeButton *littleHippo = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Transparency.png"];
        [littleHippo setName: @"hippo"];
        [littleHippo setPosition: ccp(winSize.width/2 - 75, 60)];
        [littleHippo setZoomOnTouchDown: NO];
        [littleHippo setDarkenOnTouchDown: YES];
        [littleHippo sendDelegate: self touchEvents: YES];
        [littleHippo addTarget: self action: @selector(addObjectToScene:)];
        
        SKSpriteNode *littleHippoBody = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoBody.png"];
        [littleHippoBody setName: @"body"];
        [littleHippo addChild: littleHippoBody];
        
        SKSpriteNode *littleHead = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoHead.png"];
        [littleHead setUserData: [NSMutableDictionary dictionaryWithObject: [atlas textureNamed: @"LittleHippoHeadBlink.png"] forKey: @"blinkTexture"]];
        [littleHead setName: @"head"];
        [littleHead setPosition: ccp(0, -1.5)];
        [littleHippo addChild: littleHead];
        
        float legSpacingHorozontol = littleHippoBody.frame.size.width / (4 * scaleLevel);
        float legSpacingVertical = littleHippoBody.frame.size.height / (2.25 * scaleLevel);
        
        SKSpriteNode *littleLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg1 setName: @"leg1"];
        [littleLeg1 setZPosition: 0.1];
        [littleLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg1];
        
        SKSpriteNode *littleLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg2 setName: @"leg2"];
        [littleLeg2 setZPosition: 0.1];
        [littleLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg2];
        
        
        // Mask any objects past the body
        SKCropNode *cropNode = [[SKCropNode alloc] init];
        [cropNode setName: @"moveableObjects"];
        [cropNode setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"PopoverBody"]];
        [cropNode.maskNode setXScale: 0.95];
        [cropNode setPosition: ccp(0, 0)];
        [popoverBody addChild: cropNode];
        
        // Determines distance between objects. higher number = less distance
        float offset = 175;
        if (IS_IPAD)
            offset = 250;
        
        SKScrollLayer *scroller = [SKScrollLayer nodeWithLayers: @[longWood, wideWood, littleHippo] widthOffset: offset scene: self size: CGSizeMake(popoverBody.frame.size.width, popoverBody.frame.size.height - popoverBody.frame.size.height * 0.4) showPagesIndicator: NO];
        [scroller setPosition: ccp(-popoverBody.frame.size.width/2, -popoverBody.frame.size.height * 0.4)];
        [scroller setName: @"scrollLayer"];
        [scroller setInitialPosition: scroller.position];
        [scroller setDisableNonCenterButtons: NO];
        [scroller moveToPage: lastMoveableCenterPage animated: NO];
        [cropNode addChild: scroller];
    } else if (![popoverBody childNodeWithName: @"moveableObjects"].hidden) {
        // Hide the scroller
        [[popoverBody childNodeWithName: @"moveableObjects"] setZPosition: -1];
        [[popoverBody childNodeWithName: @"moveableObjects"] setHidden: YES];
    } else if ([popoverBody childNodeWithName: @"moveableObjects"].hidden) {
        // Unhide the scroller
        [[popoverBody childNodeWithName: @"moveableObjects"] setZPosition: 0];
        [[popoverBody childNodeWithName: @"moveableObjects"] setHidden: NO];
    }
}

-(void)addObjectToScene:(SKSpriteNodeButton *)object
{
    // Create a duplicate with the same texture
    // Add it to the center of the screen
    SKSpriteNodeButton *sceneObject = [object copy];
    [sceneObject setScale: scaleLevel];
    [sceneObject setPosition: ccp(winSize.width/2, winSize.height/2)];
    [sceneObject sendDelegate: self touchEvents: YES];
    [sceneObject addTarget: self action: @selector(sceneObjectTapped:)];
    [self addChild: sceneObject];
    
    // Add the physics bodies to the scene object
    if (![sceneObject.name isEqual: @"hippo"]) {
        [sceneObject setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: sceneObject.frame.size]];
        [sceneObject.physicsBody setAffectedByGravity: NO];
        [sceneObject.physicsBody setRestitution: 0];
        [sceneObject.physicsBody setCategoryBitMask: ObjectCategory];
        [sceneObject.physicsBody setCollisionBitMask: LittleHippoCategory | MamaHippoCategory | DropObjectCategory | ObjectCategory | LegCategory | GroundCategory];
    } else {
        SKSpriteNode *body = (SKSpriteNode *)[sceneObject childNodeWithName: @"body"];
        SKSpriteNode *head = (SKSpriteNode *)[sceneObject childNodeWithName: @"head"];
        SKSpriteNode *leg1 = (SKSpriteNode *)[sceneObject childNodeWithName: @"leg1"];
        SKSpriteNode *leg2 = (SKSpriteNode *)[sceneObject childNodeWithName: @"leg2"];
        
        [body setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: body.frame.size.width/2]];
        [body.physicsBody setAffectedByGravity: NO];
        [body.physicsBody setAllowsRotation: NO];
        [body.physicsBody setRestitution: 0];
        [body.physicsBody setCategoryBitMask: LittleHippoCategory];
        [body.physicsBody setCollisionBitMask: LittleHippoCategory | DropObjectCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        [head setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: 1 * scaleLevel]];
        [head.physicsBody setAffectedByGravity: NO];
        
        [leg1 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: leg1.frame.size]];
        [leg1.physicsBody setAffectedByGravity: NO];
        [leg1.physicsBody setRestitution: 0];
        [leg1.physicsBody setCategoryBitMask: LegCategory];
        [leg1.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        [leg2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: leg2.frame.size]];
        [leg2.physicsBody setAffectedByGravity: NO];
        [leg2.physicsBody setRestitution: 0];
        [leg2.physicsBody setCategoryBitMask: LegCategory];
        [leg2.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        SKPhysicsJointFixed *leg1Joint = [SKPhysicsJointFixed jointWithBodyA: leg1.physicsBody bodyB: body.physicsBody anchor: [sceneObject convertPoint: leg1.position toNode: self]];
        [self.physicsWorld addJoint: leg1Joint];
        SKPhysicsJointFixed *leg2Joint = [SKPhysicsJointFixed jointWithBodyA: leg2.physicsBody bodyB: body.physicsBody anchor: [sceneObject convertPoint: leg2.position toNode: self]];
        [self.physicsWorld addJoint: leg2Joint];
        SKPhysicsJointFixed *headJoint = [SKPhysicsJointFixed jointWithBodyA: head.physicsBody bodyB: body.physicsBody anchor: [sceneObject convertPoint: head.position toNode: self]];
        [self.physicsWorld addJoint: headJoint];
    }
    
    
    // Save into either the moveable or stationary array
    if (selectedTab == moveableTab && ![object.name isEqual: @"hippo"])
        [moveableObjects addObject: sceneObject];
    else if (selectedTab == stationaryTab) {
        [stationaryObjects addObject: sceneObject];
        
        // Inicate it's stationary by slightly tinting it
        [sceneObject setColor: [UIColor blackColor]];
        [sceneObject setColorBlendFactor: 0.1];
    } else if (selectedTab == moveableTab && [object.name isEqual: @"hippo"])
        [littleHippos addObject: sceneObject];
    
    
    // Save the scroller's center page
    [self saveScrollersCenterPages];
    
    // Fade out the popover
    [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
        [popoverArrow removeFromParent];
        popoverArrow = nil;
    }];
    [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
        [popoverBody removeFromParent];
        popoverBody = nil;
    }];
    
    // Move the dimmer to the back
    [dimmer setZPosition: -1];
}

#pragma mark - - Stationary Objects Tab
-(void)stationaryTabPressed
{
    if (selectedTab == stationaryTab) {
        // Stationary tab's already selected, so keep the selected color
    } else {
        // Selected the stationary tab from another tab
        // Darken the other tabs since they're not selected
        [dropTab setColor: [UIColor blackColor]];
        [dropTab setColorBlendFactor: 0.5];
        
        [moveableTab setColor: [UIColor blackColor]];
        [moveableTab setColorBlendFactor: 0.5];
        
        // Undarken the stationary tab
        [stationaryTab setColorBlendFactor: 0];
        
        // Hide the last tab's views
        if (lastObjectTab == DropTab)
            [self hideShowDropObjectsViews];
        else if (lastObjectTab == MoveableTab)
            [self hideShowMoveableObjectsViews];
        
        // Show the stationary tab's views
        [self hideShowStationaryObjectViews];
        
        selectedTab = stationaryTab;
        lastObjectTab = StationaryTab;
    }
}

-(void)hideShowStationaryObjectViews
{
    if (!popoverBody)
        return;
    
    if (![popoverBody childNodeWithName: @"stationaryObjects"]) {
        // Create the scroller
        float objectScale = 0.4;
        if (IS_IPAD)
            objectScale = 0.5;
        
        SKSpriteNodeButton *longWood = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Wood.png"];
        [longWood setScale: objectScale];
        [longWood setZoomOnTouchDown: NO];
        [longWood setDarkenOnTouchDown: YES];
        [longWood sendDelegate: nil touchEvents: YES];
        [longWood setColor: [UIColor blackColor]];
        [longWood setColorBlendFactor: 0.1];
        [longWood addTarget: self action: @selector(addObjectToScene:)];
        
        SKSpriteNodeButton *wideWood = [SKSpriteNodeButton spriteNodeWithImageNamed: @"WoodLarge.png"];
        [wideWood setScale: objectScale];
        [wideWood setZoomOnTouchDown: NO];
        [wideWood setDarkenOnTouchDown: YES];
        [wideWood sendDelegate: nil touchEvents: YES];
        [wideWood setColor: [UIColor blackColor]];
        [wideWood setColorBlendFactor: 0.1];
        [wideWood addTarget: self action: @selector(addObjectToScene:)];
        
        
        // Mask any objects past the body
        SKCropNode *cropNode = [[SKCropNode alloc] init];
        [cropNode setName: @"stationaryObjects"];
        [cropNode setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"PopoverBody"]];
        [cropNode.maskNode setXScale: 0.95];
        [cropNode setPosition: ccp(0, 0)];
        [popoverBody addChild: cropNode];
        
        
        // Scroller that the objects are on
        
        // Determines distance between objects. higher number = less distance
        float offset = 175;
        if (IS_IPAD)
            offset = 250;
        
        SKScrollLayer *scroller = [SKScrollLayer nodeWithLayers: @[longWood, wideWood] widthOffset: offset scene: self size: CGSizeMake(popoverBody.frame.size.width, popoverBody.frame.size.height - popoverBody.frame.size.height * 0.4) showPagesIndicator: NO];
        [scroller setPosition: ccp(-popoverBody.frame.size.width/2, -popoverBody.frame.size.height * 0.4)];
        [scroller setName: @"scrollLayer"];
        [scroller setInitialPosition: scroller.position];
        [scroller setDisableNonCenterButtons: NO];
        [scroller moveToPage: lastStationaryCenterPage animated: NO];
        [cropNode addChild: scroller];
    } else if (![popoverBody childNodeWithName: @"stationaryObjects"].hidden) {
        // Hide the scroller
        [[popoverBody childNodeWithName: @"stationaryObjects"] setZPosition: -1];
        [[popoverBody childNodeWithName: @"stationaryObjects"] setHidden: YES];
    } else if ([popoverBody childNodeWithName: @"stationaryObjects"].hidden) {
        // Unhide the scroller
        [[popoverBody childNodeWithName: @"stationaryObjects"] setZPosition: 0];
        [[popoverBody childNodeWithName: @"stationaryObjects"] setHidden: NO];
    }
}

#pragma mark - Options Menu
-(void)addOptionsViewsToPopoverBody
{
    if (!popoverBody)
        return;
    
    // Tabs
    backgroundTab = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackgroundTab.png"];
    [backgroundTab setPosition: ccp(-popoverBody.frame.size.width * 0.2353, popoverBody.frame.size.height * 0.33)];
    [backgroundTab addTarget: self action: @selector(backgroundTabPressed)];
    [backgroundTab setZoomOnTouchDown: NO];
    [backgroundTab setDarkenOnTouchDown: YES];
    [backgroundTab setXScale: 1.005];
    [popoverBody addChild: backgroundTab];

    playSaveTab = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PlaySaveTab.png"];
    [playSaveTab setPosition: ccp(popoverBody.frame.size.width * 0.235, popoverBody.frame.size.height * 0.33)];
    [playSaveTab addTarget: self action: @selector(playSaveTabPressed)];
    [playSaveTab setZoomOnTouchDown: NO];
    [playSaveTab setDarkenOnTouchDown: YES];
    [playSaveTab setColor: [UIColor blackColor]];
    [playSaveTab setColorBlendFactor: 0.5];
    [popoverBody addChild: playSaveTab];
    
    // Select the correct tab
    if (lastOptionsTab == BackgroundTab)
        [self backgroundTabPressed];
    else if (lastOptionsTab == PlaySaveTab)
        [self playSaveTabPressed];
}

#pragma mark - - Background Tab
-(void)backgroundTabPressed
{
    if (backgroundTab == selectedTab) {
        // Background tab's already selected, so keep the selected color
    } else {
        // Selected the background tab from another tab
        // Darken the other tab since it's not selected
        [playSaveTab setColor: [UIColor blackColor]];
        [playSaveTab setColorBlendFactor: 0.5];

        // Undarken the background tab
        [backgroundTab setColorBlendFactor: 0];
        
        // Remove the other tab's views
        if (lastOptionsTab == PlaySaveTab)
            [self hideShowPlaySaveViews];
        
        // Bring the background views to the front
        [self hideShowBackgroundViews];

        selectedTab = backgroundTab;
        lastOptionsTab = BackgroundTab;
    }
}

-(void)hideShowBackgroundViews
{
    SKNode *node = [popoverBody childNodeWithName: @"backgroundScroller"];
    
    if (!node) {
        // Create an array of thumbnails
        NSMutableArray *scrollerLayers = [[NSMutableArray alloc] init];
        BOOL firstLayer = YES;
        for (SKSpriteNode *bg in backgrounds) {
            SKSpriteNodeButton *thumbnail = [SKSpriteNodeButton spriteNodeWithTexture: bg.texture size: CGSizeMake(popoverBody.frame.size.width * 0.25, popoverBody.frame.size.height * 0.25)];
            [thumbnail addTarget: self action: @selector(backgroundImageTapped:)];
            [thumbnail setUserData: [NSMutableDictionary dictionaryWithObjectsAndKeys: bg, @"realBackground", [NSNumber numberWithFloat: bg.zPosition], @"zPosition", nil]];
            [thumbnail setZoomOnTouchDown: NO];
            [thumbnail setDarkenOnTouchDown: YES];
            [thumbnail sendDelegate: nil touchEvents: YES];
            [thumbnail setEnabled: NO];
            
            if (firstLayer) {
                [thumbnail setEnabled: YES];
                firstLayer = NO;
            }
            
            // Add a green check if it's a child of the scene
            if (bg.scene == self) {
                SKSpriteNode *check = [SKSpriteNode spriteNodeWithImageNamed: @"BackgroundCheck.png"];
                [check setName: @"check"];
                [check setPosition: ccp(thumbnail.frame.size.width * 0.4, thumbnail.frame.size.height * 0.4)];
                [thumbnail addChild: check];
            }
            
            [scrollerLayers addObject: thumbnail];
        }
        
        // Mask any objects past the body
        SKCropNode *cropNode = [[SKCropNode alloc] init];
        [cropNode setName: @"backgroundScroller"];
        [cropNode setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"PopoverBody"]];
        [cropNode.maskNode setXScale: 0.95];
        [cropNode setPosition: ccp(0, 0)];
        [popoverBody addChild: cropNode];
        
        // Scroller that the objects are on
        int widthOffset = 175;
        if (IS_IPAD) widthOffset = 350;
        SKScrollLayer *scroller = [SKScrollLayer nodeWithLayers: scrollerLayers widthOffset: widthOffset scene: self size: CGSizeMake(popoverBody.frame.size.width, popoverBody.frame.size.height - popoverBody.frame.size.height * 0.4) showPagesIndicator: NO];
        [scroller setPosition: ccp(-popoverBody.frame.size.width/2, -popoverBody.frame.size.height * 0.4)];
        [scroller setName: @"scrollLayer"];
        [scroller setInitialPosition: scroller.position];
        [scroller moveToPage: lastBackgroundCenterPage animated: NO];
        [cropNode addChild: scroller];
    } else if (node.zPosition == -1) {
        [node setZPosition: 0];
        [node setHidden: NO];
    } else {
        [node setZPosition: -1];
        [node setHidden: YES];
        
        // Store the center page of the scroller since we're hiding it
        SKScrollLayer *backgrndScroller = (SKScrollLayer *)[node childNodeWithName: @"scrollLayer"];
        lastBackgroundCenterPage = backgrndScroller.currentScreen;
    }
}

-(void)backgroundImageTapped:(SKSpriteNodeButton *)image
{
    // Scale up the image and move it to the right
    SKAction *moveImage = [SKAction group: @[[SKAction scaleTo: 1.5 duration: 0.2], [SKAction moveTo: ccp(popoverBody.frame.size.width * 0.25, 0) duration: 0.2]]];
    [moveImage setTimingMode: SKActionTimingEaseInEaseOut];
    
    SKSpriteNodeButton *imageCopy = [SKSpriteNodeButton spriteNodeWithTexture: image.texture size: image.frame.size];
    [imageCopy setPosition: ccp(0,-popoverBody.frame.size.height * 0.1)];
    [imageCopy setName: @"thumbnail"];
    [imageCopy setUserData: image.userData];
    [imageCopy.userData setObject: image forKey: @"originalThumbnail"];
    [imageCopy setZoomOnTouchDown: NO];
    [imageCopy setDarkenOnTouchDown: YES];
    [imageCopy addTarget: self action: @selector(addOrRemoveBackground:)];
    [popoverBody addChild: imageCopy];
    [imageCopy runAction: moveImage];
    
    // Add a green check if the original image has one
    if ([image childNodeWithName: @"check"]) {
        SKSpriteNode *check = [SKSpriteNode spriteNodeWithImageNamed: @"BackgroundCheck.png"];
        [check setName: @"check"];
        [check setPosition: ccp(imageCopy.frame.size.width * 0.4, imageCopy.frame.size.height * 0.4)];
        [imageCopy addChild: check];
    }
    
    // Hide the original image so we don't see it fading out
    [image setHidden: YES];
    
    // Fade out the cropNode and tabs
    [[popoverBody childNodeWithName: @"backgroundScroller"] runAction: [SKAction fadeOutWithDuration: 0.2]];
    [[popoverBody childNodeWithName: @"backgroundScroller"] setUserInteractionEnabled: NO];
    [backgroundTab runAction: [SKAction fadeOutWithDuration: 0.2]];
    [playSaveTab runAction: [SKAction fadeOutWithDuration: 0.2]];
    [backgroundTab setEnabled: NO];
    [playSaveTab setEnabled: NO];
    
    
    // Add the labels and buttons
    SKLabelNode *zPositionLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [zPositionLabel setName: @"zPositionLabel"];
    [zPositionLabel setFontColor: [UIColor whiteColor]];
    [zPositionLabel setFontSize: 15];
    if (IS_IPAD)
        [zPositionLabel setFontSize: 20];
    [zPositionLabel setText: @"Z Position"];
    [zPositionLabel setPosition: ccp(-popoverBody.frame.size.width * 0.2, popoverBody.frame.size.height * 0.1)];
    [zPositionLabel setAlpha: 0];
    [popoverBody addChild: zPositionLabel];

    SKLabelNode *zPositionFloatLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [zPositionFloatLabel setName: @"zPositionFloatLabel"];
    [zPositionFloatLabel setFontColor: [UIColor whiteColor]];
    [zPositionFloatLabel setFontSize: 30];
    if (IS_IPAD)
        [zPositionFloatLabel setFontSize: 40];
    [zPositionFloatLabel setText: [NSString stringWithFormat: @"%.1f", [[imageCopy.userData objectForKey: @"zPosition"] floatValue]]];
    [zPositionFloatLabel setPosition: ccp(-popoverBody.frame.size.width * 0.2, -popoverBody.frame.size.height * 0.15)];
    [zPositionFloatLabel setAlpha: 0];
    [popoverBody addChild: zPositionFloatLabel];
    
    SKSpriteNodeButton *minusButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MinusButton.png"];
    [minusButton setName: @"minusButton"];
    [minusButton setPosition: ccp(-popoverBody.frame.size.width * 0.37, -popoverBody.frame.size.height * 0.05)];
    [minusButton addTarget: self action: @selector(changeBackgroundZPosition:)];
    [minusButton setZoomOnTouchDown: NO];
    [minusButton setAlpha: 0];
    [popoverBody addChild: minusButton];
    
    SKSpriteNodeButton *plusButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PlusButton.png"];
    [plusButton setName: @"plusButton"];
    [plusButton setPosition: ccp(-popoverBody.frame.size.width * 0.02, -popoverBody.frame.size.height * 0.05)];
    [plusButton addTarget: self action: @selector(changeBackgroundZPosition:)];
    [plusButton setZoomOnTouchDown: NO];
    [plusButton setAlpha: 0];
    [popoverBody addChild: plusButton];
    
    SKSpriteNodeButton *backButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackBackgroundButton.png"];
    [backButton setPosition: ccp(-popoverBody.frame.size.width * 0.4, popoverBody.frame.size.height * 0.25)];
    [backButton setZoomOnTouchDown: NO];
    [backButton setAlpha: 0];
    [popoverBody addChild: backButton];
    
    // Fade in the buttons and labels
    SKAction *waitAndFadeIn = [SKAction sequence: @[[SKAction waitForDuration: 0.1], [SKAction fadeInWithDuration: 0.2]]];
    [zPositionLabel runAction: waitAndFadeIn];
    [zPositionFloatLabel runAction: waitAndFadeIn];
    [minusButton runAction: waitAndFadeIn];
    [plusButton runAction: waitAndFadeIn];
    [backButton runAction: waitAndFadeIn];

    
    __weak typeof(backButton) weakBackButton = backButton;
    [backButton addBlock: ^{
        // fade out the buttons and labels
        SKAction *fadeAndRemove = [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]];
        [weakBackButton runAction: fadeAndRemove];
        [plusButton runAction: fadeAndRemove];
        [minusButton runAction: fadeAndRemove];
        [zPositionLabel runAction: fadeAndRemove];
        [zPositionFloatLabel runAction: fadeAndRemove];
        
        // Move the image, scale it down, and fade in the scroller/tabs
        SKAction *moveTheImage = [SKAction group: @[[SKAction scaleTo: 1 duration: 0.2], [SKAction moveTo: ccp(0,-popoverBody.frame.size.height * 0.1) duration: 0.3]]];
        [moveTheImage setTimingMode: SKActionTimingEaseInEaseOut];
        [imageCopy runAction: moveTheImage completion: ^{
            // Unhide the image
            [image setHidden: NO];
            
            // Fade in the cropNode and tabs
            [[popoverBody childNodeWithName: @"backgroundScroller"] runAction: [SKAction fadeInWithDuration: 0.2]];
            [[popoverBody childNodeWithName: @"backgroundScroller"] setUserInteractionEnabled: YES];
            [backgroundTab runAction: [SKAction fadeInWithDuration: 0.2]];
            [playSaveTab runAction: [SKAction fadeInWithDuration: 0.2]];
            [backgroundTab setEnabled: YES];
            [playSaveTab setEnabled: YES];
            
            // Wait until everything's faded in, then remove the imageCopy
            [imageCopy runAction: [SKAction sequence: @[[SKAction waitForDuration: 0.2], [SKAction removeFromParent]]]];
        }];
    }];

}
-(void)changeBackgroundZPosition:(SKSpriteNodeButton *)button
{
    SKSpriteNode *thumbnail = (SKSpriteNode *)[popoverBody childNodeWithName: @"thumbnail"];
    SKLabelNode *zPositionFloatLabel = (SKLabelNode *)[popoverBody childNodeWithName: @"zPositionFloatLabel"];
    float zPosition = [[thumbnail.userData objectForKey: @"zPosition"] floatValue];
    
    if ([button.name isEqual: @"minusButton"]) {
        // Subract the zPosition by 0.1
        zPosition -= 0.1;
        [thumbnail.userData setObject: [NSNumber numberWithFloat: zPosition] forKey: @"zPosition"];
        [[thumbnail.userData objectForKey: @"realBackground"] setZPosition: zPosition];
        [zPositionFloatLabel setText: [NSString stringWithFormat: @"%.1f", zPosition]];
    } else if ([button.name isEqual: @"plusButton"]) {
        // Plus the zPosition by 0.1
        zPosition += 0.1;
        [thumbnail.userData setObject: [NSNumber numberWithFloat: zPosition] forKey: @"zPosition"];
        [[thumbnail.userData objectForKey: @"realBackground"] setZPosition: zPosition];
        [zPositionFloatLabel setText: [NSString stringWithFormat: @"%.1f", zPosition]];
    }
}

-(void)addOrRemoveBackground:(SKSpriteNodeButton *)image
{
    SKSpriteNode *background = (SKSpriteNode *)[image.userData objectForKey: @"realBackground"];
    SKSpriteNode *originalThumbnail = [image.userData objectForKey: @"originalThumbnail"];
    
    // Remove it if there's a check when tapped
    // Add it if there's no check when tapped
    if ([image childNodeWithName: @"check"]) {
        // Remove it from the scene and remove the check
        [background removeFromParent];
        [[image childNodeWithName: @"check"] removeFromParent];
        [[originalThumbnail childNodeWithName: @"check"] removeFromParent];
    } else {
        // Add it to the scene and add the check
        [self addChild: background];
        
        // Add check to the thumbnail
        SKSpriteNode *check = [SKSpriteNode spriteNodeWithImageNamed: @"BackgroundCheck.png"];
        [check setName: @"check"];
        [check setPosition: ccp(originalThumbnail.frame.size.width * 0.4, originalThumbnail.frame.size.height * 0.4)];
        [image addChild: check];
        
        // Add check to the scroller thumbnail
        SKSpriteNode *check2 = [SKSpriteNode spriteNodeWithImageNamed: @"BackgroundCheck.png"];
        [check2 setName: @"check"];
        [check2 setPosition: ccp(originalThumbnail.frame.size.width * 0.4, originalThumbnail.frame.size.height * 0.4)];
        [originalThumbnail addChild: check2];
    }
}

#pragma mark - - Play/Save Tab
-(void)playSaveTabPressed
{
    if (selectedTab == playSaveTab) {
        // playSaveTab's already selected, so keep the selected color
    } else {
        // Selected the playSaveTab from the backgroundTab
        // Darken the backgroundTab since it's not selected
        [backgroundTab setColor: [UIColor blackColor]];
        [backgroundTab setColorBlendFactor: 0.5];
        
        // Undarken the playSaveTab
        [playSaveTab setColorBlendFactor: 0];
        
        // Remove the backgroundTab's views
        if (lastOptionsTab == BackgroundTab)
            [self hideShowBackgroundViews];

        // Add the play and save buttons
        [self hideShowPlaySaveViews];
        
        selectedTab = playSaveTab;
        lastOptionsTab = PlaySaveTab;
    }
}

-(void)hideShowPlaySaveViews
{
    SKSpriteNodeButton *playButton = (SKSpriteNodeButton *)[popoverBody childNodeWithName: @"playCreatorButton"];
    SKSpriteNodeButton *saveButton = (SKSpriteNodeButton *)[popoverBody childNodeWithName: @"saveButton"];
    
    if (!playButton) {
        SKSpriteNodeButton *playButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PlayCreatorButton.png"];
        [playButton setName: @"playCreatorButton"];
        [playButton addTarget: self action: @selector(playLevel)];
        [playButton setPosition: ccp(-popoverBody.frame.size.width * 0.15, -popoverBody.frame.size.height * 0.1)];
        [popoverBody addChild: playButton];
        
        SKSpriteNodeButton *saveButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SaveButton.png"];
        [saveButton setName: @"saveButton"];
        [saveButton addTarget: self action: @selector(showSaveLevelMenu)];
        [saveButton setPosition: ccp(popoverBody.frame.size.width * 0.15, -popoverBody.frame.size.height * 0.1)];
        [popoverBody addChild: saveButton];
    } else if (playButton.zPosition == 0) {
        [playButton setZPosition: -1];
        [playButton setHidden: YES];
        [playButton setEnabled: NO];
    
        [saveButton setZPosition: -1];
        [saveButton setHidden: YES];
        [saveButton setEnabled: NO];
    } else {
        [playButton setZPosition: 0];
        [playButton setHidden: NO];
        [playButton setEnabled: YES];
        
        [saveButton setZPosition: 0];
        [saveButton setHidden: NO];
        [saveButton setEnabled: YES];
    }
    
}

-(void)playLevel
{
    // Prep the objects and hippos for playing
    NSMutableArray *preppedDropObjects = [[NSMutableArray alloc] init];
    NSMutableArray *preppedMoveables = [[NSMutableArray alloc] init];
    NSMutableArray *preppedStationaries = [[NSMutableArray alloc] init];
    NSMutableArray *preppedLittleHippos = [[NSMutableArray alloc] init];
    NSMutableArray *preppedBackgrounds = [[NSMutableArray alloc] init];
    
    for (SKSpriteNodeButton *obj in dropObjects)
        [preppedDropObjects addObject: [obj spriteNodeVersion]];
    
    
    NSMutableArray *sceneObjects = [NSMutableArray arrayWithArray: moveableObjects];
    [sceneObjects addObjectsFromArray: stationaryObjects];
    
    for (SKSpriteNodeButton *obj in sceneObjects) {
        SKSpriteNode *spriteObj = [obj spriteNodeVersion];
        [spriteObj setPhysicsBody: [obj.physicsBody copy]];
        
        if ([moveableObjects containsObject: obj]) {
            [spriteObj.physicsBody setAffectedByGravity: YES];
            [spriteObj.physicsBody setDensity: 1];
            [preppedMoveables addObject: spriteObj];
        } else if ([stationaryObjects containsObject: obj]) {
            [spriteObj.physicsBody setDynamic: NO];
            [preppedStationaries addObject: spriteObj];
        }
        
        // For some reason, setting the spriteObj's physics body to a copy of obj.physicsBody
        // causes the physics body to disconnect from the obj. Reset it
        [obj setPhysicsBody: obj.physicsBody];
        
        // Make sure the zRotation isn't slightly off (ex. 0.06°)
        float degrees = RADIANS_TO_DEGREES(spriteObj.zRotation);
        if (degrees > -3 && degrees < 3)
            [spriteObj setZRotation: DEGREES_TO_RADIANS(0)];
        else if (degrees > 87 && degrees < 93)
            [spriteObj setZRotation: DEGREES_TO_RADIANS(90)];
        else if (degrees > 177 && degrees < 183)
            [spriteObj setZRotation: DEGREES_TO_RADIANS(180)];
        else if (degrees > 267 && degrees < 273)
            [spriteObj setZRotation: DEGREES_TO_RADIANS(270)];
    }
    for (SKSpriteNodeButton *hippo in littleHippos) {
        SKSpriteNode *spriteHippo = [hippo spriteNodeVersion];

        SKNode *leg1 = [spriteHippo childNodeWithName: @"leg1"];
        SKNode *leg2 = [spriteHippo childNodeWithName: @"leg2"];
        SKNode *body = [spriteHippo childNodeWithName: @"body"];
        SKNode *head = [spriteHippo childNodeWithName: @"head"];

        // Fix the body's physicsbody
        [body.physicsBody setAffectedByGravity: YES];
        [body.physicsBody setAllowsRotation: YES];
        [body.physicsBody setDensity: 2];
        [body.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | DropObjectCategory | LegCategory];
        [body setZPosition: 0.2];
        
        [self addChild: spriteHippo];
        
        // Save the body's zRotation
        float zRotation = body.zRotation;
        [body setZRotation: 0];
        
        // Adjust the position of the hippo
        [spriteHippo setPosition: [spriteHippo convertPoint: body.position toNode: self]];
        [body setPosition: ccp(0, 0)];
        [head setPosition: ccp(0, -1.5 * scaleLevel)];

        // Get rid of the legs/head physics bodies and add them as children of the body
        [leg1 setPhysicsBody: nil];
        [leg1 setZRotation: 0];
        [leg1 setScale: 1];
        [leg1 removeFromParent];
        [leg2 setPhysicsBody: nil];
        [leg2 setZRotation: 0];
        [leg2 setScale: 1];
        [leg2 removeFromParent];
        [head setPhysicsBody: nil];
        [head setZRotation: 0];
        [head setScale: 1];
        [head removeFromParent];
        
        [body addChild: leg1];
        [body addChild: leg2];
        [body addChild: head];
        
        // Adjust the legs of the hippo
        float legSpacingHorozontol = (body.frame.size.width * scaleLevel) / (4 * scaleLevel);
        float legSpacingVertical = (body.frame.size.height * scaleLevel) / (2.75 * scaleLevel);

        [leg1 setPosition: ccp(body.position.x - legSpacingHorozontol, body.position.y - legSpacingVertical)];
        [leg2 setPosition: ccp(body.position.x + legSpacingHorozontol, body.position.y - legSpacingVertical)];
        
        [leg1 setZPosition: 0.3];
        [leg2 setZPosition: 0.3];
        
        
        // Rotate the body to the correct zRotation
        [body setZRotation: zRotation];

        [spriteHippo removeFromParent];
        [preppedLittleHippos addObject: spriteHippo];
    }
    for (SKSpriteNode *bg in backgrounds) {
        if ([bg inParentHierarchy: self]) {
            SKSpriteNode *bgCopy = [bg copy];
            if ([bg.name isEqual: @"grass2"])
                [bgCopy setZPosition: 1];
            [preppedBackgrounds addObject: bgCopy];
        }
    }
    
    // Prep MamaHippo. Add head as child of body w/o physics body and prep legs physics bodies
    SKSpriteNode *preppedMamaHippo = [mamaHippo spriteNodeVersion];
    SKSpriteNode *head = (SKSpriteNode *)[preppedMamaHippo childNodeWithName: @"head"];
    [head removeFromParent];
    [head setPosition: ccp(0, -4 * scaleLevel)];
    [head setScale: 1];
    [head setPhysicsBody: nil];
    [[preppedMamaHippo childNodeWithName: @"body"] addChild: head];
    for (SKNode *child in preppedMamaHippo.children) {
        [child setPhysicsBody: [child.physicsBody copy]];
        if ([child.name isEqual: @"leg1"] || [child.name isEqual: @"leg2"])
            [child.physicsBody setAffectedByGravity: YES];
        [child.physicsBody setCollisionBitMask: ObjectCategory | GroundCategory | DropObjectCategory];
    }
    
    // Create the scene and present it
    LevelPlayer *levelPlayer = [LevelPlayer sceneWithSize: winSize creatorMode: YES scaleLevel: scaleLevel littleHippos: preppedLittleHippos mamaHippo:preppedMamaHippo stationaryObjects: preppedStationaries moveableObjects: preppedMoveables dropObjects: preppedDropObjects backgrounds: preppedBackgrounds];
    [levelPlayer setScaleMode: SKSceneScaleModeAspectFit];
    [levelPlayer setLevelCreator: self];
    [skView presentScene: levelPlayer transition: [SKTransition fadeWithDuration: 0.8]];
}

-(void)showSaveLevelMenu
{
    
}

-(void)saveLevel
{
    
}

#pragma mark - Edit Drop Objects
-(void)editDropQueue
{
    // If there's a popover, fade it out
    if (popoverBody) {
        // Save the scroller's center page
        [self saveScrollersCenterPages];

        [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            [popoverArrow removeFromParent];
            popoverArrow = nil;
        }];
        [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
            [popoverBody removeFromParent];
            popoverBody = nil;
        }];
    }

    // If there's any precision move arrows fade them out
    if ([self childNodeWithName: @"Arrows"]) {
        [[self childNodeWithName: @"Arrows"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        selectedSceneObject = nil;
    }
    
    // If there's a popover on a scene object fade it out
    if ([self childNodeWithName: @"sceneArrow"]) {
        [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        
        selectedSceneObject = nil;
    }
    
    // If there are moving scene objects, decolorize and deselect them
    if (movingSceneObjects) {
        for (SKSpriteNodeButton *obj in movingSceneObjects) {
            // Decolorize them
            [obj runAction: [SKAction colorizeWithColorBlendFactor: 0  duration: 0.2]];
            for (SKSpriteNode *child in obj.children)
                [child runAction: [SKAction colorizeWithColorBlendFactor: 0  duration: 0.2]];
        }
        
        movingSceneObjects = nil;
    }

    // Create a 'copy' of the drop queue w/out the objects
    // Make it the size to stretch accross the screen
    dropObjectEditor = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed: @"PopoverBody.png"] size: CGSizeMake(winSize.width * 0.8, winSize.height * 0.3)];
    [dropObjectEditor setPosition: ccp(winSize.width/2, winSize.height/2)];
    [dropObjectEditor setAlpha: 0];
    [dropObjectEditor setZPosition: 1];
    [self addChild: dropObjectEditor];
    
    // Add a label that tells what this is for
    int fontSize = 12;
    if (IS_IPAD) fontSize = 25;
    SKLabelNode *editDropObjectsLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [editDropObjectsLabel setFontColor: [UIColor whiteColor]];
    [editDropObjectsLabel setFontSize: fontSize];
    [editDropObjectsLabel setText: @"Edit Drop Objects"];
    [editDropObjectsLabel setPosition: ccp(0, dropObjectEditor.frame.size.height * 0.25)];
    [dropObjectEditor addChild: editDropObjectsLabel];
    
    // Fade in the editor, fade out the queue,
    // and move the queue to the back
    [dropObjectEditor runAction: [SKAction fadeInWithDuration: 0.2]];
    [dropQueue setEnabled: NO];
    [dropQueue setColorBlendFactor: 0];
    [dropQueue runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
        [dropQueue setZPosition: -1];
    }];
    
    // Fade out the first drop object which isn't in the queue
    [[dropObjects firstObject] removeAllActions];
    [[dropObjects firstObject] runAction: [SKAction fadeOutWithDuration: 0.2]];
    
    // Bring the dimmer up
    [dimmer setZPosition: 0.5];
    
    // Create an array of duplicate drop object textures to add to the
    // scroller since existing drop objects have parents already
    NSMutableArray *dropArrayDuplicate = [[NSMutableArray alloc] init];
    for (SKSpriteNode *node in dropObjects) {
        SKSpriteNodeButton *duplicate = [SKSpriteNodeButton spriteNodeWithTexture: node.texture];
        [duplicate setScale: 0.5];
        if (IS_IPAD) [duplicate setScale: 0.7];
        [duplicate setZoomOnTouchDown: NO];
        [duplicate setDarkenOnTouchDown: YES];
        [duplicate sendDelegate: nil touchEvents: YES];
        [duplicate addTarget: self action: @selector(editDropObject:)];
        
        // Add the children
        for (SKNode *child in node.children) {
            SKNode *childDuplicate = [child copy];
            [duplicate addChild: childDuplicate];
        }
        
        // Add it to the array
        [dropArrayDuplicate insertObject: duplicate atIndex: 0];
    }
    
    // Mask any objects past the body
    SKCropNode *cropNode = [[SKCropNode alloc] init];
    [cropNode setName: @"dropObjects"];
    [cropNode setMaskNode: [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed: @"PopoverBody.png"] size: CGSizeMake(winSize.width * 0.8, winSize.height * 0.3)]];
    [cropNode.maskNode setXScale: 0.95];
    [cropNode setPosition: ccp(0, 0)];
    [dropObjectEditor addChild: cropNode];
    
    // Scroller that the objects are on
    float widthOffset = 360;
    if (IS_IPAD) widthOffset = 600;
    SKScrollLayer *scroller = [SKScrollLayer nodeWithLayers: dropArrayDuplicate widthOffset: widthOffset scene: self size: CGSizeMake(dropObjectEditor.frame.size.width, dropObjectEditor.frame.size.height - dropObjectEditor.frame.size.height * 0.4) showPagesIndicator: NO];
    [scroller setPosition: ccp(-dropObjectEditor.frame.size.width/2, -dropObjectEditor.frame.size.height * 0.35)];
    [scroller setInitialPosition: scroller.position];
    [scroller setDisableNonCenterButtons: NO];
    [scroller setDelegate: self];
    [cropNode addChild: scroller];
    
    // scroll to the right screen
    int pageToScrollTo;
    if (scroller.totalScreens == 1 || scroller.totalScreens == 2)
        pageToScrollTo = 1;
    else
        pageToScrollTo = scroller.totalScreens - 2;
    
    [scroller moveToPage: pageToScrollTo animated: NO];
}

-(void)editDropObject:(SKSpriteNodeButton *)dropObject
{
    if (selectedDropObject) {
        // There's already an object selected, so remove the popover
        SKSpriteNode *theArrow = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"arrow"];
        SKSpriteNode *theBody = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"body"];
        [theArrow runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [theBody runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        
        // Don't create another popover if the tapped object
        // was the one with a popover on it before
        if (selectedDropObject == dropObject) {
            selectedDropObject = nil;
            return;
        }
    }
    
    selectedDropObject = dropObject;
    
    // Create the popover for the + - order and 'X' delete buttons
    SKSpriteNode *arrow = [SKSpriteNode spriteNodeWithImageNamed: @"PopoverArrow.png"];
    [arrow setPosition: ccp([dropObjectEditor convertPoint: dropObject.position fromNode: dropObject.parent].x, -dropObjectEditor.frame.size.height * 0.4)];
    if (IS_IPAD)
        [arrow setPosition: ccp([dropObjectEditor convertPoint: dropObject.position fromNode: dropObject.parent].x, -dropObjectEditor.frame.size.height * 0.3)];
    [arrow setAlpha: 0];
    [arrow setName: @"arrow"];
    [dropObjectEditor addChild: arrow];
    
    SKSpriteNode *body = [SKSpriteNode spriteNodeWithImageNamed: @"DropObjectEditor.png"];
    [body setPosition: ccp([dropObjectEditor convertPoint: dropObject.position fromNode: dropObject.parent].x, -dropObjectEditor.frame.size.height * 0.76)];
    if (IS_IPAD)
        [body setPosition: ccp([dropObjectEditor convertPoint: dropObject.position fromNode: dropObject.parent].x, -dropObjectEditor.frame.size.height * 0.49)];
    [body setAlpha: 0];
    [body setName: @"body"];
    [dropObjectEditor addChild: body];
    
    // Buttons
    SKSpriteNodeButton *minusButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MinusButton.png"];
    [minusButton setName: @"minusButton"];
    [minusButton setPosition: ccp(-body.frame.size.width * 0.3, 0)];
    [minusButton addTarget: self action: @selector(changeOrderOfDropObject:)];
    [minusButton setZoomOnTouchDown: NO];
    [minusButton setDarkenOnTouchDown: YES];
    [body addChild: minusButton];
    
    SKSpriteNodeButton *plusButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PlusButton.png"];
    [plusButton setName: @"plusButton"];
    [plusButton setPosition: ccp(body.frame.size.width * 0.3, 0)];
    [plusButton addTarget: self action: @selector(changeOrderOfDropObject:)];
    [plusButton setZoomOnTouchDown: NO];
    [plusButton setDarkenOnTouchDown: YES];
    [body addChild: plusButton];
    
    SKSpriteNodeButton *deleteButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"DeleteButton.png"];
    [deleteButton setName: @"deleteButton"];
    [deleteButton setPosition: ccp(0, 0)];
    [deleteButton addTarget: self action: @selector(deleteDropObject:)];
    [deleteButton setZoomOnTouchDown: NO];
    [deleteButton setDarkenOnTouchDown: YES];
    [body addChild: deleteButton];

    
    // Fade them in
    [arrow runAction: [SKAction fadeInWithDuration: 0.2]];
    [body runAction: [SKAction fadeInWithDuration: 0.2]];
}
     
-(void)changeOrderOfDropObject:(id)sender
{
    SKSpriteNode *arrow = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"arrow"];
    SKSpriteNode *body = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"body"];
    SKSpriteNodeButton *minusButton = (SKSpriteNodeButton *)[body childNodeWithName: @"minusButton"];
    SKSpriteNodeButton *plusButton = (SKSpriteNodeButton *)[body childNodeWithName: @"plusButton"];
    SKScrollLayer *scroller = (SKScrollLayer *)[selectedDropObject parent];
    
    if (sender == minusButton && selectedDropObject != [scroller.layers firstObject]) {
        // Move the popover
        SKNode *previousDropObject = (SKNode *)[scroller.layers objectAtIndex: [scroller.layers indexOfObject: selectedDropObject] - 1];
        CGPoint pt = [dropObjectEditor convertPoint: previousDropObject.position fromNode: previousDropObject.parent];
        SKAction *movePopover = [SKAction moveToX: pt.x duration: 0.2];
        [movePopover setTimingMode: SKActionTimingEaseInEaseOut];
        
        [arrow runAction: movePopover];
        [body runAction: movePopover];
        
        // Move the actual drop objects in the array, not the duplicates
        SKNode *selectedRealDropObject = [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject]];
        SKNode *previousRealDropObject = [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject] - 1];
        [dropObjects removeObject: selectedRealDropObject];
        [dropObjects insertObject: selectedRealDropObject atIndex: [dropObjects indexOfObject: previousRealDropObject]];
        
        [scroller movePageLayerDownOnePosition: selectedDropObject animated: YES];
    }
    else if (sender == plusButton && selectedDropObject != [scroller.layers lastObject]) {
        // Move the popover
        SKNode *frontDropObject = (SKNode *)[scroller.layers objectAtIndex: [scroller.layers indexOfObject: selectedDropObject] + 1];
        CGPoint pt = [dropObjectEditor convertPoint: frontDropObject.position fromNode: frontDropObject.parent];
        SKAction *movePopover = [SKAction moveToX: pt.x duration: 0.2];
        [movePopover setTimingMode: SKActionTimingEaseInEaseOut];
        
        [arrow runAction: movePopover];
        [body runAction: movePopover];
        
        // Move the actual drop objects in the array, not the duplicates
        SKNode *selectedRealDropObject = [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject]];
        SKNode *nextRealDropObject = [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject] + 1];
        [dropObjects removeObject: nextRealDropObject];
        [dropObjects insertObject: nextRealDropObject atIndex: [dropObjects indexOfObject: selectedRealDropObject]];

        [scroller movePageLayerUpOnePosition: selectedDropObject animated: YES];
    }
}

-(void)deleteDropObject:(id)sender
{
    SKNode *deleteButton = (SKNode *)[[dropObjectEditor childNodeWithName: @"body"] childNodeWithName: @"deleteButton"];
    SKScrollLayer *scroller = (SKScrollLayer *)selectedDropObject.parent;
    if (sender == deleteButton) {
        // Remove the drop object from the array
        if ([littleHippos containsObject: [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject]]])
            [littleHippos removeObject: [dropObjects objectAtIndex: [scroller.layers indexOfObject: selectedDropObject]]];
        
        [dropObjects removeObjectAtIndex: [scroller.layers indexOfObject: selectedDropObject]];
        [scroller removePageLayer: selectedDropObject animated: YES];
        
        // Remove the popover
        SKSpriteNode *theArrow = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"arrow"];
        SKSpriteNode *theBody = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"body"];
        [theArrow runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [theBody runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        selectedDropObject = nil;
    }
}


#pragma mark - Edit Scene Objects
-(void)sceneObjectTapped:(SKSpriteNodeButton *)sender
{
    if (selectedSceneObject && sender != selectedSceneObject) {
        // There's a popover being shown on another object. Remove it
        [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        
        selectedSceneObject = nil;
    }
    
    if (!selectedSceneObject) {
        selectedSceneObject = sender;
        
        // Get the right frame and position. If it's a hippo, get the frame and position of the body
        CGSize senderSize = sender.calculateAccumulatedFrame.size;
        CGPoint senderPos = sender.position;
        if (sender == mamaHippo || [littleHippos containsObject: sender]) {
            senderSize = [sender childNodeWithName: @"body"].frame.size;
            senderPos = [sender convertPoint: [sender childNodeWithName: @"body"].position toNode: self];
        }
        
        // Determine whether or not to show the delete button
        BOOL showDeleteButton = NO;
        if (sender != mamaHippo && ![movingSceneObjects containsObject: mamaHippo]) {
            if ((![littleHippos containsObject: sender] && ![movingSceneObjects containsObject: littleHippos.firstObject]) || ([littleHippos containsObject: sender] && littleHippos.count > 1))
                showDeleteButton = YES;
        }
        
        // Show the popover
        SKSpriteNode *arrow = [SKSpriteNode spriteNodeWithImageNamed: @"PopoverArrow.png"];
        [arrow setName: @"sceneArrow"];
        [arrow setZRotation: M_PI];
        [arrow setZPosition: 0.2];
        [arrow setPosition: ccp(senderPos.x, senderPos.y + senderSize.height/2 + arrow.frame.size.height * 0.4)];
        [arrow setAlpha: 0];
        [self addChild: arrow];
        
        SKSpriteNode *body = [SKSpriteNode spriteNodeWithImageNamed: @"SceneObjectPopoverSmall.png"];
        [body setName: @"small"];
        if (showDeleteButton && movingSceneObjects.count < 2 && ![stationaryObjects containsObject: sender] && ![moveableObjects containsObject: sender]) {
            body = [SKSpriteNode spriteNodeWithImageNamed: @"SceneObjectPopoverMedium.png"];
            [body setName: @"medium"];
        } else if (showDeleteButton && movingSceneObjects.count < 2 && ([stationaryObjects containsObject: sender] || [moveableObjects containsObject: sender])){
            body = [SKSpriteNode spriteNodeWithImageNamed: @"SceneObjectPopoverLarge.png"];
            [body setName: @"large"];
        }
        [body setZPosition: 0.2];
        [body setPosition: ccp(arrow.position.x, arrow.position.y + arrow.frame.size.height/2 + body.frame.size.height*0.35)];
        [body setAlpha: 0];
        [self addChild: body];
        
        // Adjust position of the popover if it's off the screen or intersects buttons
        for (int i = 1, centerOffset = 0; i <= 4; i++) {
            if (CGRectIntersectsRect(body.frame, addObjectButton.frame) || CGRectIntersectsRect(body.frame, optionsButton.frame) || CGRectIntersectsRect(body.frame, dropQueue.frame) || CGRectIntersectsRect(body.frame, pauseButton.frame) || !CGRectContainsRect(self.frame, body.frame)) {
                if (i == 1) {
                    // Try the popover on the right side
                    [arrow setZRotation: M_PI/2];
                    [arrow setPosition: ccp(senderPos.x + senderSize.width/2 + arrow.frame.size.width * 0.4, senderPos.y)];
                    [body setPosition: ccp(arrow.position.x + arrow.frame.size.width/2 + body.frame.size.width * 0.35, arrow.position.y + centerOffset)];
                } else if (i == 2) {
                    // Try the popover on the left side
                    [arrow setZRotation: M_PI*1.5];
                    [arrow setPosition: ccp(senderPos.x - senderSize.width/2 - arrow.frame.size.width * 0.4, senderPos.y)];
                    [body setPosition: ccp(arrow.position.x - arrow.frame.size.width/2 - body.frame.size.width * 0.35, arrow.position.y + centerOffset)];
                } else if (i == 3) {
                    // Put the popover on the bottom
                    [arrow setZRotation: 0];
                    [arrow setPosition: ccp(senderPos.x, senderPos.y - senderSize.height/2 - arrow.frame.size.height * 0.4 + centerOffset)];
                    [body setPosition: ccp(arrow.position.x + centerOffset, arrow.position.y - arrow.frame.size.height/2 - body.frame.size.height*0.35)];
                } else if (i == 4) {
                    // Popover couldn't pass any, so try making the popover offset from the arrow
                    centerOffset += 5;
                    i = 0;
                }
            } else
                // the popover passes, so stop the loop
                break;
        }
        
        // Buttons
        SKSpriteNodeButton *moveButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MoveTab.png"];
        [moveButton setZoomOnTouchDown: NO];
        [moveButton setDarkenOnTouchDown: YES];
        [moveButton addTarget: self action: @selector(showPrecisionMovingTools)];
        [body addChild: moveButton];
        
        // Set the button's position
        if ([body.name isEqual: @"small"])
            [moveButton setPosition: ccp(0, body.frame.size.height * 0.2)];
        else if ([body.name isEqual: @"medium"])
            [moveButton setPosition: ccp(0, body.frame.size.height * 0.23)];
        else if ([body.name isEqual: @"large"])
            [moveButton setPosition: ccp(0, body.frame.size.height * 0.3)];

        // Rotate button if there's only one object selected
        SKSpriteNodeButton *rotateButton;
        if (movingSceneObjects.count < 2) {
            // Only add rotate button if there's one object selected
            rotateButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RotateTab.png"];
            [rotateButton setZoomOnTouchDown: NO];
            [rotateButton setDarkenOnTouchDown: YES];
            [rotateButton addTarget: self action: @selector(showRotateTools)];
            [body addChild: rotateButton];
            
            // Set the button's position
            if ([body.name isEqual: @"small"])
                [rotateButton setPosition: ccp(0, -body.frame.size.height * 0.1)];
            else if ([body.name isEqual: @"medium"])
                [rotateButton setPosition: ccp(0, 0)];
            else if ([body.name isEqual: @"large"])
                [rotateButton setPosition: ccp(0, body.frame.size.height * 0.105)];
        }
        
        // Add a delete button if the sender's not mamaHippo or last littleHippo
        if (showDeleteButton) {
            SKSpriteNodeButton *deleteButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"DeleteTab.png"];
            [deleteButton setZoomOnTouchDown: NO];
            [deleteButton setDarkenOnTouchDown: YES];
            [deleteButton addTarget: self action: @selector(deleteSceneObject)];
            [body addChild: deleteButton];
            
            // Set the button's position
            if ([body.name isEqual: @"small"])
                [deleteButton setPosition: ccp(0, -body.frame.size.height * 0.1)];
            else if ([body.name isEqual: @"medium"])
                [deleteButton setPosition: ccp(0, -body.frame.size.height * 0.23)];
            else if ([body.name isEqual: @"large"])
                [deleteButton setPosition: ccp(0, -body.frame.size.height * 0.09)];
        }
        
        // Show a label to indicate if the object is moveable or stationary
        // and add a button to make it the opposite if the body is large
        if ([body.name isEqual: @"large"]) {
            int fontSize = 8;
            if (IS_IPAD) fontSize = 15;
            
            SKLabelNode *indicatorLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
            [indicatorLabel setFontColor: [UIColor colorWithWhite: 0.5 alpha: 1]];
            [indicatorLabel setFontSize: fontSize];
            [indicatorLabel setPosition: ccp(0, -body.frame.size.height * 0.43)];
            [body addChild: indicatorLabel];
            
            if ([moveableObjects containsObject: sender])
                [indicatorLabel setText: @"Moveable"];
            else if ([stationaryObjects containsObject: sender])
                [indicatorLabel setText: @"Stationary"];
            
            SKSpriteNodeButton *makeOppositeButton;
            if ([stationaryObjects containsObject: sender])
                makeOppositeButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MakeMoveableTab.png"];
            else if ([moveableObjects containsObject: sender])
                makeOppositeButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"MakeStationaryTab.png"];
            
            [makeOppositeButton setZoomOnTouchDown: NO];
            [makeOppositeButton setDarkenOnTouchDown: YES];
            [makeOppositeButton addTarget: self action: @selector(makeObjectStationaryOrMoveable:)];
            [makeOppositeButton setPosition: ccp(0, -body.frame.size.height * 0.285)];
            [makeOppositeButton setUserData: [NSMutableDictionary dictionaryWithObject: indicatorLabel forKey: @"indicatorLabel"]];
            [body addChild: makeOppositeButton];
        }
        
        [body setName: @"sceneBody"];
        
        [arrow runAction: [SKAction fadeInWithDuration: 0.2]];
        [body runAction: [SKAction fadeInWithDuration: 0.2]];
        [rotateButton runAction: [SKAction fadeInWithDuration: 0.2]];
        [moveButton runAction: [SKAction fadeInWithDuration: 0.2]];
    } else if (sender == selectedSceneObject) {
        // The popover was being shown on this object. Fade it out
        [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        
        selectedSceneObject = nil;
    }
}

-(void)showRotateTools
{
    rotatingSceneObject = YES;
    
    CGPoint objectCenter = selectedSceneObject.position;
    SKNode *object = selectedSceneObject;
    if (selectedSceneObject == mamaHippo || [littleHippos containsObject: selectedSceneObject]) {
        objectCenter = [selectedSceneObject convertPoint: [selectedSceneObject childNodeWithName: @"body"].position toNode: self];
        object = [selectedSceneObject childNodeWithName: @"body"];
    }
    
    // Fade out the popover
    [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    
    // Add the rotate tool and fade it in
    SKSpriteNode *rotateTool = [SKSpriteNode spriteNodeWithImageNamed: @"RotateTool.png"];
    [rotateTool setName: @"rotateTool"];
    [rotateTool setScale: scaleLevel];
    [rotateTool setAlpha: 0];
    [rotateTool setZPosition: 1.7];
    [rotateTool setColor: [UIColor blackColor]];
    [rotateTool setPosition: objectCenter];
    [self addChild: rotateTool];

    // The shadow won't rotate with the tool
    SKSpriteNode *rotateToolShadow = [SKSpriteNode spriteNodeWithImageNamed: @"RotateToolShadow.png"];
    [rotateToolShadow setName: @"rotateToolShadow"];
    [rotateToolShadow setScale: scaleLevel];
    [rotateToolShadow setAlpha: 0];
    [rotateToolShadow setZPosition: 1.6];
    [rotateToolShadow setPosition: ccp(objectCenter.x + 3.5, objectCenter.y - 3.5)];
    if (IS_IPAD)
        [rotateToolShadow setPosition: ccp(objectCenter.x + 6, objectCenter.y - 6)];
    [self addChild: rotateToolShadow];
    
    // Show the degrees of the object
    SKLabelNode *degreesLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
    [degreesLabel setName: @"degreesLabel"];
    [degreesLabel setFontColor: [UIColor whiteColor]];
    [degreesLabel setFontSize: 15];
    if (IS_IPAD)
        [degreesLabel setFontSize: 30];
    [degreesLabel setText: [NSString stringWithFormat: @"%.1f\u00B0", RADIANS_TO_DEGREES(selectedSceneObject.zRotation)]];
    [degreesLabel setAlpha: 0];
    [degreesLabel setPosition: ccp(objectCenter.x, objectCenter.y + object.frame.size.height * 0.6)];
    [degreesLabel setScale: 1.3]; // To put the label a comfortable amount away from the edge of the screen
    if (!CGRectContainsRect(self.frame, degreesLabel.calculateAccumulatedFrame))
        [degreesLabel setPosition: ccp(objectCenter.x, objectCenter.y - object.frame.size.height * 0.7)];
    [degreesLabel setScale: 1];
    [degreesLabel setZPosition: 2];
    [self addChild: degreesLabel];

    [rotateTool runAction: [SKAction fadeInWithDuration: 0.2]];
    [rotateToolShadow runAction: [SKAction fadeInWithDuration: 0.2]];
    [degreesLabel runAction: [SKAction fadeInWithDuration: 0.2]];
    
    // Move the dimmer to the front and put the hippo above the dimmer
    [dimmer setZPosition: 1.5];
    [selectedSceneObject setZPosition: 1.8];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.3 duration: 0.2]];
}

-(void)showPrecisionMovingTools
{
    // get the right object and object center. If it's a hippo, it will be the body child
    CGPoint objectCenter = selectedSceneObject.position;
    SKNode *object = selectedSceneObject;
    if (selectedSceneObject == mamaHippo || [littleHippos containsObject: selectedSceneObject]) {
        objectCenter = [selectedSceneObject convertPoint: [selectedSceneObject childNodeWithName: @"body"].position toNode: self];
        object = [selectedSceneObject childNodeWithName: @"body"];
    }

    // Fade out the popover
    [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];

    // Create the node where the arrows will be
    SKNode *parent = [SKNode node];
    [parent setAlpha: 0];
    [parent setName: @"Arrows"];
    
    SKSpriteNodeButton *upArrow = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackBackgroundButton.png"];
    [upArrow setName: @"up"];
    [upArrow setZRotation: M_PI * 1.5];
    [upArrow setPosition: ccp(0, upArrow.frame.size.height * 0.5)];
    [upArrow setZoomOnTouchDown: NO];
    [upArrow setDarkenOnTouchDown: YES];
    [upArrow addTarget: self action: @selector(arrowPressed:)];
    [parent addChild: upArrow];
    
    SKSpriteNodeButton *downArrow = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackBackgroundButton.png"];
    [downArrow setName: @"down"];
    [downArrow setZRotation: M_PI * 0.5];
    [downArrow setPosition: ccp(0, -downArrow.frame.size.height * 0.5)];
    [downArrow setZoomOnTouchDown: NO];
    [downArrow setDarkenOnTouchDown: YES];
    [downArrow addTarget: self action: @selector(arrowPressed:)];
    [parent addChild: downArrow];
    
    SKSpriteNodeButton *leftArrow = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackBackgroundButton.png"];
    [leftArrow setName: @"left"];
    [leftArrow setPosition: ccp(-leftArrow.frame.size.width, 0)];
    [leftArrow setZoomOnTouchDown: NO];
    [leftArrow setDarkenOnTouchDown: YES];
    [leftArrow addTarget: self action: @selector(arrowPressed:)];
    [parent addChild: leftArrow];

    SKSpriteNodeButton *rightArrow = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackBackgroundButton.png"];
    [rightArrow setName: @"right"];
    [rightArrow setZRotation: M_PI];
    [rightArrow setPosition: ccp(rightArrow.frame.size.width, 0)];
    [rightArrow setZoomOnTouchDown: NO];
    [rightArrow setDarkenOnTouchDown: YES];
    [rightArrow addTarget: self action: @selector(arrowPressed:)];
    [parent addChild: rightArrow];

    [parent setPosition: ccp(objectCenter.x, objectCenter.y + object.frame.size.height/2 + rightArrow.frame.size.height * 0.8)];
    [self addChild: parent];
    
    // Adjust position of the arrows if their off the screen or intersects buttons
    for (int i = 1; i <= 3; i++) {
        if (CGRectIntersectsRect(parent.calculateAccumulatedFrame, addObjectButton.frame) || CGRectIntersectsRect(parent.calculateAccumulatedFrame, optionsButton.frame) || CGRectIntersectsRect(parent.calculateAccumulatedFrame, dropQueue.frame) || CGRectIntersectsRect(parent.calculateAccumulatedFrame, pauseButton.frame) || !CGRectContainsRect(self.frame, parent.calculateAccumulatedFrame)) {
            if (i == 1) {
                // Try the arrows on the right side
                [parent setPosition: ccp(objectCenter.x + object.frame.size.width/2 + rightArrow.frame.size.width * 0.8, 0)];
            } else if (i == 2) {
                // Try the arrows on the left side
                [parent setPosition: ccp(objectCenter.x - object.frame.size.width/2 - rightArrow.frame.size.width * 0.8, 0)];
            } else if (i == 3) {
                // Put the arrows on the bottom since all else has failed
                [parent setPosition: ccp(objectCenter.x, objectCenter.y - object.frame.size.height/2 - rightArrow.frame.size.height * 0.8)];
            }
        } else
            // the popover passes, so stop the loop
            break;
    }

    // Fade in the arrows
    [parent runAction: [SKAction fadeInWithDuration: 0.2]];
}

-(void)deleteSceneObject
{
    // remove it from it's array
    if ([littleHippos containsObject: selectedSceneObject])
        [littleHippos removeObject: selectedSceneObject];
    else if ([moveableObjects containsObject: selectedSceneObject])
        [moveableObjects removeObject: selectedSceneObject];
    else if ([stationaryObjects containsObject: selectedSceneObject])
        [stationaryObjects removeObject: selectedSceneObject];
    
    if (movingSceneObjects.count > 1) {
        for (SKNode *obj in movingSceneObjects) {
            // remove it from it's array
            if ([littleHippos containsObject: obj])
                [littleHippos removeObject: obj];
            else if ([moveableObjects containsObject: obj])
                [moveableObjects removeObject: obj];
            else if ([stationaryObjects containsObject: obj])
                [stationaryObjects removeObject: obj];
            
            // Fade out the object and remove it from the scene
            [obj runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        }
    }
    
    // Fade out the object and remove it from the scene
    [selectedSceneObject runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    
    // Fade out the popover
    [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
    
    selectedSceneObject = nil;
    movingSceneObjects = nil;
}

-(void)makeObjectStationaryOrMoveable:(SKSpriteNodeButton *)button
{
    if ([moveableObjects containsObject: selectedSceneObject]) {
        [moveableObjects removeObject: selectedSceneObject];
        [stationaryObjects addObject: selectedSceneObject];
        [(SKSpriteNode *)selectedSceneObject setColor: [UIColor blackColor]];
        [(SKSpriteNode *)selectedSceneObject runAction: [SKAction colorizeWithColorBlendFactor: 0.1 duration: 0.2]];
        [button setTexture: [SKTexture textureWithImageNamed: @"MakeMoveableTab.png"]];
        [(SKLabelNode *)[button.userData objectForKey: @"indicatorLabel"] setText: @"Stationary"];
    } else if ([stationaryObjects containsObject: selectedSceneObject]) {
        [stationaryObjects removeObject: selectedSceneObject];
        [moveableObjects addObject: selectedSceneObject];
        [(SKSpriteNode *)selectedSceneObject runAction: [SKAction colorizeWithColorBlendFactor: 0 duration: 0.2]];
        [button setTexture: [SKTexture textureWithImageNamed: @"MakeStationaryTab.png"]];
        [(SKLabelNode *)[button.userData objectForKey: @"indicatorLabel"] setText: @"Moveable"];
    }
}

-(void)arrowPressed:(SKSpriteNodeButton *)sender
{
    // Precision move the object
    if ([sender.name isEqual: @"up"])
        [selectedSceneObject setPosition: ccp(selectedSceneObject.position.x, selectedSceneObject.position.y + 1)];
    else if ([sender.name isEqual: @"down"])
        [selectedSceneObject setPosition: ccp(selectedSceneObject.position.x, selectedSceneObject.position.y - 1)];
    else if ([sender.name isEqual: @"left"])
        [selectedSceneObject setPosition: ccp(selectedSceneObject.position.x - 1, selectedSceneObject.position.y)];
    else if ([sender.name isEqual: @"right"]) 
        [selectedSceneObject setPosition: ccp(selectedSceneObject.position.x + 1, selectedSceneObject.position.y)];
}

#pragma mark - Other
-(void)scaleObjectsWithScaleDifference:(float)scaleDifference
{
    float proposedScaleLevel = scaleLevel + scaleDifference;
    NSString *roundedScale = [NSString stringWithFormat: @"%.2f", proposedScaleLevel];
    proposedScaleLevel = [roundedScale floatValue];
    
    // Can't scale below 0.65. Can't scale above 1
    if (proposedScaleLevel < 0.65 || proposedScaleLevel > 1)
        return;
    
    // Scale level looks OK.

    // Remove the physics bodies if needed, set the original origin of each object, and scale the object
    NSMutableArray *allSceneObjects = [NSMutableArray arrayWithArray: stationaryObjects];
    [allSceneObjects addObjectsFromArray: moveableObjects];
    [allSceneObjects addObjectsFromArray: littleHippos];
    [allSceneObjects addObject: mamaHippo];

    for (SKNode *obj in allSceneObjects) {
        if (!objectsNeedPhysicsBodies) {
            [obj setPhysicsBody: nil];
            
            if ([littleHippos containsObject: obj] || obj == mamaHippo) {
                for (SKNode *bodyPart in obj.children)
                    [bodyPart setPhysicsBody: nil];
            }
        }

        
        CGPoint origin = ccp(obj.frame.origin.x / scaleLevel, obj.frame.origin.y / scaleLevel);
        if (![obj.userData objectForKey: @"originalOrigin"])
            [obj setUserData: [NSMutableDictionary dictionaryWithObject: [NSValue valueWithCGPoint: origin] forKey:@"originalOrigin"]];
        else
            origin = [[obj.userData objectForKey: @"originalOrigin"] CGPointValue];
        
        [obj setScale: proposedScaleLevel];
        [obj setPosition: ccp(origin.x * proposedScaleLevel + obj.frame.size.width/2, origin.y * proposedScaleLevel + obj.frame.size.height/2)];

        // Don't let the objects go below the ground y
        CGRect objFrame = obj.frame;
        if (objFrame.origin.y < groundY)
            [obj setPosition: ccp(obj.position.x, groundY + obj.frame.size.height/2)];
    }
    
    if (!objectsNeedPhysicsBodies) {
        [self.physicsWorld removeAllJoints];
        objectsNeedPhysicsBodies = YES;
    }
    
    // Scale the drop object that's not in the dropQueue
    [(SKNode *)[dropObjects lastObject] setScale: proposedScaleLevel];
}

-(void)giveObjectsPhysicsBodies
{
    // Give the little hippos their physics bodies
    for (SKNode *hippo in littleHippos) {
        SKNode *body = [hippo childNodeWithName: @"body"];
        SKNode *head = [hippo childNodeWithName: @"head"];
        SKNode *leg1 = [hippo childNodeWithName: @"leg1"];
        SKNode *leg2 = [hippo childNodeWithName: @"leg2"];
        
        [body setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: body.frame.size.width/2 * hippo.xScale]];
        [body.physicsBody setAffectedByGravity: NO];
        [body.physicsBody setAllowsRotation: NO];
        [body.physicsBody setRestitution: 0];
        [body.physicsBody setCategoryBitMask: LittleHippoCategory];
        [body.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        [head setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: 1 * hippo.xScale]];
        [head.physicsBody setAffectedByGravity: NO];
        
        [leg1 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: CGSizeMake(leg1.frame.size.width * hippo.xScale, leg1.frame.size.height * hippo.xScale)]];
        [leg1.physicsBody setAffectedByGravity: NO];
        [leg1.physicsBody setRestitution: 0];
        [leg1.physicsBody setCategoryBitMask: LegCategory];
        [leg1.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        [leg2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: CGSizeMake(leg2.frame.size.width * hippo.xScale, leg2.frame.size.height * hippo.xScale)]];
        [leg2.physicsBody setAffectedByGravity: NO];
        [leg2.physicsBody setRestitution: 0];
        [leg2.physicsBody setCategoryBitMask: LegCategory];
        [leg2.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
        
        SKPhysicsJointFixed *leg1Joint = [SKPhysicsJointFixed jointWithBodyA: leg1.physicsBody bodyB: body.physicsBody anchor: [hippo convertPoint:leg1.position toNode: self]];
        [self.physicsWorld addJoint: leg1Joint];
        SKPhysicsJointFixed *leg2Joint = [SKPhysicsJointFixed jointWithBodyA: leg2.physicsBody bodyB: body.physicsBody anchor: [hippo convertPoint:leg2.position toNode: self]];
        [self.physicsWorld addJoint: leg2Joint];
        SKPhysicsJointFixed *headJoint = [SKPhysicsJointFixed jointWithBodyA: head.physicsBody bodyB: body.physicsBody anchor: [hippo convertPoint: head.position toNode: self]];
        [self.physicsWorld addJoint: headJoint];
    }
    
    // Give the mama hippo her physics bodies
    SKNode *body = [mamaHippo childNodeWithName: @"body"];
    SKNode *head = [mamaHippo childNodeWithName: @"head"];
    SKNode *leg1 = [mamaHippo childNodeWithName: @"leg1"];
    SKNode *leg2 = [mamaHippo childNodeWithName: @"leg2"];
    
    [body setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: body.frame.size.width/2 * mamaHippo.xScale]];
    [body.physicsBody setAffectedByGravity: NO];
    [body.physicsBody setAllowsRotation: NO];
    [body.physicsBody setRestitution: 0];
    [body.physicsBody setCategoryBitMask: MamaHippoCategory];
    [body.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | MamaHippoCategory | GroundCategory];
    
    [head setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: 1 * mamaHippo.xScale]];
    [head.physicsBody setAffectedByGravity: NO];
    
    [leg1 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: CGSizeMake(leg1.frame.size.width * mamaHippo.xScale, leg1.frame.size.height * mamaHippo.xScale)]];
    [leg1.physicsBody setAffectedByGravity: NO];
    [leg1.physicsBody setRestitution: 0];
    [leg1.physicsBody setCategoryBitMask: LegCategory];
    [leg1.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | GroundCategory];
    
    [leg2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: CGSizeMake(leg2.frame.size.width * mamaHippo.xScale, leg2.frame.size.height * mamaHippo.xScale)]];
    [leg2.physicsBody setAffectedByGravity: NO];
    [leg2.physicsBody setRestitution: 0];
    [leg2.physicsBody setCategoryBitMask: LegCategory];
    [leg2.physicsBody setCollisionBitMask: LittleHippoCategory | ObjectCategory | GroundCategory];
    
    SKPhysicsJointFixed *leg1Joint = [SKPhysicsJointFixed jointWithBodyA: leg1.physicsBody bodyB: body.physicsBody anchor: [mamaHippo convertPoint: leg1.position toNode: self]];
    [self.physicsWorld addJoint: leg1Joint];
    SKPhysicsJointFixed *leg2Joint = [SKPhysicsJointFixed jointWithBodyA: leg2.physicsBody bodyB: body.physicsBody anchor: [mamaHippo convertPoint: leg2.position toNode: self]];
    [self.physicsWorld addJoint: leg2Joint];
    SKPhysicsJointFixed *headJoint = [SKPhysicsJointFixed jointWithBodyA: head.physicsBody bodyB: body.physicsBody anchor: [mamaHippo convertPoint: head.position toNode: self]];
    [self.physicsWorld addJoint: headJoint];
    
    
    // Give the objects their physics bodies
    NSMutableArray *allSceneObjects = [NSMutableArray arrayWithArray: stationaryObjects];
    [allSceneObjects addObjectsFromArray: moveableObjects];
    
    for (SKNode *obj in allSceneObjects) {
        [obj setPhysicsBody: nil];
        float zRotation = obj.zRotation;
        [obj setZRotation: 0];
        [obj setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: obj.frame.size]];
        [obj.physicsBody setAffectedByGravity: NO];
        [obj.physicsBody setRestitution: 0];
        [obj.physicsBody setCategoryBitMask: ObjectCategory];
        [obj.physicsBody setCollisionBitMask: LittleHippoCategory | MamaHippoCategory | ObjectCategory | LegCategory | GroundCategory];
        [obj setZRotation: zRotation];
    }
}

-(void)scrollLayerDidBeginScrolling:(id)scroller
{
    if ([(SKScrollLayer *)scroller parent] == [dropObjectEditor childNodeWithName: @"dropObjects"] && [dropObjectEditor childNodeWithName: @"arrow"]) {
        // Showing a popover on a drop object. Remove it
        SKSpriteNode *theArrow = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"arrow"];
        SKSpriteNode *theBody = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"body"];
        [theArrow runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        [theBody runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
        selectedDropObject = nil;
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    numberOfTouchesOnScreen = (int)event.allTouches.count;
    
    if (numberOfTouchesOnScreen == 1 && firstTouch) {
        // set the firstTouch variable to nil since the touch
        // beginning right now should be the firstTouch
        firstTouch = nil;
    } else if (numberOfTouchesOnScreen == 2 && secondTouch) {
        // set the secondTouch variable to nil since the touch
        // beginning right now should be the secondTouch
        secondTouch = nil;
    }
    
    for (UITouch *touch in touches) {
        SKNode *node = [self nodeAtPoint: [touch locationInNode: self]];
        
        // For the second touch
        if (firstTouch && touch != firstTouch && !secondTouch && ![self childNodeWithName: @"PauseMenu"] && !rotatingSceneObject) {
            secondTouch = touch;
            startingDistanceBetweenTouches = ccpDistance([firstTouch locationInNode: self], [secondTouch locationInNode: self]);
        }
        
        // For the first touch
        if (!firstTouch) {
            if (popoverArrow && node == dimmer && ![self childNodeWithName: @"PauseMenu"]) {
                // Showing a popover and touch began outside the popover
                
                // Save the scroller's center page
                [self saveScrollersCenterPages];
                
                // Fade out the popover
                [popoverArrow runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
                    [popoverArrow removeFromParent];
                    popoverArrow = nil;
                }];
                [popoverBody runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
                    [popoverBody removeFromParent];
                    popoverBody = nil;
                }];
                
                // Move the dimmer to the back
                [dimmer setZPosition: -1];
                
                // Prep for multiple object selection
                firstTouch = touch;
                touchStart = [touch locationInNode: self];
            } else if (dropObjectEditor && node == dimmer && ![self childNodeWithName: @"PauseMenu"]) {
                // Fade out the drop objects editor
                [dropObjectEditor removeAllActions];
                [dropObjectEditor runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
                    [dropObjectEditor removeAllChildren];
                    [dropObjectEditor removeFromParent];
                    dropObjectEditor = nil;
                }];
                
                // Update the queue, fade it in, and bring it to the front
                [self updateDropQueue];
                [dropQueue setZPosition: 1];
                [dropQueue removeAllActions];
                [dropQueue runAction: [SKAction fadeInWithDuration: 0.2]];
                [dropQueue setEnabled: YES];
                
                // Move the dimmer to the back
                [dimmer setZPosition: -1];
                
            } else if (node == dropObjectEditor && [dropObjectEditor childNodeWithName: @"arrow"] && ![self childNodeWithName: @"PauseMenu"]) {
                // Fade out the drop object order/deletion popover
                SKSpriteNode *theArrow = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"arrow"];
                SKSpriteNode *theBody = (SKSpriteNode *)[dropObjectEditor childNodeWithName: @"body"];
                [theArrow runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                [theBody runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                selectedDropObject = nil;
            } else if (!firstTouch && !rotatingSceneObject && (!movingSceneObjects || movingSceneObjects.count == 1)) {
                // Moving a lone scene object
                hipposNeedPositionAdjustments = YES;
                
                if (![movingSceneObjects containsObject: node] || ![movingSceneObjects containsObject: node.parent]) {
                    // Tapped outside of the moving scene object
                    for (SKSpriteNode *object in movingSceneObjects)
                        [object setColorBlendFactor: 0];
                    movingSceneObjects = nil;
                }
                
                // If there's any precision move arrows fade them out
                if ([self childNodeWithName: @"Arrows"]) {
                    [[self childNodeWithName: @"Arrows"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    selectedSceneObject = nil;
                }
                
                // Find out which object was selected and assign that as the selectedSceneObject
                NSMutableArray *allSceneObjects = [NSMutableArray arrayWithArray: stationaryObjects];
                [allSceneObjects addObjectsFromArray: moveableObjects];
                [allSceneObjects addObjectsFromArray: littleHippos];
                [allSceneObjects addObject: mamaHippo];

                for (SKNode *obj in allSceneObjects) {
                    if (node == obj || [node inParentHierarchy: obj]) {
                        movingSceneObjects = [NSMutableArray arrayWithObject: obj];
                        firstTouch = touch;
                        [obj setUserData: [NSMutableDictionary dictionaryWithObject: [NSValue valueWithCGPoint: ccp([touch locationInNode: self].x - obj.position.x, [touch locationInNode: self].y - obj.position.y)] forKey: @"touchOffset"]];
                        return;
                    }
                }

                // None of the scene objects were tapped. Check if there's a popover on a
                // scene object, and if there is, fade it out
                if ([self childNodeWithName: @"sceneArrow"] && node != [self childNodeWithName: @"sceneArrow"] && node != [self childNodeWithName: @"sceneBody"]) {
                    [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    
                    selectedSceneObject = nil;
                }
                
                // Prep for multiple object selection
                firstTouch = touch;
                touchStart = [touch locationInNode: self];
            } else if (movingSceneObjects.count > 1 && ![self childNodeWithName: @"PauseMenu"]) {
                // Moving multiple scene objects
                hipposNeedPositionAdjustments = YES;
                
                // If there's any precision move arrows fade them out
                if ([self childNodeWithName: @"Arrows"]) {
                    [[self childNodeWithName: @"Arrows"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    selectedSceneObject = nil;
                }
                
                if ([movingSceneObjects containsObject: node] || [movingSceneObjects containsObject: node.parent]) {
                    // Touch began on a moving object
                    firstTouch = touch;
                    
                    // Store the offset of the touch from each node in the movingSceneObjects array
                    for (SKNode *node in movingSceneObjects)
                        [node setUserData: [NSMutableDictionary dictionaryWithObject: [NSValue valueWithCGPoint: ccp([touch locationInNode: self].x - node.position.x, [touch locationInNode: self].y - node.position.y)] forKey: @"touchOffset"]];
                } else {
                    // Touch began somewhere else on the screen, so unselect them
                    for (SKSpriteNodeButton *obj in movingSceneObjects) {
                        // Decolorize them
                        float colorBlendFactor = 0;
                        if ([stationaryObjects containsObject: obj])
                            colorBlendFactor = 0.1;
                        
                        [obj runAction: [SKAction colorizeWithColorBlendFactor: colorBlendFactor duration: 0.2]];
                        for (SKSpriteNode *child in obj.children)
                            [child runAction: [SKAction colorizeWithColorBlendFactor: colorBlendFactor duration: 0.2]];
                    }
                    
                    // Fade out any selection popovers
                    if ([self childNodeWithName: @"sceneArrow"] && node != [self childNodeWithName: @"sceneArrow"] && node != [self childNodeWithName: @"sceneBody"]) {
                        [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                        [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                        
                        selectedSceneObject = nil;
                    }
                    
                    movingSceneObjects = nil;
                    
                    // Prep for multiple object selection again
                    firstTouch = touch;
                    touchStart = [touch locationInNode: self];
                }
            } else if (rotatingSceneObject && !firstTouch) {
                // Rotate the scene object
                hipposNeedPositionAdjustments = YES;
                firstTouch = touch;
                if (node == [self childNodeWithName: @"rotateTool"]) {
                    // Start rotating the object
                    
                    // Make sure we've got the right object
                    SKNode *objectToRotate = selectedSceneObject;
                    CGPoint objectPosition = selectedSceneObject.position;
                    if (selectedSceneObject == mamaHippo || [littleHippos containsObject: selectedSceneObject]) {
                        objectToRotate = [selectedSceneObject childNodeWithName: @"body"];
                        objectPosition = [selectedSceneObject convertPoint: objectToRotate.position toNode: self];
                    }
                    
                    initialVector = ccp([touch locationInNode: self].x - objectPosition.x, [touch locationInNode: self].y - objectPosition.y);
                    initialZRotation = objectToRotate.zRotation;
                    [(SKSpriteNode *)[self childNodeWithName: @"rotateTool"] setColorBlendFactor: 0.5];
                } else {
                    // Fade out the rotate tool and dimmer
                    [[self childNodeWithName: @"rotateTool"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    [[self childNodeWithName: @"rotateToolShadow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    [[self childNodeWithName: @"degreesLabel"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    [dimmer runAction: [SKAction fadeOutWithDuration: 0.2] completion: ^{
                        [dimmer setZPosition: -1];
                        [selectedSceneObject setZPosition: 0];
                        selectedSceneObject = nil;
                    }];
                    rotatingSceneObject = NO;
                }
            }
        }
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        // If there's two touches, scale the objects
        if (firstTouch && secondTouch) {
            float difference = ccpDistance([firstTouch locationInNode: self], [secondTouch locationInNode: self]) - startingDistanceBetweenTouches;
            float scaleDifference = difference/450;
            
            [self scaleObjectsWithScaleDifference: scaleDifference];
        }
        
        // If there's only one touch
        if (firstTouch && !secondTouch) {
            if (touch == firstTouch && movingSceneObjects && !selectionRect) {
                // Move the scene object
                
                // Remove any popover on this or other scene objects
                if ([self childNodeWithName: @"sceneArrow"]) {
                    [[self childNodeWithName: @"sceneArrow"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    [[self childNodeWithName: @"sceneBody"] runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                    
                    selectedSceneObject = nil;
                }
                
                CGPoint location = [touch locationInNode: self];
                
                for (SKSpriteNodeButton *object in movingSceneObjects) {
                    CGPoint touchOffset = [[object.userData objectForKey: @"touchOffset"] CGPointValue];
                    [object setPosition: ccp(location.x - touchOffset.x, location.y - touchOffset.y)];
                    
                    // Disable the button since it's moving
                    [object setEnabled: NO];
                    
                    // Colorize the scene object and its children
                    [object setColorBlendFactor: 0.5];
                    for (SKSpriteNode *child in object.children) {
                        [child setColor: [UIColor blackColor]];
                        [child setColorBlendFactor: 0.5];
                    }
                    
                    // Check if the object is out of bounds of the frame, and if it is, correct it
                    // It can't be a hippo
                    CGRect selfFrame = CGRectMake(self.frame.origin.x, groundY, self.frame.size.width, self.frame.size.height);
                    CGRect objectFrame = object.frame;
                    if (!CGRectContainsRect(selfFrame, objectFrame) && object != mamaHippo && ![littleHippos containsObject: object]) {
                        if (!CGRectContainsPoint(selfFrame, objectFrame.origin) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), objectFrame.origin.y)) && !CGRectContainsPoint(selfFrame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))))
                            // The object is in the bottom left corner
                            [object setPosition: ccp(objectFrame.size.width/2, objectFrame.size.height/2 + groundY)];
                        else if (!CGRectContainsPoint(selfFrame, objectFrame.origin) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), CGRectGetMaxY(objectFrame))) && CGRectContainsPoint(selfFrame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))))
                            // The object is in the bottom right corner
                            [object setPosition: ccp(selfFrame.size.width - objectFrame.size.width/2, objectFrame.size.height/2 + groundY)];
                        else if (!CGRectContainsPoint(selfFrame, objectFrame.origin) && CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), objectFrame.origin.y)) && !CGRectContainsPoint(selfFrame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), CGRectGetMaxY(objectFrame))))
                            // The object is in the top left corner
                            [object setPosition: ccp(objectFrame.size.width/2, selfFrame.size.height - objectFrame.size.height * 0.45)];
                        else if (CGRectContainsPoint(selfFrame, objectFrame.origin) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), objectFrame.origin.y)) && !CGRectContainsPoint(selfFrame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), CGRectGetMaxY(objectFrame))))
                            // The object is in the top right corner
                            [object setPosition: ccp(selfFrame.size.width - objectFrame.size.width/2, selfFrame.size.height - objectFrame.size.height * 0.45)];
                        else if (!CGRectContainsPoint(selfFrame, objectFrame.origin) && CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), objectFrame.origin.y)))
                            // The object is too far to the left
                            [object setPosition: ccp(objectFrame.size.width/2, object.position.y)];
                        else if (CGRectContainsPoint(selfFrame, objectFrame.origin) && !CGRectContainsPoint(selfFrame, ccp(CGRectGetMaxX(objectFrame), objectFrame.origin.y)))
                            // The object is too far to the right
                            [object setPosition: ccp(selfFrame.size.width - objectFrame.size.width/2, object.position.y)];
                        else if (!CGRectContainsPoint(selfFrame, objectFrame.origin) && CGRectContainsPoint(selfFrame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))))
                            // The object is too far down
                            [object setPosition: ccp(object.position.x, objectFrame.size.height/2 + groundY)];
                        else if (CGRectContainsPoint(selfFrame, objectFrame.origin) && !CGRectContainsPoint(self.frame, ccp(objectFrame.origin.x, CGRectGetMaxY(objectFrame))))
                            // The object is too far up
                            [object setPosition: ccp(object.position.x, selfFrame.size.height - objectFrame.size.height * 0.45)];
                    }
                }
            } else if (touch == firstTouch && rotatingSceneObject) {
                // Make sure we're rotating the right object
                SKNode *objectToRotate = selectedSceneObject;
                CGPoint objectPosition = selectedSceneObject.position;
                if (selectedSceneObject == mamaHippo || [littleHippos containsObject: selectedSceneObject]) {
                    objectToRotate = [selectedSceneObject childNodeWithName: @"body"];
                    objectPosition = [selectedSceneObject convertPoint: objectToRotate.position toNode: self];
                }
                
                // Rotate the scene object and rotate tool
                CGPoint vector = ccp([touch locationInNode: self].x - objectPosition.x, [touch locationInNode: self].y - objectPosition.y);
                float rotation = ccpToAngle(vector) - ccpToAngle(initialVector) + initialZRotation;
                
                // 'fix' the zRotation if it's above 360 degrees or below 0
                if (rotation > M_PI*2)
                    rotation = fmodf(rotation, M_PI*2);
                if (rotation < 0)
                    rotation = M_PI*2 + rotation;
                
                // If the rotation is within 3 degrees of a 45 degree mark, put it at that mark
                float degreesRotation = RADIANS_TO_DEGREES(rotation);
                for (int i = 0; i <= 360; i += 90) {
                    if (i == 0 && (degreesRotation < i + 3 || degreesRotation > 357)) {
                        rotation = DEGREES_TO_RADIANS(0);
                        degreesRotation = 0;
                        break;
                    }
                    else if (degreesRotation < i + 3 && degreesRotation > i - 3) {
                        rotation = DEGREES_TO_RADIANS(i);
                        degreesRotation = i;
                        break;
                    }
                }
                
                [objectToRotate setZRotation: rotation];
                [[self childNodeWithName: @"rotateTool"] setZRotation: rotation];
                
                // Update the degrees label
                [(SKLabelNode *)[self childNodeWithName: @"degreesLabel"] setText: [NSString stringWithFormat: @"%.1f\u00B0", degreesRotation]];
            } else if (touch == firstTouch && touchStart.x && touchStart.y) {
                // Multiple selection of objects
                if (!selectionRect) {
                    selectionRect = [[SKShapeNode alloc] init];
                    [selectionRect setPath: CGPathCreateWithRect(CGRectMake(touchStart.x, touchStart.y, [touch locationInNode: self].x - touchStart.x, [touch locationInNode: self].y - touchStart.y), NULL)];
                    [selectionRect setStrokeColor: [UIColor clearColor]];
                    [selectionRect setFillColor: [UIColor colorWithWhite: 0 alpha: 0.25]];
                    [self addChild: selectionRect];
                }
                
                CGRect rect = CGRectMake(touchStart.x, touchStart.y, [touch locationInNode: self].x - touchStart.x, [touch locationInNode: self].y - touchStart.y);
                [selectionRect setPath: CGPathCreateWithRect(rect, NULL)];
                
                // Initialize the movingSceneObjects array
                movingSceneObjects = [[NSMutableArray alloc] init];
                
                NSMutableArray *allSceneObjects = [NSMutableArray arrayWithArray: stationaryObjects];
                [allSceneObjects addObjectsFromArray: moveableObjects];
                [allSceneObjects addObjectsFromArray: littleHippos];
                [allSceneObjects addObject: mamaHippo];

                for (SKSpriteNode *obj in allSceneObjects) {
                    if (CGRectIntersectsRect(obj.calculateAccumulatedFrame, rect)) {
                        // If the selectionRect intersects the object, darken it
                        [movingSceneObjects addObject: obj];
                        [obj setColor: [UIColor blackColor]];
                        [obj setColorBlendFactor: 0.5];
                        for (SKSpriteNode *child in obj.children) {
                            [child setColor: [UIColor blackColor]];
                            [child setColorBlendFactor: 0.5];
                        }
                    } else {
                        if ([movingSceneObjects containsObject: obj])
                            [movingSceneObjects removeObject: obj];
                        
                        [obj setColorBlendFactor: 0];
                        for (SKSpriteNode *child in obj.children)
                            [child setColorBlendFactor: 0];
                    }
                }
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    numberOfTouchesOnScreen -= touches.count;
    
    for (UITouch *touch in touches) {
        
        // Make sure the hippos' bodies are in the same position as their button parent
        if (hipposNeedPositionAdjustments) {
            SKNode *mamaBody = [mamaHippo childNodeWithName: @"body"];
            SKNode *mamaHead = [mamaHippo childNodeWithName: @"head"];
            SKNode *mamaLeg1 = [mamaHippo childNodeWithName: @"leg1"];
            SKNode *mamaLeg2 = [mamaHippo childNodeWithName: @"leg2"];
            CGPoint headDifference = ccp(mamaHead.position.x - mamaBody.position.x, mamaHead.position.y - mamaBody.position.y);
            CGPoint leg1Diference = ccp(mamaLeg1.position.x - mamaBody.position.x, mamaLeg1.position.y - mamaBody.position.y);
            CGPoint leg2Difference = ccp(mamaLeg2.position.x - mamaBody.position.x, mamaLeg2.position.y - mamaBody.position.y);
            [mamaHippo setPosition: [mamaHippo convertPoint: mamaBody.position toNode: self]];
            [mamaBody setPosition: [self convertPoint: mamaHippo.position toNode: mamaHippo]];
            [mamaHead setPosition: ccp(mamaBody.position.x + headDifference.x, mamaBody.position.y + headDifference.y)];
            [mamaLeg1 setPosition: ccp(mamaBody.position.x + leg1Diference.x, mamaBody.position.y + leg1Diference.y)];
            [mamaLeg2 setPosition: ccp(mamaBody.position.x + leg2Difference.x, mamaBody.position.y + leg2Difference.y)];
            
            for (SKNode *hippo in littleHippos) {
                SKNode *body = [hippo childNodeWithName: @"body"];
                SKNode *head = [hippo childNodeWithName: @"head"];
                SKNode *leg1 = [hippo childNodeWithName: @"leg1"];
                SKNode *leg2 = [hippo childNodeWithName: @"leg2"];
                CGPoint headDiff = ccp(head.position.x - body.position.x, head.position.y - body.position.y);
                CGPoint leg1Diff = ccp(leg1.position.x - body.position.x, leg1.position.y - body.position.y);
                CGPoint leg2Diff = ccp(leg2.position.x - body.position.x, leg2.position.y - body.position.y);
                [hippo setPosition: [hippo convertPoint: body.position toNode: self]];
                [body setPosition: [self convertPoint: hippo.position toNode: hippo]];
                [head setPosition: ccp(body.position.x + headDiff.x, body.position.y + headDiff.y)];
                [leg1 setPosition: ccp(body.position.x + leg1Diff.x, body.position.y + leg1Diff.y)];
                [leg2 setPosition: ccp(body.position.x + leg2Diff.x, body.position.y + leg2Diff.y)];
            }
            
            hipposNeedPositionAdjustments = NO;
        }
                
        // set to yes if we need to forget all touches
        BOOL clearFirstTouch = NO;
        
        // Second touch
        if (secondTouch && numberOfTouchesOnScreen <= 1) {
            // There was a secondTouch but now there's only one touch on the screen
            // Forget all touches
            firstTouch = nil;
            secondTouch = nil;
            clearFirstTouch = YES;
        }
        
        // First touch
        if ((touch == firstTouch && !rotatingSceneObject && movingSceneObjects.count == 1) || clearFirstTouch) {
            // Done moving the scene object
            SKSpriteNodeButton *object = movingSceneObjects.firstObject;
            [object setColorBlendFactor: 0];
            for (SKSpriteNode *child in object.children)
                [child setColorBlendFactor: 0];
                
            if ([stationaryObjects containsObject: object]) {
                // set the color blend factor to 0.1 since it's stationary
                [object setColorBlendFactor: 0.1];
                for (SKSpriteNode *child in object.children)
                    [child setColorBlendFactor: 0.1];
            }
            
            [object setEnabled: YES];
            
            movingSceneObjects = nil;
            firstTouch = nil;
            touchStart = CGPointZero;
            [selectionRect runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
            selectionRect = nil;
        } else if (touch == firstTouch && !rotatingSceneObject && movingSceneObjects.count != 1) {
            if (movingSceneObjects.count == 0)
                movingSceneObjects = nil;
            else {
                // Darken and enable all the objects
                for (SKSpriteNodeButton *obj in movingSceneObjects) {
                    [obj setEnabled: YES];
                    [obj setColor: [UIColor blackColor]];
                    [obj setColorBlendFactor: 0.5];
                    for (SKSpriteNode *child in obj.children) {
                        [child setColor: [UIColor blackColor]];
                        [child setColorBlendFactor: 0.5];
                    }
                }
            }
            
            if (selectionRect) {
                [selectionRect runAction: [SKAction sequence: @[[SKAction fadeOutWithDuration: 0.2], [SKAction removeFromParent]]]];
                selectionRect = nil;
            }
            firstTouch = nil;
            touchStart = CGPointZero;
        } else if (touch == firstTouch && rotatingSceneObject) {
            [(SKSpriteNode *)[self childNodeWithName: @"rotateTool"] setColorBlendFactor: 0];
            firstTouch = nil;
        }
    }
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Same as touchesEnded:withEvent:
    [self touchesEnded: touches withEvent: event];
}

-(void)update:(NSTimeInterval)currentTime
{
    // Give the objects their physics bodies if needed and there's only 1 or less touches on the screen
    if (objectsNeedPhysicsBodies && numberOfTouchesOnScreen <= 1) {
        [self giveObjectsPhysicsBodies];
        objectsNeedPhysicsBodies = NO;
        
        // Set the scaleLevel to the actual object scale level
        scaleLevel = mamaHippo.xScale;
    }
}
@end