//
//  SKSpriteNodeButton.m
//  Lost Hippos
//
//  Created by Josh Braun on 8/15/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "SKSpriteNodeButton.h"

@implementation SKSpriteNodeButton

@synthesize zoomOnTouchDown, darkenOnTouchDown, selected, touch, enabled;

+(id)spriteNodeWithImageNamed:(NSString *)name
{
    SKSpriteNodeButton *button = [super spriteNodeWithImageNamed: name];
    if (button) {
        [button setUserInteractionEnabled: YES];
        
        // Zoom is default to yes
        [button setZoomOnTouchDown: YES];
        
        // Darkening is default to no
        [button setDarkenOnTouchDown: NO];
        
        [button setEnabled: YES];
    }
    return button;
}

+(id)spriteNodeWithTexture:(SKTexture *)texture
{
    SKSpriteNodeButton *button = [super spriteNodeWithTexture: texture];
    if (button) {
        [button setUserInteractionEnabled: YES];
        
        // Zoom is default to yes
        [button setZoomOnTouchDown: YES];
        
        // Darkening is default to no
        [button setDarkenOnTouchDown: NO];
        
        [button setEnabled: YES];
    }
    return button;
}

#pragma mark - Sending Delegate touch events
-(void)sendDelegate:(id)theDelegate touchEvents:(BOOL)sendEvents
{
    delegate = theDelegate;
    sendTouchEvents = sendEvents;
}

-(void)stopSendingDelegateTouchEvents
{
    sendTouchEvents = NO;
}

#pragma mark - Adding/Removing target-action pairs
-(void)addTarget:(id)target action:(SEL)action
{
    if (!targetActions)
        targetActions = [[NSMutableArray alloc] init];
    
    // Store the target-action pair in a dictionary and
    // add it to the target-action array
    NSDictionary *targetActionPair = [NSDictionary dictionaryWithObjectsAndKeys: target, @"target", [NSValue valueWithPointer: action], @"action", nil];
    
    [targetActions addObject: targetActionPair];
}

-(void)removeTarget:(id)target action:(SEL)action
{
    if (targetActions) {
        for (NSDictionary *dict in targetActions) {
            if ([dict objectForKey: @"target"] == target && [[dict objectForKey: @"action"] pointerValue] == action) {
                [targetActions removeObject: dict];
                break;
            }
        }
    }
}

#pragma mark - Adding block actions
-(void)addBlock:(void (^)(void))block
{
    if (!targetActions)
        targetActions = [[NSMutableArray alloc] init];
    
    // Store the block
    [targetActions addObject: block];
}

-(void)removeBlocks
{
    for (void(^block)(void) in targetActions)
        [targetActions removeObject: block];
}

#pragma mark - Handling different touch events
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (sendTouchEvents && !delegate)
        delegate = self.parent;
    
    if (sendTouchEvents && [delegate respondsToSelector: @selector(touchesBegan:withEvent:)])
        [delegate touchesBegan: touches withEvent: event];
    
    if (enabled) {
        [self setTouch: [touches anyObject]];
        
        // Let's anyone know that this button is selected
        selected = YES;
        
        // Darken if we need to
        if (darkenOnTouchDown) {
            originalColorBlendFactor = self.colorBlendFactor;
            [self setColor: [UIColor blackColor]];
            [self setColorBlendFactor: 0.5];
            for (SKNode *child in self.children) {
                if ([child isKindOfClass: [SKSpriteNode class]]) {
                    [(SKSpriteNode *)child setColor: [UIColor blackColor]];
                    [(SKSpriteNode *)child setColorBlendFactor: 0.5];
                } else if ([child isKindOfClass: [SKLabelNode class]]) {
                    [(SKLabelNode *)child setColor: [UIColor blackColor]];
                    [(SKLabelNode *)child setColorBlendFactor: 0.5];
                }
            }
        }
        
        // Zoom if we need to
        if (zoomOnTouchDown) {
            if (!originalXScale && !originalYScale) {
                originalXScale = self.xScale;
                originalYScale = self.yScale;
            }
            [self runAction: [SKAction scaleBy: 1.1 duration: 0.1]];
        }
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (sendTouchEvents && [delegate respondsToSelector: @selector(touchesMoved:withEvent:)])
        [delegate touchesMoved: touches withEvent: event];
    
    if (enabled) {
        for (UITouch *theTouch in touches) {
            SKNode *node = [[self scene] nodeAtPoint: [theTouch locationInNode: [self scene]]];
            if (selected && theTouch == touch && node != self) {
                // Touches have moved off our button
                selected = NO;
                
                // Undarken if we need to
                if (darkenOnTouchDown)
                    [self setColorBlendFactor: 0];
                
                // Unzoom if we need to
                if (zoomOnTouchDown)
                    [self runAction: [SKAction scaleXTo: originalXScale y: originalYScale duration: 0.1]];
            } else if (!selected && theTouch == touch && node == self) {
                // Touches have moved on our button
                selected = YES;
                
                // Darken if we need to
                if (darkenOnTouchDown) {
                    [self setColor: [UIColor blackColor]];
                    [self setColorBlendFactor: 0.5];
                }
                
                // Zoom if we need to
                if (zoomOnTouchDown) {
                    originalXScale = self.xScale;
                    originalYScale = self.yScale;
                    [self runAction: [SKAction scaleBy: 1.1 duration: 0.1]];
                }
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (selected && enabled) {
        selected = NO;
        
        // Undarken if we need to
        if (darkenOnTouchDown) {
            [self setColorBlendFactor: originalColorBlendFactor];
            for (SKNode *child in self.children) {
                if ([child isKindOfClass: [SKSpriteNode class]])
                    [(SKSpriteNode *)child setColorBlendFactor: originalColorBlendFactor];
                else if ([child isKindOfClass: [SKLabelNode class]])
                    [(SKLabelNode *)child setColorBlendFactor: originalColorBlendFactor];
            }
        }

        // Unzoom if we need to
        if (zoomOnTouchDown)
            [self runAction: [SKAction scaleXTo: originalXScale y: originalYScale duration: 0.1]];

        // Call our target-action pairs/blocks
        for (id action in targetActions) {
            if ([action isKindOfClass: [NSDictionary class]]) {
                id target = [action objectForKey: @"target"];
                SEL actionSel = [[action objectForKey: @"action"] pointerValue];
                
                // Ignore the performSelector warning
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                if ([target respondsToSelector: actionSel])
                    [target performSelector: actionSel withObject: self];
            } else {
                // Must be a block
                void(^block)(void) = action;
                block();
            }
        }
    }
    
    if (sendTouchEvents && [delegate respondsToSelector: @selector(touchesEnded:withEvent:)])
        [delegate touchesEnded: touches withEvent: event];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Undarken if we need to
    if (darkenOnTouchDown) {
        [self setColorBlendFactor: originalColorBlendFactor];
        for (SKNode *child in self.children) {
            if ([child isKindOfClass: [SKSpriteNode class]])
                [(SKSpriteNode *)child setColorBlendFactor: originalColorBlendFactor];
            else if ([child isKindOfClass: [SKLabelNode class]])
                [(SKLabelNode *)child setColorBlendFactor: originalColorBlendFactor];
        }
    }
    
    // Unzoom if we need to
    if (zoomOnTouchDown)
        [self runAction: [SKAction scaleXTo: originalXScale y: originalYScale duration: 0.1]];
    
    if (sendTouchEvents && [delegate respondsToSelector: @selector(touchesCancelled:withEvent:)])
        [delegate touchesCancelled: touches withEvent: event];
}

-(SKSpriteNode *)spriteNodeVersion
{
    SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture: self.texture];
    [sprite setPosition: self.position];
    [sprite setXScale: self.xScale];
    [sprite setYScale: self.yScale];
    [sprite setZPosition: self.zPosition];
    [sprite setZRotation: self.zRotation];
    [sprite setAlpha: self.alpha];
    [sprite setHidden: self.hidden];
    [sprite setName: self.name];
    [sprite setSpeed: self.speed];
    [sprite setPaused: self.paused];
    [sprite setPhysicsBody: self.physicsBody];
    [sprite setUserData: self.userData];
    [sprite setColor: self.color];
    [sprite setColorBlendFactor: self.colorBlendFactor];
    [sprite setBlendMode: self.blendMode];
    [sprite setUserInteractionEnabled: NO];
    
    for (SKNode *child in self.children) {
        SKNode *copy = [child copy];
        [sprite addChild: copy];
    }
    return sprite;
}

-(void)setScale:(CGFloat)scale
{
    self.xScale = scale;
    self.yScale = scale;
    originalXScale = 0;
    originalYScale = 0;
}

#pragma mark - NSCopying
-(id)copyWithZone:(NSZone *)zone
{
    SKSpriteNodeButton *copy = [super copyWithZone: zone];
    [copy setUserInteractionEnabled: YES];
    [copy setZoomOnTouchDown: zoomOnTouchDown];
    [copy setDarkenOnTouchDown: darkenOnTouchDown];
    [copy setEnabled: enabled];
    
    return copy;
}

@end
