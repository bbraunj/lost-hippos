//
//  LevelInfoStore.m
//  Lost Hippos
//
//  Created by Josh Braun on 9/29/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "LevelInfoStore.h"

@implementation LevelInfoStore
@synthesize furthestLevelReached;

static LevelInfoStore *sharedStore;
+(LevelInfoStore *)sharedStore
{
    if (!sharedStore) {
        sharedStore = [[LevelInfoStore alloc] init];
        [sharedStore loadLevelInfo];
    }
   
    return sharedStore;
}

#pragma mark - Scores
-(void)setScore:(int)score forLevel:(int)levelNumber
{
    [scores setObject: [NSNumber numberWithInt: score] forKey: [NSString stringWithFormat: @"Level%iScore", levelNumber]];
}

-(int)scoreForLevel:(int)levelNumber
{
    return [[scores objectForKey: [NSString stringWithFormat: @"Level%iScore", levelNumber]] intValue];
}

#pragma mark - Saving/Loading Level Info
-(void)saveLevelInfo
{
    // Get the path of the doc directory and save the levelinfo
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
    NSString *scoresFilePath = [documentDirectory stringByAppendingPathComponent: @"snfo"];
    NSString *furthestLevelFilePath = [documentDirectory stringByAppendingPathComponent: @"flnfo"];

    [NSKeyedArchiver archiveRootObject: scores toFile: scoresFilePath];
    [NSKeyedArchiver archiveRootObject: [NSNumber numberWithInt: furthestLevelReached] toFile: furthestLevelFilePath];
}

-(void)loadLevelInfo
{
    // Get the path of the doc directory and levelinfo
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
    NSString *scoresFilePath = [documentDirectory stringByAppendingPathComponent: @"snfo"];
    NSString *furthestLevelFilePath = [documentDirectory stringByAppendingPathComponent: @"flnfo"];

    // Load the info
    scores = [NSKeyedUnarchiver unarchiveObjectWithFile: scoresFilePath];
    furthestLevelReached = [[NSKeyedUnarchiver unarchiveObjectWithFile: furthestLevelFilePath] intValue];
    
    // If there isn't any, make it
    if (!scores)
        scores = [[NSMutableDictionary alloc] init];
    if (!furthestLevelReached)
        furthestLevelReached = 1;
}

@end
