//
//  SKScrollLayer.h
//  Lost Hippos
//
//  Created by Josh Braun on 8/16/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol SKScrollLayerDelegate <NSObject>

@optional
-(void)scrollLayerDidBeginScrolling:(id)scroller;
-(void)scrollLayerDidEndScrolling:(id)scroller;

@end

@interface SKScrollLayer : SKShapeNode
{
	// Holds the current width of the screen substracting offset.
	CGFloat scrollWidth_;
	
	// Holds the current page being displayed.
	int currentScreen_;
	
	// A count of the total screens available.
	int totalScreens_;
	
	// The x coord of initial point the user starts their swipe.
	CGFloat startSwipe_;
	
	// For what distance user must slide finger to start scrolling menu.
	CGFloat minimumTouchLengthToSlide_;
	
	// For what distance user must slide finger to change the page.
	CGFloat minimumTouchLengthToChangePage_;
	
	// Whenever show or not gray/white dots under scrolling content.
	BOOL showPagesIndicator_;
	
	// Internal state of scrollLayer (scrolling or idle).
	int state_;
    BOOL buttonsDisabled;
    
    // Make sure the touch is the first touch
    UITouch *firstTouch;
    
    // Used in setting the position of the scroller while touchesMoved and moveToPage:animated:
    CGPoint initialPosition;
	
}
@property(readwrite, assign) CGFloat minimumTouchLengthToSlide;
@property(readwrite, assign) CGFloat minimumTouchLengthToChangePage;
@property(readwrite, assign) BOOL showPagesIndicator;
@property(readonly) int totalScreens;
@property(readonly) int currentScreen;
@property(nonatomic) BOOL disableNonCenterButtons;

@property(nonatomic)id<SKScrollLayerDelegate> delegate;

@property(nonatomic) NSMutableArray *points;
@property(nonatomic) NSMutableArray *layers;

-(void)setInitialPosition:(CGPoint)pos;

+(id) nodeWithLayers:(NSArray *)theLayers widthOffset: (int) widthOffset scene:(SKScene *)theScene size:(CGSize)size showPagesIndicator:(BOOL)showIndicators;
-(id) initWithLayers:(NSArray *)theLayers widthOffset: (int) widthOffset scene:(SKScene *)theScene size:(CGSize)size showPagesIndicator:(BOOL)showIndicators;

-(void) moveToPage:(int)page animated:(BOOL)animated;
-(void) movePageLayerUpOnePosition:(SKNode *)pageLayer animated:(BOOL)animated;
-(void) movePageLayerDownOnePosition:(SKNode *)pageLayer animated:(BOOL)animated;
-(void) removePageLayer:(SKNode *)pageLayer animated:(BOOL)animated;

@end
