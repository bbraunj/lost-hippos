//
//  AllLevelsImporter.h
//  Lost Hippos
//
//  Created by Josh Braun on 10/13/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//


// Use this class to import all the level
// files into the main menu
#import <Foundation/Foundation.h>
#import "Level1.h"
#import "Level2.h"

@interface AllLevelsImporter : NSObject

@end
