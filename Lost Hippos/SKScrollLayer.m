//
//  SKScrollLayer.m
//  Lost Hippos
//
//  Created by Josh Braun on 8/16/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "SKScrollLayer.h"
#import "SKSpriteNodeButton.h"

@implementation SKScrollLayer

enum
{
	kCCScrollLayerStateIdle,
	kCCScrollLayerStateSliding,
};

@synthesize minimumTouchLengthToSlide = minimumTouchLengthToSlide_;
@synthesize minimumTouchLengthToChangePage = minimumTouchLengthToChangePage_;
@synthesize totalScreens = totalScreens_;
@synthesize currentScreen = currentScreen_;
@synthesize showPagesIndicator = showPagesIndicator_;
@synthesize disableNonCenterButtons;
@synthesize points, layers;
@synthesize delegate;

+(id) nodeWithLayers:(NSArray *)theLayers widthOffset: (int) widthOffset scene:(SKScene *)theScene size:(CGSize)size showPagesIndicator:(BOOL)showIndicators
{
	return [[self alloc] initWithLayers: theLayers widthOffset:widthOffset scene: theScene size: size showPagesIndicator: showIndicators];
}

-(id) initWithLayers:(NSArray *)theLayers widthOffset: (int) widthOffset scene:(SKScene *)theScene size:(CGSize)size showPagesIndicator:(BOOL)showIndicators
{
	if ( (self = [super init]) )
	{
        layers = [NSMutableArray arrayWithArray: theLayers];
        buttonsDisabled = NO;
        disableNonCenterButtons = YES; // by default
        
		// Enable touches.
		self.userInteractionEnabled = YES;
		
		// Set default minimum touch length to scroll.
		self.minimumTouchLengthToSlide = size.width * 0.05;
		self.minimumTouchLengthToChangePage = size.width * 0.09;
		
		// Show indicator by default.
		self.showPagesIndicator = showIndicators;
		
		// Set up the starting variables
		currentScreen_ = 1;
        		
		// offset added to show preview of next/previous screens
		scrollWidth_ = size.width - widthOffset;
		
		// Loop through the array and add the screens
		int i = 0;
		for (SKNode *l in layers)
		{
			l.position = ccp((i*scrollWidth_ + size.width/2),size.height/2);
            if (l.userData)
                [l.userData setObject: [NSNumber numberWithInt: i + 1] forKey: @"pageNumber"];
            else
                [l setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithInt: i + 1] forKey: @"pageNumber"]];
			[self addChild:l];
			i++;
		}
		
		// Setup a count of the available screens
		totalScreens_ = (int)[layers count];
		
        if (self.showPagesIndicator)
        {
            // Prepare Points Array
            CGFloat n = (CGFloat)totalScreens_; //< Total points count in CGFloat.
            CGFloat pY = 30; //< Points y-coord in parent coord sys.
            CGFloat d = 16.0f; //< Distance between points.
            points = [[NSMutableArray alloc] init];
            
            // Adjust dots positions if it's an iPad
            if (IS_IPAD) {
                pY = 70;
                d = 25.0f;
            }

            for (int i=0; i < totalScreens_; ++i)
            {
                CGFloat pX = 0.5f * size.width + d * ( (CGFloat)i - 0.5f*(n-1.0f) );
                [points addObject: [NSValue valueWithCGPoint: ccp(pX, pY)]];
            }
            
            
            NSMutableArray *dots = [[NSMutableArray alloc] init];
            
            // Add the black dots
            for (NSValue *pointValue in points) {
                CGPoint point = [pointValue CGPointValue];
                
                SKSpriteNode *dot = [SKSpriteNode spriteNodeWithImageNamed: @"BlackDot.png"];
                [dot setPosition: point];
                [dot setZPosition: -10];
                [theScene addChild: dot];
                
                [dots addObject: dot];
            }
            
            // Add the white dot
            CGPoint point = [[points objectAtIndex: currentScreen_ - 1] CGPointValue];
            SKSpriteNode *whiteDot = [SKSpriteNode spriteNodeWithImageNamed: @"WhiteDot.png"];
            [whiteDot setPosition: point];
            [whiteDot setZPosition: -10];
            [theScene addChild: whiteDot];
            [dots addObject: whiteDot];
            
            points = dots;
        }
	}
	return self;
}

-(void)setInitialPosition:(CGPoint)pos
{
    initialPosition = pos;
}

#pragma mark Pages Control

-(void) moveToPage:(int)page animated:(BOOL)animated
{
    if (animated) {
        SKAction *changePage = [SKAction moveTo: ccp(-((page-1)*scrollWidth_) + initialPosition.x ,self.position.y) duration: 0.3];
        [self runAction:changePage completion: ^{
            SKSpriteNode *whiteDot = [points lastObject];
            SKSpriteNode *currentPageDot = [points objectAtIndex: page - 1];
            [whiteDot setPosition: currentPageDot.position];
        }];
    } else {
        [self setPosition: ccp(-((page-1)*scrollWidth_) + initialPosition.x ,self.position.y)];
        SKSpriteNode *whiteDot = [points lastObject];
        SKSpriteNode *currentPageDot = [points objectAtIndex: page - 1];
        [whiteDot setPosition: currentPageDot.position];
    }
	currentScreen_ = page;
    
    // Disable any pages that aren't the one in the center
    if (disableNonCenterButtons) {
        for (int i = 1; i <= layers.count; i++) {
            SKSpriteNodeButton *button = (SKSpriteNodeButton *)[layers objectAtIndex: i - 1];
            if (i != page || ([button.userData objectForKey: @"unlocked"] && ![[button.userData objectForKey: @"unlocked"] boolValue]))
                [button setEnabled: NO];
            else 
                [button setEnabled: YES];
        }
    }
}

-(void)movePageLayerUpOnePosition:(SKNode *)pageLayer animated:(BOOL)animated
{
    if ([layers containsObject: pageLayer] && pageLayer != [layers lastObject]) {
        // Get the layer in front of this one
        SKNode *layerInFront = [layers objectAtIndex: [layers indexOfObject: pageLayer] + 1];
        if (animated) {
            // Move the objects
            SKAction *moveFrontLayerDown1 = [SKAction moveToX: pageLayer.position.x duration: 0.2];
            [moveFrontLayerDown1 setTimingMode: SKActionTimingEaseInEaseOut];
            SKAction *moveLayerUp1 = [SKAction moveToX: layerInFront.position.x duration: 0.2];
            [moveLayerUp1 setTimingMode: SKActionTimingEaseInEaseOut];
            
            [layerInFront runAction: moveFrontLayerDown1];
            [pageLayer runAction: moveLayerUp1];
        } else {
            CGPoint frontLayerPos = layerInFront.position;
            [layerInFront setPosition: pageLayer.position];
            [pageLayer setPosition: frontLayerPos];
        }
        
        // Move the object in the model layers array
        int frontLayerIndex = (int)[layers indexOfObject: layerInFront];
        int pageLayerIndex = (int)[layers indexOfObject: pageLayer];
        [layers removeObject: layerInFront];
        [layers insertObject: layerInFront atIndex: pageLayerIndex];
        [layers removeObject: pageLayer];
        [layers insertObject: pageLayer atIndex: frontLayerIndex];
    }
}

-(void)movePageLayerDownOnePosition:(SKNode *)pageLayer animated:(BOOL)animated
{
    if ([layers containsObject: pageLayer] && pageLayer != [layers firstObject]) {
        // Get the layer behind this one
        SKNode *layerBehind = [layers objectAtIndex: [layers indexOfObject: pageLayer] - 1];
        if (animated) {
            // Move the objects
            SKAction *moveBehindLayerUp1 = [SKAction moveToX: pageLayer.position.x duration: 0.2];
            [moveBehindLayerUp1 setTimingMode: SKActionTimingEaseInEaseOut];
            SKAction *moveLayerDown1 = [SKAction moveToX: layerBehind.position.x duration: 0.2];
            [moveLayerDown1 setTimingMode: SKActionTimingEaseInEaseOut];
            
            [layerBehind runAction: moveBehindLayerUp1];
            [pageLayer runAction: moveLayerDown1];
        } else {
            CGPoint frontLayerPos = layerBehind.position;
            [layerBehind setPosition: pageLayer.position];
            [pageLayer setPosition: frontLayerPos];
        }
        
        // Move the object in the model layers array
        int frontLayerIndex = (int)[layers indexOfObject: layerBehind];
        int pageLayerIndex = (int)[layers indexOfObject: pageLayer];
        [layers removeObject: layerBehind];
        [layers insertObject: layerBehind atIndex: pageLayerIndex];
        [layers removeObject: pageLayer];
        [layers insertObject: pageLayer atIndex: frontLayerIndex];
    }
}

-(void)removePageLayer:(SKNode *)pageLayer animated:(BOOL)animated
{
    if ([layers containsObject: pageLayer]) {
        if (animated) {
            // Animate the removal of the object
            [pageLayer runAction: [SKAction group: @[[SKAction moveByX: 0 y: pageLayer.frame.size.height * 2 duration: 0.3], [SKAction fadeOutWithDuration: 0.3]]] completion: ^{ [pageLayer removeFromParent]; }];
            
            // Move the layers in front of this object back
            if (pageLayer != [layers lastObject]) {
                for (int i = (int)[layers indexOfObject: pageLayer] + 1; i < layers.count; i++) {
                    CGPoint previousObjPosition = [(SKNode *)[layers objectAtIndex: i - 1] position];
                    [(SKNode *)[layers objectAtIndex: i] runAction: [SKAction moveToX: previousObjPosition.x duration: 0.3]];
                }
            }
            
            [layers removeObject: pageLayer];
        }
    }
}

#pragma mark Touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!firstTouch) {
        UITouch *touch = [touches anyObject];
        CGPoint touchPoint = [touch locationInNode: self.scene];
        firstTouch = touch;
        
        startSwipe_ = touchPoint.x;
        state_ = kCCScrollLayerStateIdle;
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch) {
            CGPoint touchPoint = [touch locationInNode: self.scene];
            
            // If finger is dragged for more distance then minimum - start sliding and cancel pressed buttons.
            // Of course only if we not already in sliding mode
            if ( (state_ != kCCScrollLayerStateSliding)
                && (fabsf(touchPoint.x-startSwipe_) >= self.minimumTouchLengthToSlide) )
            {
                state_ = kCCScrollLayerStateSliding;
                
                // Avoid jerk after state change.
                startSwipe_ = touchPoint.x;
            }
            
            if (state_ == kCCScrollLayerStateSliding) {
                // Send a message to the delegate
                if (delegate && [delegate respondsToSelector: @selector(scrollLayerDidBeginScrolling:)])
                    [delegate performSelector: @selector(scrollLayerDidBeginScrolling:) withObject: self];
                
                // Disable the buttons since we're scrolling
                if (!buttonsDisabled) {
                    for (SKNode *node in layers) {
                        if ([node isKindOfClass: [SKSpriteNodeButton class]]) {
                            [(SKSpriteNodeButton *)node setEnabled: NO];
                            buttonsDisabled = YES;
                        }
                    }
                }
                
                self.position = ccp((-(currentScreen_-1)*scrollWidth_)+(touchPoint.x-startSwipe_) + initialPosition.x,self.position.y);
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch) {
            firstTouch = nil;
            CGPoint touchPoint = [touch locationInNode: self.scene];
            
            // Send a message to the delegate
            if (delegate && [delegate respondsToSelector: @selector(scrollLayerDidEndScrolling:)])
                [delegate performSelector: @selector(scrollLayerDidEndScrolling:) withObject: self];

            // Reenable the buttons
            if (buttonsDisabled) {
                for (SKNode *node in layers) {
                    if ([node isKindOfClass: [SKSpriteNodeButton class]]) {
                        [(SKSpriteNodeButton *)node setEnabled: YES];
                        [(SKSpriteNodeButton *)node setColorBlendFactor: 0];
                        buttonsDisabled = NO;
                    }
                }
            }
            
            int newX = touchPoint.x;
            
            if ( (newX - startSwipe_) < -self.minimumTouchLengthToChangePage && (currentScreen_+1) <= totalScreens_ )
            {
                [self moveToPage: currentScreen_+1 animated: YES];
            }
            else if ( (newX - startSwipe_) > self.minimumTouchLengthToChangePage && (currentScreen_-1) > 0 )
            {
                [self moveToPage: currentScreen_-1 animated: YES];
            }
            else
            {
                [self moveToPage:currentScreen_ animated: YES];
            }
        }
    }
}

@end
