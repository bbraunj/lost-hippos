//
//  JRBViewController.m
//  Lost Hippos
//
//  Created by Josh Braun on 7/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "JRBViewController.h"
#import "GCHelper.h"
#import "MainMenu.h"
#import "Level1.h"

@implementation JRBViewController

- (void)viewWillLayoutSubviews
{
    // Use this method instead of viewDidLoad so the
    // scene size will be in landscape, not portrait
    [super viewWillLayoutSubviews];
    
    
    // Configure the view.
    skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    if (!skView.scene) {
        // Create and configure the scene.
        MainMenu *scene = [MainMenu sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFit;
    
        // Present the scene.
        [skView presentScene:scene];
    }
    
    
    // Give the GCHelper our SKView so it can notify
    // the scene of authentication changes
    [[GCHelper sharedHelper] setView: skView];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

// Remove the status bar
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Buttons
- (IBAction)gameCenterPressed
{
    // Make sure we're authenticated
    if ([[GKLocalPlayer localPlayer] isAuthenticated]) {
        // Present a GameCenter View Controller
        GKGameCenterViewController *gameCenterVC = [[GKGameCenterViewController alloc] init];
        [gameCenterVC setGameCenterDelegate: self];
        [self presentViewController: gameCenterVC animated: YES completion: nil];
    } else {
        // If not, authenticate
        [[GCHelper sharedHelper] authenticateLocalUser];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction)settingsPressed:(id)sender
{

}

- (IBAction)shopPressed:(id)sender
{

}

- (IBAction)levels1_20Pressed:(id)sender {
}

- (IBAction)levels21_40Pressed:(id)sender {
}

- (IBAction)levels41_60Pressed:(id)sender {
}

- (IBAction)levels61_80Pressed:(id)sender {
}

- (IBAction)backButtonPressed:(id)sender
{

}
@end
