//
//  main.m
//  Lost Hippos
//
//  Created by Josh Braun on 7/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JRBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JRBAppDelegate class]));
    }
}
