//
//  LevelPlayer.h
//  Lost Hippos
//
//  Created by Josh Braun on 11/15/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class SKSpriteNodeButton;
@class LevelCreator;

@interface LevelPlayer : SKScene
{
    CGSize winSize;
    SKTextureAtlas *atlas;
    float scaleLevel;
    BOOL creatorMode;
    BOOL levelComplete;
    
    // Maximum distance the littleHippo can be away
    // from the mamaHippo to complete the level
    float maxDistance;
    
    // Hippos
    NSArray *littleHippos;
    SKSpriteNode *mamaHippo;
    
    // Level Objects
    NSMutableArray *dropObjects;
    NSMutableArray *droppedObjects;
    NSArray *stationaryObjects;
    NSArray *moveableObjects;

    // Backgrounds
    NSArray *backgrounds;

    // Misc.
    SKSpriteNode *dropZone, *dropQueue;
    SKSpriteNodeButton *pauseButton, *restartButton;
    SKLabelNode *scoreLabel;
        
    // Pause Menu
    SKSpriteNode *pauseMenu;
    SKShapeNode *dimmer;
    SKSpriteNodeButton *playButton, *restartPauseButton, *levelSelectButton, *soundButton;
    
    // Score Menu
    SKSpriteNode *scoreMenu;
    
    // Store the first touch
    UITouch *firstTouch;
    
    // Have we let the object go?
    BOOL objectLetGo;
    
    // Should we let the object go?
    // - dont let it go if it's intersecting any
    //   other physicsBodies
    BOOL canLetObjectGo;
    
    // Have we already told the littleHippo to rotate?
    BOOL canLittleHippoRotate;
    
    int numberOfLittleHipposWithinDistance;
}

@property (weak) LevelCreator *levelCreator;

+(LevelPlayer *)sceneWithSize:(CGSize)size creatorMode:(BOOL)creatorMode scaleLevel:(float)scale littleHippos:(NSArray *)littleHippos mamaHippo:(SKSpriteNode *)mama stationaryObjects:(NSArray *)stationary moveableObjects:(NSArray *)moveable dropObjects:(NSArray *)dropObjects backgrounds:(NSArray *)bgs;
-(id)initWithSize:(CGSize)size creatorMode:(BOOL)creator scaleLevel:(float)scale littleHippos:(NSArray *)little mamaHippo:(SKSpriteNode *)mama stationaryObjects:(NSArray *)stationary moveableObjects:(NSArray *)moveable dropObjects:(NSArray *)drop backgrounds:(NSArray *)bgs;

- (float)randomTimeBetween:(float)minimum and:(float)maximum;

-(void)startAnimatingHippos;

// Buttons
-(void)pauseButtonPressed;
-(void)restartButtonPressed;
-(void)playButtonPressed;
-(void)levelSelectButtonPressed;
-(void)levelCreatorButtonPressed;
-(void)soundButtonPressed;

-(void)countDown:(NSNumber *)number;
-(void)showScoreMenu;

@end
