//
//  JRBMyScene.h
//  Lost Hippos
//

//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "GameDevHelper.h"

@interface JRBMyScene : SKScene
{
    CGSize winSize;

    SKSpriteNode *mamaHippo, *ninjaHippo, *littleHippo;
}



@end
