//
//  MainMenu.h
//  Lost Hippos
//
//  Created by Josh Braun on 8/8/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class SKSpriteNodeButton, SKScrollLayer;

@interface MainMenu : SKScene
{
    CGSize winSize;
    SKTextureAtlas *atlas, *atlas2;
    
    SKSpriteNode *logo;
    //SKShapeNode *dimmer;
    
    SKSpriteNode *mamaHippo, *ninjaHippo, *littleHippo;
    int rollDirection;// How far the littleHippo will roll
                    // -- negative is left -- positive is right
    float littleHippoLegY;
    // Set it back after the roll
    
    // Buttons
    SKSpriteNodeButton *playButton, *shopButton, *gameCenterButton, *settingsButton;
    SKSpriteNodeButton *levelCreatorButton;
    SKScrollLayer *scroller;
    int startPage;
    
    SKSpriteNodeButton *selectedButton;
    
    int furthestLevelReached;
    NSMutableArray *levelButtons;
    
    // Know whether to show the gamecenter
    // button or hide it once authenticated
    BOOL showingLevelSelect;
}

// Need to access in level scenes
@property (nonatomic) SKSpriteNodeButton *levels1_20, *levels21_40, *levels41_60, *levels61_80, *backButton;

- (float)randomTimeBetween:(float)minimum and:(float)maximum;

-(void)startAnimatingHippos;
-(void)stopAnimatingHippos;
-(void)littleRoll;
-(void)ninjaPeek;

-(void)playButtonPressed;
-(void)shopButtonPressed;
-(void)gameCenterPressed;
-(void)settingsButtonPressed;
-(void)levelCreatorButtonPressed;

// Game Center
-(void)gameCenterNotAvailable;
-(void)gameCenterAvailable;

-(void)goToLevelSelect;
-(void)goFromLevelSelect;

-(void)levelPackButtonPressed:(id)sender;
-(void)backButtonPressed;


@end
