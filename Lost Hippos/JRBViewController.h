//
//  JRBViewController.h
//  Lost Hippos
//

//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>

@interface JRBViewController : UIViewController <GKGameCenterControllerDelegate>
{
    SKView *skView;
}

- (IBAction)gameCenterPressed;

@end