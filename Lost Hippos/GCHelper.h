//
//  GCHelper.h
//  Lost Hippos
//
//  Created by Josh Braun on 8/8/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GCHelper : NSObject <UIAlertViewDelegate>
{
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

// Let the scene know authentication changes
@property (nonatomic) SKView *view;
@property (assign, readonly) BOOL gameCenterAvailable;

+(GCHelper *)sharedHelper;
-(void)authenticateLocalUser;
@end
