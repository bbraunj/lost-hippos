//
//  PhsyicsCategories.h
//  Lost Hippos
//
//  Created by Josh Braun on 1/3/14.
//  Copyright (c) 2014 The Supremacy. All rights reserved.
//


// Define all the physics categories for the game
static const uint32_t LittleHippoCategory = 0x1 << 0;
static const uint32_t MamaHippoCategory = 0x1 << 1;
static const uint32_t LegCategory = 0x1 << 2;
static const uint32_t ObjectCategory = 0x1 << 3;
static const uint32_t GroundCategory = 0x1 << 4;
static const uint32_t DropObjectCategory = 0x1 << 5;
static const uint32_t None = 0x1 << 6;
