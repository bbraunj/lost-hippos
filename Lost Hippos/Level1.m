//
//  Level1.m
//  Lost Hippos
//
//  Created by Josh Braun on 8/18/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "Level1.h"
#import "MainMenu.h"
#import "CGPointExtension.h"
#import "LevelInfoStore.h"
#import "Level2.h"
#import "BMGlyphFont.h"
#import "BMGlyphLabel.h"
#import "PhysicsCategories.h"

@implementation Level1

-(id)initWithSize:(CGSize)size
{
    self = [super initWithSize: size];
    if (self) {
        winSize = self.frame.size;
        
        // Have the app load the right atlas
        atlas = [SKTextureAtlas atlasNamed: @"Images"];

        canLittleHippoRotate = YES;
        levelComplete = NO;
        
        maxDistance = 85;
        if (IS_IPAD)
            maxDistance = 150;
        
        // Make the ground physicsBody
        int groundY = 5;
        if (IS_IPAD)
            groundY = 10;
        [self setPhysicsBody: [SKPhysicsBody bodyWithEdgeFromPoint: ccp(-winSize.width, groundY) toPoint: ccp(winSize.width*2, groundY)]];

        // Background
        SKSpriteNode *sky = [SKSpriteNode spriteNodeWithImageNamed: @"BlueSky.png"];
        [sky setPosition: ccp(winSize.width/2, winSize.height/2)];
        [sky setZPosition: -2];
        [self addChild: sky];
        
        SKSpriteNode *rocks = [SKSpriteNode spriteNodeWithImageNamed: @"Level1Rocks.png"];
        [rocks setPosition: ccp(winSize.width/2, winSize.height * 0.13)];
        [rocks setZPosition: -1.5];
        [self addChild: rocks];
        
        dropZone = [SKSpriteNode spriteNodeWithImageNamed: @"DropZone.png"];
        [dropZone setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.175)];
        [dropZone setZPosition: -1.5];
        [self addChild: dropZone];
        
        SKSpriteNode *grass = [SKSpriteNode spriteNodeWithImageNamed: @"Grass.png"];
        [grass setPosition: ccp(winSize.width/2, 7.5)];
        [grass setZPosition: -1.4];
        [self addChild: grass];
        
        SKSpriteNode *grass2 = [SKSpriteNode spriteNodeWithImageNamed: @"Grass2.png"];
        [grass2 setPosition: ccp(winSize.width/2, 4)];
        [grass2 setZPosition: 1];
        [self addChild: grass2];
        
        // Score
        int fontSize = 20;
        int yPosition = winSize.height - winSize.height * 0.075;
        if (IS_IPAD) {
            fontSize = 40;
            yPosition = winSize.height - winSize.height * 0.06;
        }
        
        SKLabelNode *score = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [score setName: @"Score"];
        [score setFontColor: [UIColor whiteColor]];
        [score setFontSize: fontSize];
        [score setText: @"Score:"];
        [score setAlpha: 0.9];
        [score setPosition: ccp(winSize.width * 0.06, yPosition)];
        [self addChild: score];

        scoreLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [scoreLabel setFontColor: [UIColor whiteColor]];
        [scoreLabel setFontSize: fontSize];
        [scoreLabel setText: @"500"];
        [scoreLabel setAlpha: 0.9];
        [scoreLabel setPosition: ccp(winSize.width * 0.14, yPosition)];
        [self addChild: scoreLabel];
        
        // Start counting down the score
        [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
        
        
        // Buttons
        pauseButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PauseButton.png"];
        [pauseButton setPosition: ccp(winSize.width - 18.5, winSize.height - 20)];
        [pauseButton addTarget: self action: @selector(pauseButtonPressed)];
        [self addChild: pauseButton];
        
        restartButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartButton.png"];
        [restartButton setPosition: ccp(winSize.width - 20, winSize.height - 60)];
        [restartButton addTarget: self action: @selector(restartButtonPressed)];
        [self addChild: restartButton];
        
        
        // Pause Menu
        dimmer = [[SKShapeNode alloc] init];
        [dimmer setPath: CGPathCreateWithRect(self.frame, NULL)];
        [dimmer setFillColor: [UIColor blackColor]];
        [dimmer setStrokeColor: [UIColor clearColor]];
        [dimmer setAlpha: 0];
        [dimmer setZPosition: -1];
        [self addChild: dimmer];
        
        pauseMenu = [SKSpriteNode spriteNodeWithImageNamed: @"PauseMenu.png"];
        [pauseMenu setPosition: ccp(winSize.width/2, winSize.height/2)];
        [self addChild: pauseMenu];
        
        playButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PausePlayButton.png"];
        [playButton setPosition: ccp(-pauseMenu.frame.size.width * 0.26, pauseMenu.frame.size.height * 0.15)];
        [playButton addTarget: self action: @selector(playButtonPressed)];
        [pauseMenu addChild: playButton];
        
        restartPauseButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartLevelButton.png"];
        [restartPauseButton setPosition: ccp(0, pauseMenu.frame.size.height * 0.15)];
        [restartPauseButton addTarget: self action: @selector(restartButtonPressed)];
        [pauseMenu addChild: restartPauseButton];
        
        levelSelectButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelSelectButton.png"];
        [levelSelectButton setPosition: ccp(pauseMenu.frame.size.width * 0.3, pauseMenu.frame.size.height * 0.15)];
        [levelSelectButton addTarget: self action: @selector(levelSelectButtonPressed)];
        [pauseMenu addChild: levelSelectButton];
        
        // SoundButton
        if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"enabled"])
            soundButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SoundOnButton.png"];
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"disabled"])
            soundButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SoundOffButton.png"];
        
        [soundButton setPosition: ccp(0, -pauseMenu.frame.size.height * 0.23)];
        [soundButton addTarget: self action: @selector(soundButtonPressed)];
        [pauseMenu addChild: soundButton];
        
        [pauseMenu setScale: 0];

        
        // Wood Blocks
        wood = [SKSpriteNode spriteNodeWithImageNamed: @"Wood.png"];
        [wood setPosition: ccp(winSize.width/2 + 40, wood.frame.size.height/2 + 5)];
        [wood setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: wood.frame.size]];
        [[wood physicsBody] setLinearDamping: 0];
        [[wood physicsBody] setMass: 1];
        [self addChild: wood];
        
        wood2 = [SKSpriteNode spriteNodeWithImageNamed: @"Wood.png"];
        [wood2 setPosition: ccp(winSize.width * 0.1, wood2.frame.size.height/2 + 5)];
        [wood2 setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: wood2.frame.size]];
        [[wood2 physicsBody] setLinearDamping: 0];
        [[wood2 physicsBody] setMass: 1];
        [self addChild: wood2];
        
        // Boulder
        boulder = [SKSpriteNode spriteNodeWithImageNamed: @"Boulder.png"];
        [boulder setPosition: ccp(winSize.width/2, winSize.height - winSize.height * 0.2)];
        [boulder setAlpha: 0.6];
        [self addChild: boulder];
        
        canLetBoulderGo = YES;
        boulderLetGo = NO;
        
        // Fade the alpha until they touch the screen
        SKAction *repeat = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction fadeAlphaTo: 0.8 duration: 1], [SKAction fadeAlphaTo: 0.6 duration: 1]]]];
        [boulder runAction: repeat];
        
        
        // Mama hippo
        mamaHippo = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoBody.png"];
        [mamaHippo setPosition: ccp(winSize.width/2 - winSize.width * 0.25, 50)];
        [mamaHippo setZPosition: -1];
        
        SKSpriteNode *mamaHead = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoHead.png"];
        [mamaHead setUserData: [NSMutableDictionary dictionaryWithObject: [atlas textureNamed: @"MamaHippoHeadBlink.png"] forKey: @"blinkTexture"]];
        [mamaHead setName: @"mamaHead"];
        [mamaHead setPosition: ccp(0, -4)];
        if (IS_IPAD) [mamaHead setPosition: ccp(0, -4 * 1.8)];
        [mamaHippo addChild: mamaHead];
        
        // Calculate the position of the legs
        float legSpacingHorozontol = mamaHippo.frame.size.width / 4;
        float legSpacingVertical = mamaHippo.frame.size.height / 2.1;
        
        SKSpriteNode *mamaLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg1 setName: @"mamaLeg1"];
        [mamaLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg1];
        
        SKSpriteNode *mamaLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg2 setName: @"mamaLeg2"];
        [mamaLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg2];
        
        [self addChild: mamaHippo];
        
        // Little hippo
        littleHippo = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoBody.png"];
        [littleHippo setPosition: ccp(winSize.width/2 + 40, wood.frame.size.height + littleHippo.frame.size.height/2 + 5)];
        [littleHippo setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: littleHippo.frame.size.width/2 - 1]];
        [[littleHippo physicsBody] setCategoryBitMask: LittleHippoCategory];
        [[littleHippo physicsBody] setCollisionBitMask: ObjectCategory];
        [[littleHippo physicsBody] setContactTestBitMask: LittleHippoCategory];
        [[littleHippo physicsBody] setLinearDamping: 1];
        [[littleHippo physicsBody] setMass: 1];
        
        SKSpriteNode *littleHead = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoHead.png"];
        [littleHead setUserData: [NSMutableDictionary dictionaryWithObject: [atlas textureNamed: @"LittleHippoHeadBlink.png"] forKey: @"blinkTexture"]];
        [littleHead setName: @"littleHead"];
        [littleHead setPosition: ccp(0, -1.5)];
        [littleHippo addChild: littleHead];
        
        legSpacingHorozontol = littleHippo.frame.size.width / 4;
        legSpacingVertical = littleHippo.frame.size.height / 2.75;
        
        SKSpriteNode *littleLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg1 setName: @"littleLeg1"];
        [littleLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg1];
        
        SKSpriteNode *littleLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg2 setName: @"littleLeg2"];
        [littleLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg2];
        
        [self addChild: littleHippo];
        
        // Position adjustments for iPad
        if (IS_IPAD) {
            [grass setPosition: ccp(winSize.width/2, 27)];
            [grass2 setPosition: ccp(winSize.width/2, 10)];
            [wood setPosition: ccp(winSize.width/2 + 80, wood.frame.size.height/2 + 5)];
            [wood2 setPosition: ccp(120, wood2.frame.size.height/2 + 5)];
            [pauseButton setPosition: ccp(winSize.width - 35, winSize.height - 40)];
            [restartButton setPosition: ccp(winSize.width - 37.5, winSize.height - 120)];
            [mamaHippo setPosition: ccp(winSize.width/2 - 250, 95)];
            [littleHippo setPosition: ccp(winSize.width/2 + 80, wood.frame.size.height + littleHippo.frame.size.height/2 + 5)];
        }
    }
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    [self startAnimatingHippos];
}

- (float)randomTimeBetween:(float)minimum and:(float)maximum
{
    // Random time between minimum and maximum time in seconds
    float diff = maximum - minimum;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + minimum;
}

#pragma mark - Hippo Animations
-(void)startAnimatingHippos
{
    // Moves the body up and down 4 pts
    SKAction *moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -4.5 duration: 0.2], [SKAction moveByX: 0 y: 4.5 duration: 0.2]]]];
    
    // Since the legs are children, reverse the
    // motion to keep them in the same position
    SKAction *legsSamePosition = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: 4.5 duration: 0.2], [SKAction moveByX: 0 y: -4.5 duration: 0.2]]]];
    
    // Same goes for the head, but give it
    // some extra motion for a more 3d effect
    SKAction *headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: 6 duration: 0.2], [SKAction moveByX: 0 y: -6 duration: 0.2]]]];
    
    
    // Animate the mama hippo
    [mamaHippo runAction: moveBody];
    
    for (SKSpriteNode *child in [mamaHippo children]) {
        if ([child.name isEqual: @"mamaLeg1"] || [child.name isEqual: @"mamaLeg2"]) {
            // It's a leg
            [child runAction: legsSamePosition withKey: @"samePosition"];
        }
        else {
            // The head gets more movement than the legs
            [child runAction: headMotion withKey: @"headMotion"];
        }
    }
    
    // Make the mama blink
    SKSpriteNode *mamaHead = (SKSpriteNode *)[mamaHippo childNodeWithName: @"mamaHead"];
    
    NSArray *blinkTextures = [NSArray arrayWithObjects: mamaHead.texture, [mamaHead.userData objectForKey: @"blinkTexture"], mamaHead.texture, nil];
    SKAction *blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    SKAction *repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 1.5], blink]];
    [mamaHead runAction: [SKAction repeatActionForever: repeatBlink]];
    
    
    // Make the littleHippo blink
    SKSpriteNode *littleHead = (SKSpriteNode *)[littleHippo childNodeWithName: @"littleHead"];
    
    blinkTextures = [NSArray arrayWithObjects: littleHead.texture, [littleHead.userData objectForKey: @"blinkTexture"], littleHead.texture, nil];
    blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 3], blink]];
    [littleHead runAction: [SKAction repeatActionForever: repeatBlink]];
}

#pragma mark - Buttons
-(void)pauseButtonPressed
{
    // Pause both the score countdown and the
    // littleHippo in range countdown
    [NSObject cancelPreviousPerformRequestsWithTarget: self];
    
    // Pause the mamaHippo's bouncing and blinking
    [mamaHippo setPaused: YES];
    
    // Pause littleHippo's blinking
    [littleHippo setPaused: YES];
    
    // Pause the physics simulations
    [[self physicsWorld] setSpeed: 0];
    
    // Bring the dimmer and pauseMenu to the front
    [dimmer setZPosition: 1.9];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.2]];
    [pauseMenu setZPosition: 2];
    [pauseMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.15], [SKAction scaleTo: 1 duration: 0.1]]]];
    
    // All touches for gameplay are now ignored
    firstTouch = [[UITouch alloc] init];
    
    // Hide the pause/restart buttons on the scene
    [pauseButton setAlpha: 0];
    [restartButton setAlpha: 0];
}

-(void)restartButtonPressed
{
    SKScene *scene = [Level1 sceneWithSize: winSize];
    [scene setScaleMode: SKSceneScaleModeAspectFit];
    [self.view presentScene: scene transition: [SKTransition fadeWithDuration: 0.8]];
}

#pragma mark - Pause Menu Buttons
-(void)playButtonPressed
{
    // Unpause the score countdown
    [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
    
    // Unpause the littleHippo in range countdown if it's on
    if ([self childNodeWithName: @"numberLabel"]) {
        SKLabelNode *counterLabel = (SKLabelNode *)[self childNodeWithName: @"numberLabel"];
        int countDownNumber = [[counterLabel text] intValue];
        [self countDown: [NSNumber numberWithInt: countDownNumber]];
    }
    
    
    // Unpause the mamaHippo's bouncing and blinking
    [mamaHippo setPaused: NO];
    
    // Unpause littleHippo's blinking
    [littleHippo setPaused: NO];
    
    // Pause the physics simulations
    [[self physicsWorld] setSpeed: 1];

    // Fade out the dimmer and scale down the pause menu
    [dimmer runAction: [SKAction fadeAlphaTo: 0 duration: 0.3] completion: ^{
        [dimmer setZPosition: -1];
    }];
    [pauseMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.1], [SKAction scaleTo: 0 duration: 0.15]]] completion: ^{
        [pauseMenu setZPosition: -1];
    }];
    
    // All touches for gameplay are now recognized
    firstTouch = nil;
    
    // Hide the pause/restart buttons on the scene
    [pauseButton setAlpha: 1];
    [restartButton setAlpha: 1];
}

-(void)levelSelectButtonPressed
{
    // Create the scene and navigate to the levelPage of this level
    MainMenu *mainMenu = [MainMenu sceneWithSize: winSize];
    [mainMenu setScaleMode: SKSceneScaleModeAspectFit];
    [mainMenu playButtonPressed];
    [mainMenu levelPackButtonPressed: mainMenu.levels1_20];
    [[self view] presentScene: mainMenu transition: [SKTransition fadeWithDuration: 0.8]];
}

-(void)soundButtonPressed
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"enabled"]) {
        [soundButton setTexture: [SKTexture textureWithImageNamed: @"SoundOffButton.png"]];
        [[NSUserDefaults standardUserDefaults] setObject: @"disabled" forKey: @"soundEnabled"];
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"] isEqual: @"disabled"]) {
        [soundButton setTexture: [SKTexture textureWithImageNamed: @"SoundOnButton.png"]];
        [[NSUserDefaults standardUserDefaults] setObject: @"enabled" forKey: @"soundEnabled"];
    }
}


#pragma mark - The Game
-(void)countScoreDown
{
    int newScore = [[scoreLabel text] intValue] - 1;
    if (newScore >= 0) {
        [scoreLabel setText: [NSString stringWithFormat: @"%i", newScore]];
        [self performSelector: @selector(countScoreDown) withObject: nil afterDelay: 0.5];
    } else
        [self restartButtonPressed];
    // Restart the level if we reach 0
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!firstTouch && !boulderLetGo) {
        firstTouch = [touches anyObject];
        
        // Only start if the touch is in the drop zone
        if ([dropZone containsPoint: [firstTouch locationInNode: self]]) {
            [boulder removeAllActions];
            [boulder setPosition: [firstTouch locationInNode: self]];
            
            // Check if the boulder intersects the littleHippo
            if (CGRectIntersectsRect(boulder.frame, littleHippo.frame)) {
                [boulder setAlpha: 0.7];
                [boulder setColor: [UIColor redColor]];
                [boulder setColorBlendFactor: 1];
                canLetBoulderGo = NO;
            } else {
                [boulder setAlpha: 1];
                [boulder setColorBlendFactor: 0];
                canLetBoulderGo = YES;
            }
        } else // Touch isn't in the dropZone
            canLetBoulderGo = NO;
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch && !boulderLetGo && [dropZone containsPoint: [firstTouch locationInNode: self]]) {
            [boulder setPosition: [firstTouch locationInNode: self]];
           
            // Check if the boulder intersects the littleHippo
            if (CGRectIntersectsRect(boulder.frame, littleHippo.frame)) {
                [boulder setAlpha: 0.7];
                [boulder setColor: [UIColor redColor]];
                [boulder setColorBlendFactor: 1];
                canLetBoulderGo = NO;
            } else {
                [boulder removeAllActions];
                [boulder setAlpha: 1];
                [boulder setColorBlendFactor: 0];
                canLetBoulderGo = YES;
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        if (touch == firstTouch) {
            firstTouch = nil;
            
            // If we can and haven't yet let the boulder go
            if (!boulderLetGo && canLetBoulderGo) {
                int mass = 2;
                if (IS_IPAD)
                    mass = 1;
                
                // Physics category
                uint32_t ObjectCategory = 0x1 << 1;

                [boulder setPhysicsBody: [SKPhysicsBody bodyWithCircleOfRadius: boulder.frame.size.width/2 - 1]];
                [[boulder physicsBody] setCategoryBitMask: ObjectCategory];
                [[boulder physicsBody] setLinearDamping: 1];
                [[boulder physicsBody] setMass: mass];
                
                boulderLetGo = YES;
                canLetBoulderGo = NO;
            }
        }
    }
}


-(void)countDown:(NSNumber *)number
{
    // If the number's over 0
    if ([number intValue] > 0) {
        // Remove any old numberLabels
        if ([self childNodeWithName: @"numberLabel"])
            [[self childNodeWithName: @"numberLabel"] removeFromParent];
        
        // Show the numberLabel
        int fontSize = 120;
        if (IS_IPAD)
            fontSize = 240;
        
        SKLabelNode *numberLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
        [numberLabel setName: @"numberLabel"];
        [numberLabel setText: [NSString stringWithFormat: @"%i", [number intValue]]];
        [numberLabel setFontColor: [UIColor whiteColor]];
        [numberLabel setFontSize: fontSize];
        [numberLabel setPosition: ccp(winSize.width/2, winSize.height/2)];
        [numberLabel setAlpha: 0];
        [self addChild: numberLabel];
        
        // Fade in/scale down the number
        [numberLabel runAction: [SKAction group: @[[SKAction scaleTo: 0.5 duration: 0.6], [SKAction fadeAlphaTo: 1 duration: 0.7]]]];
        [self performSelector: @selector(countDown:) withObject: [NSNumber numberWithInt: [number intValue] - 1] afterDelay: 1];
    } else if ([number intValue] == 0) {
        // Remove any old numberLabels
        if ([self childNodeWithName: @"numberLabel"])
            [[self childNodeWithName: @"numberLabel"] removeFromParent];

        [self showScoreMenu];
    }
}

-(void)update:(NSTimeInterval)currentTime
{
    // Physics categories
    uint32_t HippoCategory = 0x1 << 0;
    uint32_t ObjectCategory = 0x1 << 1;

    if (ccpDistance(littleHippo.position, mamaHippo.position) < maxDistance && ![self childNodeWithName: @"numberLabel"] && !levelComplete) {
        // If the littleHippo's in range, start counting down
        [self countDown: [NSNumber numberWithInt: 5]];
        canLittleHippoRotate = YES;
    }
    else if (ccpDistance(littleHippo.position, mamaHippo.position) > maxDistance && [self childNodeWithName: @"numberLabel"]) {
        // LittleHippo too far from mamaHippo, so stop counting down and remove the label
        NSNumber *number = [NSNumber numberWithInt: [[(SKLabelNode *)[self childNodeWithName: @"numberLabel"] text] intValue] - 1];
        [NSObject cancelPreviousPerformRequestsWithTarget: self selector: @selector(countDown:) object: number];
        [[self childNodeWithName: @"numberLabel"] removeFromParent];
        canLittleHippoRotate = NO;
    }
    
    
    // If the littleHippo's not moving much and it's near
    // the mamaHippo, then rotate it and make it bounce
    if (littleHippo.physicsBody.angularVelocity < 1.5 && littleHippo.physicsBody.angularVelocity > -1.5 && canLittleHippoRotate) {
        canLittleHippoRotate = NO;
        [littleHippo runAction: [SKAction waitForDuration: 2] completion: ^{
            if (littleHippo.physicsBody.angularVelocity < 1.5 && littleHippo.physicsBody.angularVelocity > -1.5 && ccpDistance(littleHippo.position, mamaHippo.position) < maxDistance) {
                // Give the littleHippo a nonDynamic body
                SKPhysicsBody *body = [SKPhysicsBody bodyWithCircleOfRadius: littleHippo.frame.size.width/2 - littleHippo.frame.size.width * 0.15];
                [body setDynamic: NO];
                [body setCategoryBitMask: HippoCategory];
                [body setCollisionBitMask: ObjectCategory];
                [body setContactTestBitMask: HippoCategory];
                [littleHippo setPhysicsBody: body];
                
                // Less movement for the little hippo
                SKAction *moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -2 duration: 0.16], [SKAction moveByX: 0 y: 2 duration: 0.16]]]];
                SKAction *legsSamePosition = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: 2 duration: 0.16], [SKAction moveByX: 0 y: -2 duration: 0.16]]]];
                SKAction *headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: 3 duration: 0.16], [SKAction moveByX: 0 y: -3 duration: 0.16]]]];
                                
                // The y position we want the littleHippo to move to
                float littleHippoY = littleHippo.position.y * 1.11830684;
                                                           // iphone ratio
                if (IS_IPAD)
                    littleHippoY = littleHippo.position.y * 1.13577124;
                                                         // ipad ratio
                
                [littleHippo runAction: [SKAction group: @[[SKAction rotateToAngle: 0 duration: 0.3 shortestUnitArc: YES], [SKAction moveToY: littleHippoY duration: 0.3]]] completion: ^{
                    // Move the littleHippo
                    [littleHippo runAction: moveBody withKey: @"moveBody"];
                    
                    for (SKSpriteNode *child in [littleHippo children]) {
                        if ([child.name isEqual: @"littleLeg1"] || [child.name isEqual: @"littleLeg2"])
                            // Legs
                            [child runAction: legsSamePosition withKey: @"samePosition"];
                        else
                            // The head gets more movement than the legs
                            [child runAction: headMotion withKey: @"headMotion"];
                    }
                }];
               
                // Move the legs out
                float legSpacingVertical = littleHippo.frame.size.height * 0.05;
                [[littleHippo childNodeWithName: @"littleLeg1"] runAction: [SKAction moveByX: 0 y: -legSpacingVertical duration: 0.3]];
                [[littleHippo childNodeWithName: @"littleLeg2"] runAction: [SKAction moveByX: 0 y: -legSpacingVertical duration: 0.3]];
                
            }
        }];
    }
}

#pragma mark - Level Completed
-(void)showScoreMenu
{
    levelComplete = YES;
    BOOL newHighScore = NO;
    
    // Save the score and furthestLevelReached
    if ([[LevelInfoStore sharedStore] scoreForLevel: 1] < [scoreLabel.text intValue]) {
        newHighScore = YES;
        [[LevelInfoStore sharedStore] setScore: [scoreLabel.text intValue] forLevel: 1];
    }
    
    if ([[LevelInfoStore sharedStore] furthestLevelReached] == 1)
        [[LevelInfoStore sharedStore] setFurthestLevelReached: 2];
    
    [[LevelInfoStore sharedStore] saveLevelInfo];
    
    
    // Stop counting the score down
    [NSObject cancelPreviousPerformRequestsWithTarget: self selector: @selector(countScoreDown) object: nil];
    
    // Fade out the scoreLabels / pause/restart buttons
    [[self childNodeWithName: @"Score"] runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [scoreLabel runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [pauseButton runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    [restartButton runAction: [SKAction fadeAlphaTo: 0 duration: 0.2]];
    
    // Create the scoreMenu
    SKSpriteNode *scoreMenu = [SKSpriteNode spriteNodeWithImageNamed: @"ScoreMenu.png"];
    [scoreMenu setPosition: ccp(winSize.width/2, winSize.height/2)];
    [scoreMenu setZPosition: 2];
    [self addChild: scoreMenu];
    
    // If there's a new high score, show the new high score sprite
    if (newHighScore) {
        // Create the crop node
        SKCropNode *newHighScore = [[SKCropNode alloc] init];
        [newHighScore setMaskNode: [SKSpriteNode spriteNodeWithImageNamed: @"NewHighScoreMask.png"]];
        [newHighScore setPosition: ccp(0, scoreMenu.frame.size.height * 0.08)];
        [newHighScore.maskNode setPosition: ccp(-newHighScore.maskNode.frame.size.width, 0)];
        [scoreMenu addChild: newHighScore];
        
        // Create the actual new high score sprite
        SKSpriteNode *newHighScoreSprite = [SKSpriteNode spriteNodeWithImageNamed: @"NewHighScore.png"];
        [newHighScoreSprite setPosition: ccp(0, 0)];
        [newHighScore addChild: newHighScoreSprite];
        
        // Create the particle emitter
        SKEmitterNode *emitterNode = [NSKeyedUnarchiver unarchiveObjectWithFile: [[NSBundle mainBundle] pathForResource: @"Sparky" ofType: @"sks"]];
        [emitterNode setPosition: ccp(newHighScoreSprite.frame.origin.x, newHighScoreSprite.frame.origin.y + scoreMenu.frame.size.height * 0.12)];
        [emitterNode setZPosition: 3];
        [scoreMenu addChild: emitterNode];
        
        // Set the scale of the emitter
        if (!IS_IPAD)
            [emitterNode setScale: 0.2];
        else
            [emitterNode setScale: 0.5];
        
        // Save the birthrate for later to start the emitter
        float birthrate = emitterNode.particleBirthRate;
        [emitterNode setParticleBirthRate: 0];
        
        // Create the path for the emitter to follow
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter: ccp(scoreMenu.frame.size.width * 0.3, -scoreMenu.frame.size.height * 0.31) radius: newHighScore.maskNode.frame.size.width * 0.7 startAngle: 2.5 endAngle: 0.6 clockwise: NO];
        
        // Start the emitter, follow the path, fade out the emitter, and remove from parent
        [emitterNode runAction: [SKAction waitForDuration: 0.7] completion: ^{
            [emitterNode setParticleBirthRate: birthrate];
            [emitterNode runAction: [SKAction sequence: @[[SKAction group: @[[SKAction followPath: path.CGPath asOffset: YES orientToPath: NO duration: 0.6], [SKAction sequence: @[[SKAction waitForDuration: 0.4], [SKAction fadeOutWithDuration: 0.2]]]]], [SKAction removeFromParent]]]];
        }];
        
        
        // Move the mask to reveal the sprite
        [newHighScore.maskNode runAction: [SKAction sequence: @[[SKAction waitForDuration: 0.7], [SKAction moveToX: 0 duration: 0.6]]]];
    }
    
    
    SKSpriteNodeButton *restartLevelButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"RestartLevelScoreButton.png"];
    [restartLevelButton setPosition: ccp(-scoreMenu.frame.size.width * 0.25, -scoreMenu.frame.size.height * 0.2)];
    [restartLevelButton addTarget: self action: @selector(restartButtonPressed)];
    [scoreMenu addChild: restartLevelButton];
    
    SKSpriteNodeButton *nextLevelButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"NextLevelButton.png"];
    [nextLevelButton setPosition: ccp(scoreMenu.frame.size.width * 0.27, -scoreMenu.frame.size.height * 0.2)];
    [nextLevelButton addTarget: self action: @selector(nextLevelButtonPressed)];
    [scoreMenu addChild: nextLevelButton];
    
    SKSpriteNodeButton *levelSelectScoreButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelSelectScoreButton.png"];
    [levelSelectScoreButton setPosition: ccp(0, -scoreMenu.frame.size.height * 0.35)];
    [levelSelectScoreButton addTarget: self action: @selector(levelSelectButtonPressed)];
    [scoreMenu addChild: levelSelectScoreButton];
    
    // Score Label
    BMGlyphFont *font = [BMGlyphFont fontWithName: @"ScoreFont"];
    BMGlyphLabel *scoreResultLabel = [BMGlyphLabel labelWithText: scoreLabel.text font: font];
    [scoreResultLabel setPosition: ccp(-scoreMenu.frame.size.width * 0.03, scoreMenu.frame.size.height)];
    [scoreResultLabel setZPosition: 1];
    [scoreResultLabel setScale: 2];
    [scoreResultLabel setAlpha: 0];
    [scoreMenu addChild: scoreResultLabel];
    
    // If it's a new high score, the score labels will need to move down a little further
    int lableMovement = 0;
    if (newHighScore)
        lableMovement = scoreMenu.frame.size.height * 0.07;
    
    // Move the score labels down from the top
    SKAction *moveScore = [SKAction group: @[[SKAction scaleTo: 1 duration: 0.3], [SKAction moveTo: ccp(-scoreMenu.frame.size.width * 0.03, scoreMenu.frame.size.height * 0.02 - lableMovement) duration: 0.3]]];
    [moveScore setTimingMode: SKActionTimingEaseOut];
    [scoreResultLabel runAction: [SKAction waitForDuration: 0.3] completion: ^{
        [scoreResultLabel setAlpha: 1];
        [scoreResultLabel runAction: moveScore];
    }];
    
    // Bring the dimmer to the front and fade it in
    [dimmer setZPosition: 1.9];
    [dimmer runAction: [SKAction fadeAlphaTo: 0.6 duration: 0.2]];
    
    // Scale up the scoreMenu
    [scoreMenu setScale: 0];
    [scoreMenu runAction: [SKAction sequence: @[[SKAction scaleTo: 1.1 duration: 0.15], [SKAction scaleTo: 1 duration: 0.1]]]];
}

-(void)nextLevelButtonPressed
{
    Level2 *nextLevel = [Level2 sceneWithSize: winSize];
    [nextLevel setScaleMode: SKSceneScaleModeAspectFit];
    [self.view presentScene: nextLevel transition: [SKTransition fadeWithDuration: 0.8]];
}

@end
