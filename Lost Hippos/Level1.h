//
//  Level1.h
//  Lost Hippos
//
//  Created by Josh Braun on 8/18/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SKSpriteNodeButton.h"

@interface Level1 : SKScene
{
    CGSize winSize;
    
    SKTextureAtlas *atlas;
    
    BOOL levelComplete;
    
    // Maximum distance the littleHippo can be away
    // from the mamaHippo to complete the level
    float maxDistance;
    
    SKSpriteNode *boulder, *wood, *wood2, *dropZone, *littleHippo, *mamaHippo;
    SKSpriteNodeButton *pauseButton, *restartButton;
    SKLabelNode *scoreLabel;
    
    // Pause Menu
    SKSpriteNode *pauseMenu;
    SKShapeNode *dimmer;
    SKSpriteNodeButton *playButton, *restartPauseButton, *levelSelectButton, *soundButton;
    
    // Store the first touch
    UITouch *firstTouch;
    
    // Have we let the boulder go?
    BOOL boulderLetGo;

    // Should we let the boulder go?
    // - dont let it go if it's intersecting any
    //   other physicsBodies
    BOOL canLetBoulderGo;
    
    // Have we already told the littleHippo to rotate?
    BOOL canLittleHippoRotate;
}

- (float)randomTimeBetween:(float)minimum and:(float)maximum;

-(void)startAnimatingHippos;

// Buttons
-(void)pauseButtonPressed;
-(void)restartButtonPressed;
-(void)playButtonPressed;
-(void)levelSelectButtonPressed;
-(void)soundButtonPressed;


-(void)countDown:(NSNumber *)number;
-(void)showScoreMenu;
-(void)nextLevelButtonPressed;

@end
