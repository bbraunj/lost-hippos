//
//  JRBMyScene.m
//  Lost Hippos
//
//  Created by Josh Braun on 7/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "JRBMyScene.h"

@implementation JRBMyScene

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        winSize = self.frame.size;
        
        //[self setBackgroundColor: [UIColor colorWithRed: 56/255.0f green: 146/255.0f blue: 227/255.0f alpha: 1]];
        
        SKSpriteNode *sky = [SKSpriteNode spriteNodeWithImageNamed: @"Sky.png"];
        [sky setPosition: ccp(winSize.width/2, winSize.height/2)];
        
        SKSpriteNode *rocks = [SKSpriteNode spriteNodeWithImageNamed: @"Rocks.png"];
        [rocks setPosition: ccp(winSize.width/2, winSize.height/2)];
        
        SKSpriteNode *tree = [SKSpriteNode spriteNodeWithImageNamed: @"Tree.png"];
        [tree setPosition: ccp(winSize.width/2, winSize.height/2)];
        
        SKSpriteNode *grass = [SKSpriteNode spriteNodeWithImageNamed: @"Grass.png"];
        [grass setPosition: ccp(winSize.width/2, 7.5)];

        SKSpriteNode *grass2 = [SKSpriteNode spriteNodeWithImageNamed: @"Grass2.png"];
        [grass2 setPosition: ccp(winSize.width/2, 4)];
        [grass2 setZPosition: 1];
        
        [self addChild: sky];
        [self addChild: rocks];
        [self addChild: tree];
        [self addChild: grass];
        [self addChild: grass2];
        
        // Logo
        SKSpriteNode *logo = [SKSpriteNode spriteNodeWithImageNamed: @"Logo.png"];
        [logo setPosition: ccp(winSize.width/2, winSize.height - 60)];
        [self addChild: logo];
        
        // Mama hippo
        mamaHippo = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoBody.png"];
        [mamaHippo setPosition: ccp(winSize.width/2 - 150, 50)];
        
        SKSpriteNode *mamaHead = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoHead.png"];
        [mamaHead setName: @"mamaHead"];
        [mamaHippo addChild: mamaHead];
        
        float legSpacingHorozontol = mamaHippo.frame.size.width / 4;
        float legSpacingVertical = mamaHippo.frame.size.height / 2.25;
        
        SKSpriteNode *mamaLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg1 setName: @"mamaLeg1"];
        [mamaLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg1];
        
        SKSpriteNode *mamaLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg2 setName: @"mamaLeg2"];
        [mamaLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg2];
        
        [self addChild: mamaHippo];
        
        // Little hippo
        littleHippo = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoBody.png"];
        [littleHippo setPosition: ccp(winSize.width/2 - 75, 30)];
        
        SKSpriteNode *littleHead = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoHead.png"];
        [littleHead setName: @"littleHead"];
        [littleHippo addChild: littleHead];
        
        legSpacingHorozontol = littleHippo.frame.size.width / 4;
        legSpacingVertical = littleHippo.frame.size.height / 2.25;
        
        SKSpriteNode *littleLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg1 setName: @"littleLeg1"];
        [littleLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg1];
        
        SKSpriteNode *littleLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg2 setName: @"littleLeg2"];
        [littleLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg2];

        [self addChild: littleHippo];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            // Different positions for ipad than iphone
            [grass setPosition: ccp(winSize.width/2, 27)];
            [grass2 setPosition: ccp(winSize.width/2, 10)];
            [logo setPosition: ccp(winSize.width/2, winSize.height - 110)];
            [mamaHippo setPosition: ccp(winSize.width/2 - 215, 90)];
            [littleHippo setPosition: ccp(winSize.width/2 - 75, 57.5)];
        }
        
    }
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    // Moves the body up and down 4 pts
    SKAction *moveBody = [SKAction sequence: @[[SKAction moveByX: 0 y: 4 duration: 0.2], [SKAction moveByX: 0 y: -4 duration: 0.2]]];
    SKAction *moveBodyRepeat = [SKAction repeatActionForever: moveBody];

    // Since the body parts are children, reverse
    // the body motion to keep them in the same position
    SKAction *childrenSamePosition = [SKAction sequence: @[[SKAction moveByX: 0 y: -4 duration: 0.2], [SKAction moveByX: 0 y: 4 duration: 0.2]]];
    SKAction *childrenSamePositionRepeat = [SKAction repeatActionForever: childrenSamePosition];

    
    // Animate the mama hippo
    [mamaHippo runAction: moveBodyRepeat];
    
    for (SKSpriteNode *child in [mamaHippo children])
        [child runAction: childrenSamePositionRepeat];
    
    
    // Less movement for the little hippo
    moveBody = [SKAction sequence: @[[SKAction moveByX: 0 y: -2 duration: 0.16], [SKAction moveByX: 0 y: 2 duration: 0.16]]];
    moveBodyRepeat = [SKAction repeatActionForever: moveBody];
    
    childrenSamePosition = [SKAction sequence: @[[SKAction moveByX: 0 y: 2 duration: 0.16], [SKAction moveByX: 0 y: -2 duration: 0.16]]];
    childrenSamePositionRepeat = [SKAction repeatActionForever: childrenSamePosition];
    
    
    // Animate the little hippo
    [littleHippo runAction: moveBodyRepeat];
    
    for (SKSpriteNode *child in [littleHippo children])
        [child runAction: childrenSamePositionRepeat];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
