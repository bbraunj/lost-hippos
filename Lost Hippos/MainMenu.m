//
//  MainMenu.m
//  Lost Hippos
//
//  Created by Josh Braun on 8/8/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "MainMenu.h"
#import "SKSpriteNodeButton.h"
#import "JRBViewController.h"
#import "SKScrollLayer.h"
#import "GCHelper.h"
#import "LevelInfoStore.h"
#import "AllLevelsImporter.h"
#import "LevelCreator.h"
#import "Images.h"
#import "Level Creator.h"

@implementation MainMenu

@synthesize levels1_20, levels21_40, levels41_60, levels61_80, backButton;

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        winSize = self.frame.size;
        startPage = 0;
        
        furthestLevelReached = [[LevelInfoStore sharedStore] furthestLevelReached];
        
        showingLevelSelect = NO;
        
        // Have the app load the right atlas
        atlas = [SKTextureAtlas atlasNamed: IMAGES_ATLAS_NAME];
        atlas2 = [SKTextureAtlas atlasNamed: LEVEL_CREATOR_ATLAS_NAME];
        
        // If there's no sound preference stored, set it to enabled
        if (![[NSUserDefaults standardUserDefaults] objectForKey: @"soundEnabled"])
            [[NSUserDefaults standardUserDefaults] setObject: @"enabled" forKey: @"soundEnabled"];
        
        // Background
        SKSpriteNode *sky = [SKSpriteNode spriteNodeWithImageNamed: @"OrangeSky.png"];
        [sky setPosition: ccp(winSize.width/2, winSize.height/2)];
        [sky setZPosition: -5];
        
        SKSpriteNode *rocks = [SKSpriteNode spriteNodeWithImageNamed: @"Rocks.png"];
        [rocks setPosition: ccp(winSize.width/2, winSize.height/2 - 20)];
        [rocks setZPosition: -4];
        
        SKSpriteNode *vines = [SKSpriteNode spriteNodeWithImageNamed: @"Vines.png"];
        [vines setPosition: ccp(winSize.width/2, winSize.height/2)];
        [vines setZPosition: -3];
        
        SKSpriteNode *tree = [SKSpriteNode spriteNodeWithImageNamed: @"Tree.png"];
        [tree setPosition: ccp(winSize.width/2, winSize.height/2)];
        [tree setZPosition: -1];
        
        SKSpriteNode *grass = [SKSpriteNode spriteNodeWithImageNamed: @"Grass.png"];
        [grass setPosition: ccp(winSize.width/2, 9)];
        
        SKSpriteNode *grass2 = [SKSpriteNode spriteNodeWithImageNamed: @"Grass2.png"];
        [grass2 setPosition: ccp(winSize.width/2, 2)];
        [grass2 setZPosition: 1];
        
        [self addChild: sky];
        [self addChild: rocks];
        [self addChild: vines];
        [self addChild: tree];
        [self addChild: grass];
        [self addChild: grass2];
        
        
        // Logo
        logo = [SKSpriteNode spriteNodeWithImageNamed: @"Logo.png"];
        [logo setPosition: ccp(winSize.width/2, winSize.height - 60)];
        [self addChild: logo];
        
        // Buttons
        playButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"PlayButton.png"];
        [playButton setPosition: ccp(winSize.width/2, winSize.height/2)];
        [playButton addTarget: self action: @selector(playButtonPressed)];
        [self addChild: playButton];
        
        shopButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"ShopButton.png"];
        [shopButton setPosition: ccp(winSize.width - 100, 40)];
        [shopButton setZPosition: 2];
        [shopButton addTarget: self action: @selector(shopButtonPressed)];
        [self addChild: shopButton];
        
        gameCenterButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"GameCenterButton.png"];
        [gameCenterButton setPosition: ccp(winSize.width - 35, 100)];
        [gameCenterButton setZPosition: 2];
        [gameCenterButton setAlpha: 0];
        [gameCenterButton setEnabled: NO];
        if ([[GKLocalPlayer localPlayer] isAuthenticated]) {
            [gameCenterButton setAlpha: 1];
            [gameCenterButton setEnabled: YES];
        }
        [gameCenterButton addTarget: self action: @selector(gameCenterPressed)];
        [self addChild: gameCenterButton];
        
        settingsButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"SettingsButton.png"];
        [settingsButton setPosition: ccp(winSize.width - 35, 40)];
        [settingsButton setZPosition: 2];
        [settingsButton addTarget: self action: @selector(settingsButtonPressed)];
        [self addChild: settingsButton];
        
        levelCreatorButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LevelCreatorButton.png"];
        [levelCreatorButton setPosition: ccp(35, 40)];
        [levelCreatorButton addTarget: self action: @selector(levelCreatorButtonPressed)];
        [levelCreatorButton setZPosition: 2];
        [self addChild: levelCreatorButton];
        
        
        // Level Select Buttons
        levels1_20 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Levels1-20.png"];
        [levels1_20 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: YES] forKey: @"unlocked"]];
        [levels1_20 setZoomOnTouchDown: NO];
        [levels1_20 sendDelegate: nil touchEvents: YES];
        [levels1_20 addTarget: self action: @selector(levelPackButtonPressed:)];
        
        // Adjust it's image and enabled/disabled if the level's unlocked
        if (furthestLevelReached > 20){
            levels21_40 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Levels21-40.png"];
            [levels21_40 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: YES] forKey: @"unlocked"]];
        }
        else {
            levels21_40 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LockedLevelPack.png"];
            [levels21_40 setEnabled: NO];
            [levels21_40 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: NO] forKey: @"unlocked"]];
        }
        [levels21_40 setZoomOnTouchDown: NO];
        [levels21_40 sendDelegate: nil touchEvents: YES];
        [levels21_40 addTarget: self action: @selector(levelPackButtonPressed:)];
        
        if (furthestLevelReached > 40) {
            levels41_60 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Levels41-60.png"];
            [levels41_60 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: YES] forKey: @"unlocked"]];
        }
        else {
            levels41_60 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LockedLevelPack.png"];
            [levels41_60 setEnabled: NO];
            [levels41_60 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: NO] forKey: @"unlocked"]];
        }
        [levels41_60 setZoomOnTouchDown: NO];
        [levels41_60 sendDelegate: nil touchEvents: YES];
        [levels41_60 addTarget: self action: @selector(levelPackButtonPressed:)];
        
        if (furthestLevelReached > 60) {
            levels61_80 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"Levels61-80.png"];
            [levels21_40 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: YES] forKey: @"unlocked"]];
        }
        else {
            levels61_80 = [SKSpriteNodeButton spriteNodeWithImageNamed: @"LockedLevelPack.png"];
            [levels61_80 setEnabled: NO];
            [levels61_80 setUserData: [NSMutableDictionary dictionaryWithObject: [NSNumber numberWithBool: NO] forKey: @"unlocked"]];
        }
        [levels61_80 setZoomOnTouchDown: NO];
        [levels61_80 sendDelegate: nil touchEvents: YES];
        [levels61_80 addTarget: self action: @selector(levelPackButtonPressed:)];
        
        
        // Scrolling layer for level select
        float widthOffset;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            if (winSize.width < 481)
                widthOffset = 175;
            else
                widthOffset = 250;
        } else
            widthOffset = 240;
        
        scroller = [SKScrollLayer nodeWithLayers: @[levels1_20, levels21_40, levels41_60, levels61_80] widthOffset: widthOffset scene: self size: self.frame.size showPagesIndicator: YES];
        [scroller setPath: CGPathCreateWithRect(self.frame, NULL)];
        [scroller setStrokeColor: [UIColor clearColor]];
        [scroller setPosition: ccp(0, 0)];
        [scroller setInitialPosition: scroller.position];
        [scroller setZPosition: -10];//Make sure it's not visible
        [self addChild: scroller];
        
        backButton = [SKSpriteNodeButton spriteNodeWithImageNamed: @"BackButton.png"];
        [backButton setPosition: ccp(40, 30)];
        [backButton setZPosition: -10];// Make sure it's in the back
        [backButton addTarget: self action: @selector(backButtonPressed)];
        [self addChild: backButton];
        
        // Mama hippo
        mamaHippo = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoBody.png"];
        [mamaHippo setPosition: ccp(winSize.width/2 - 150, 50)];
        
        SKSpriteNode *mamaHead = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoHead.png"];
        [mamaHead setName: @"mamaHead"];
        [mamaHead setPosition: ccp(0, -4)];
        if (IS_IPAD) [mamaHead setPosition: ccp(0, -4 * 1.8)];
        [mamaHead setZPosition: 0.2];
        [mamaHippo addChild: mamaHead];
        
        // Calculate the position of the legs
        float legSpacingHorozontol = mamaHippo.frame.size.width / 4;
        float legSpacingVertical = mamaHippo.frame.size.height / 2.1;
        
        SKSpriteNode *mamaLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg1 setName: @"mamaLeg1"];
        [mamaLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg1];
        
        SKSpriteNode *mamaLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"MamaHippoLeg.png"];
        [mamaLeg2 setName: @"mamaLeg2"];
        [mamaLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [mamaHippo addChild: mamaLeg2];
        
        [self addChild: mamaHippo];
        
        // Little hippo
        littleHippo = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoBody.png"];
        [littleHippo setPosition: ccp(winSize.width/2 - 75, 30)];
        
        SKSpriteNode *littleHead = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoHead.png"];
        [littleHead setName: @"littleHead"];
        [littleHead setPosition: ccp(0, -1.5)];
        [littleHead setZPosition: 0.2];
        [littleHippo addChild: littleHead];
        
        legSpacingHorozontol = littleHippo.frame.size.width / 4;
        legSpacingVertical = littleHippo.frame.size.height / 2.25;
        
        littleHippoLegY = -legSpacingVertical;
        
        SKSpriteNode *littleLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg1 setName: @"littleLeg1"];
        [littleLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg1];
        
        SKSpriteNode *littleLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"LittleHippoLeg.png"];
        [littleLeg2 setName: @"littleLeg2"];
        [littleLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [littleHippo addChild: littleLeg2];
        
        [self addChild: littleHippo];
        
        // Ninja Hippo
        ninjaHippo = [SKSpriteNode spriteNodeWithImageNamed: @"NinjaHippoBody.png"];
        [ninjaHippo setPosition: ccp(winSize.width/2 - 75, winSize.height - 40)];
        [ninjaHippo setZRotation: M_PI];
        [ninjaHippo setZPosition: -2];
        
        SKSpriteNode *ninjaHead = [SKSpriteNode spriteNodeWithImageNamed: @"NinjaHippoHead.png"];
        [ninjaHead setName: @"ninjaHead"];
        [ninjaHead setPosition: ccp(0, 20)];
        [ninjaHippo addChild: ninjaHead];
        
        legSpacingHorozontol = ninjaHippo.frame.size.width / 4;
        legSpacingVertical = ninjaHippo.frame.size.height / 2.25;
        
        SKSpriteNode *ninjaLeg1 = [SKSpriteNode spriteNodeWithImageNamed: @"NinjaHippoLeg.png"];
        [ninjaLeg1 setName: @"ninjaLeg1"];
        [ninjaLeg1 setPosition: ccp(-legSpacingHorozontol, -legSpacingVertical)];
        [ninjaHippo addChild: ninjaLeg1];
        
        SKSpriteNode *ninjaLeg2 = [SKSpriteNode spriteNodeWithImageNamed: @"NinjaHippoLeg.png"];
        [ninjaLeg2 setName: @"ninjaLeg2"];
        [ninjaLeg2 setPosition: ccp(legSpacingHorozontol, -legSpacingVertical)];
        [ninjaHippo addChild: ninjaLeg2];
        
        [self addChild: ninjaHippo];
        
        // Position adjustments for iPad
        if (IS_IPAD) {
            [grass setPosition: ccp(winSize.width/2, 27)];
            [grass2 setPosition: ccp(winSize.width/2, 10)];
            [logo setPosition: ccp(winSize.width/2, winSize.height - 110)];
            [shopButton setPosition: ccp(winSize.width - 162.5, 65)];
            [gameCenterButton setPosition: ccp(winSize.width - 67.5, 160)];
            [settingsButton setPosition: ccp(winSize.width - 67.5, 65)];
            [levelCreatorButton setPosition: ccp(67.5, 65)];
            [backButton setPosition: ccp(70, 55)];
            [mamaHippo setPosition: ccp(winSize.width/2 - 250, 95)];
            [littleHippo setPosition: ccp(winSize.width/2 - 110, 57.5)];
            [ninjaHippo setPosition: ccp(winSize.width/2 - 110, winSize.height - 185)];
        }
    }
    return self;
}

- (float)randomTimeBetween:(float)minimum and:(float)maximum
{
    // Random time between minimum and maximum time in seconds
    float diff = maximum - minimum;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + minimum;
}

-(void)didMoveToView:(SKView *)view
{
    [self startAnimatingHippos];
}

#pragma mark - Hippo Animations
-(void)startAnimatingHippos
{
    // Moves the body up and down 4.5 pts (8.1 for iPad)
    float movement = 4.5;
    if (IS_IPAD) movement = 8.1;
    SKAction *moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -movement duration: 0.2], [SKAction moveByX: 0 y: movement duration: 0.2]]]];
    
    // Since the legs are children, reverse the
    // motion to keep them in the same position
    SKAction *legsSamePosition = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: movement duration: 0.2], [SKAction moveByX: 0 y: -movement duration: 0.2]]]];
    
    // Same goes for the head, but give it
    // some extra motion for a more 3d effect
    float headMovement = 6;
    if (IS_IPAD) headMovement = 10.8;
    SKAction *headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: headMovement duration: 0.2], [SKAction moveByX: 0 y: -headMovement duration: 0.2]]]];
    
    
    // Animate the mama hippo
    [mamaHippo runAction: moveBody];
    
    for (SKSpriteNode *child in [mamaHippo children]) {
        if ([child.name isEqual: @"mamaLeg1"] || [child.name isEqual: @"mamaLeg2"])
            // 'Move' the leg
            [child runAction: legsSamePosition withKey: @"samePosition"];
        else if ([child.name isEqual: @"mamaHead"])
            // The head gets more movement than the legs
            [child runAction: headMotion withKey: @"headMotion"];
    }
    
    // Make the mama blink
    SKSpriteNode *mamaHead = (SKSpriteNode *)[mamaHippo childNodeWithName: @"mamaHead"];
    
    NSArray *blinkTextures = [NSArray arrayWithObjects: mamaHead.texture, [SKTexture textureWithImageNamed: @"MamaHippoHeadBlink.png"], mamaHead.texture, nil];
    SKAction *blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    SKAction *repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 1.5], blink]];
    [mamaHead runAction: [SKAction repeatActionForever: repeatBlink]];
    
    
    // Less movement for the little hippo
    movement = 2;
    if (IS_IPAD) movement = 3.6;
    headMovement = 3;
    if (IS_IPAD) headMovement = 5.4;
    moveBody = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: -movement duration: 0.16], [SKAction moveByX: 0 y: movement duration: 0.16]]]];
    legsSamePosition = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: movement duration: 0.16], [SKAction moveByX: 0 y: -movement duration: 0.16]]]];
    headMotion = [SKAction repeatActionForever: [SKAction sequence: @[[SKAction moveByX: 0 y: headMovement duration: 0.16], [SKAction moveByX: 0 y: -headMovement duration: 0.16]]]];
    
    // Animate the little hippo
    [littleHippo runAction: moveBody withKey: @"moveBody"];
    
    for (SKSpriteNode *child in [littleHippo children]) {
        if ([child.name isEqual: @"littleLeg1"] || [child.name isEqual: @"littleLeg2"])
            // Legs
            [child runAction: legsSamePosition withKey: @"samePosition"];
        else if ([child.name isEqual: @"littleHead"])
            // The head gets more movement than the legs
            [child runAction: headMotion withKey: @"headMotion"];
    }
    
    rollDirection = winSize.width/2 * 0.7;
    [self performSelector: @selector(littleRoll) withObject: nil afterDelay: [self randomTimeBetween: 2 and: 6]];
    
    // Make the littleHippo blink
    SKSpriteNode *littleHead = (SKSpriteNode *)[littleHippo childNodeWithName: @"littleHead"];
    
    blinkTextures = [NSArray arrayWithObjects: littleHead.texture, [SKTexture textureWithImageNamed: @"LittleHippoHeadBlink.png"], littleHead.texture, nil];
    blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 3], blink]];
    [littleHead runAction: [SKAction repeatActionForever: repeatBlink]];

    
    // Make the ninjaHippo blink
    SKSpriteNode *ninjaHead = (SKSpriteNode *)[ninjaHippo childNodeWithName: @"ninjaHead"];
    
    blinkTextures = [NSArray arrayWithObjects: ninjaHead.texture, [SKTexture textureWithImageNamed: @"NinjaHippoHeadBlink.png"], ninjaHead.texture, nil];
    blink = [SKAction animateWithTextures: blinkTextures timePerFrame: 0.1];
    repeatBlink = [SKAction sequence: @[[SKAction waitForDuration: 2.5 withRange: 3], blink]];
    [ninjaHead runAction: [SKAction repeatActionForever: repeatBlink]];
    
    // Make the ninja peek out of the tree
    // between 3 and 6 seconds in the future
    [self performSelector: @selector(ninjaPeek) withObject: nil afterDelay: [self randomTimeBetween: 3 and: 6]];
    
}

-(void)stopAnimatingHippos
{
    [mamaHippo removeAllActions];
    [littleHippo removeAllActions];
    [ninjaHippo removeAllActions];
}

-(void)littleRoll
{
    SKAction *moveBody = [littleHippo actionForKey: @"moveBody"];
    SKAction *legsSamePosition = [[littleHippo childNodeWithName: @"littleLeg1"] actionForKey: @"samePosition"];
    SKAction *headMotion = [[littleHippo childNodeWithName: @"littleHead"] actionForKey: @"headMotion"];
    
    // Stop the moving of the body while rolling
    [littleHippo removeActionForKey: @"moveBody"];
    [[littleHippo childNodeWithName: @"littleHead"] removeActionForKey: @"headMotion"];
    [[littleHippo childNodeWithName: @"littleLeg1"] removeActionForKey: @"samePosition"];
    [[littleHippo childNodeWithName: @"littleLeg2"] removeActionForKey: @"samePosition"];
    
    // Which way is he rolling
    float rollAngle;
    if (rollDirection > 0)
        rollAngle = -M_PI * 2;
    else
        rollAngle = M_PI * 2;
    
    // How far the hippo will go down and up
    int littleYPosition = 25;
    float littleYPosition2 = 30;
    if (IS_IPAD) {
        littleYPosition = 45;
        littleYPosition2 = 57.5;
    }
    
    
    [littleHippo runAction: [SKAction sequence: @[[SKAction moveToY: littleYPosition duration: 0.1], [SKAction group: @[[SKAction moveByX: rollDirection y: 0 duration: 0.6], [SKAction rotateByAngle: rollAngle duration: 0.6]]], [SKAction moveToY: littleYPosition2 duration: 0.1]]] completion: ^{
        // Have to run it with a key for next time we roll
        [littleHippo runAction: moveBody withKey: @"moveBody"];
        for (SKNode *child in littleHippo.children) {
            if ([child.name isEqual: @"littleLeg1"] || [child.name isEqual: @"littleLeg2"]) {
                // Legs
                [child runAction: legsSamePosition withKey: @"samePosition"];
            }
            else if ([child.name isEqual: @"littleHead"]){
                // The head gets more movement than the legs
                [child runAction: headMotion withKey: @"headMotion"];
            }
        }
    }];
    [[littleHippo childNodeWithName: @"littleLeg1"] runAction: [SKAction sequence: @[[SKAction moveToY: littleHippoLegY - (littleHippoLegY * 0.3) duration: 0.1], [SKAction waitForDuration: 0.4], [SKAction moveToY: littleHippoLegY duration: 0.1]]]];
    [[littleHippo childNodeWithName: @"littleLeg2"] runAction: [SKAction sequence: @[[SKAction moveToY: littleHippoLegY - (littleHippoLegY * 0.3) duration: 0.1], [SKAction waitForDuration: 0.4], [SKAction moveToY: littleHippoLegY duration: 0.1]]]];
    [[littleHippo childNodeWithName: @"littleHead"] runAction: [SKAction moveTo: ccp(0, -1.5) duration: 0.1]];
    
    // Prep for next time
    if (rollDirection > 0)
        rollDirection = -winSize.width/2 * 0.7;
    else
        rollDirection = winSize.width/2 * 0.7;
    
    [self performSelector: @selector(littleRoll) withObject: nil afterDelay: [self randomTimeBetween: 2 and: 6]];
}

-(void)ninjaPeek
{
    float waitDuration = [self randomTimeBetween: 1 and: 5];
    float goDownDuration = [self randomTimeBetween: 0.2 and: 0.6];
    
    // Body actions
    SKAction *goDown = [SKAction moveToY: winSize.height - 75 duration: goDownDuration];
    SKAction *goUp = [SKAction moveToY: winSize.height - 40 duration: 0.2];
    SKAction *peek = [SKAction sequence: @[goDown, [SKAction waitForDuration: waitDuration], goUp]];
    
    // Head actions
    SKAction *goDownHead = [SKAction moveToY: 25 duration: goDownDuration];
    SKAction *goUpHead = [SKAction moveToY: 20 duration: 0.2];
    SKAction *peekHead = [SKAction sequence: @[goDownHead, [SKAction waitForDuration: waitDuration], goUpHead]];
    
    // Adjust actions for more point movement on iPad
    if (IS_IPAD) {
        // Body actions
        goDown = [SKAction moveToY: winSize.height - 230 duration: goDownDuration];
        goUp = [SKAction moveToY: winSize.height - 185 duration: 0.2];
        peek = [SKAction sequence: @[goDown, [SKAction waitForDuration: waitDuration], goUp]];
        
        // Head actions
        goDownHead = [SKAction moveToY: 45 duration: goDownDuration];
        goUpHead = [SKAction moveToY: 20 duration: 0.2];
        peekHead = [SKAction sequence: @[goDownHead, [SKAction waitForDuration: waitDuration], goUpHead]];
    }
    
    // Run the animations
    [ninjaHippo runAction: peek];
    [[ninjaHippo childNodeWithName: @"ninjaHead"] runAction: peekHead];
    
    // Repeat after a delay
    [self performSelector: @selector(ninjaPeek) withObject: nil afterDelay: [self randomTimeBetween: 6 and: 10]];
}

#pragma mark - Main Menu Buttons
-(void)playButtonPressed
{
    [self goToLevelSelect];
}

-(void)shopButtonPressed
{

}

-(void)gameCenterPressed
{
    // Start the gamecenter request
    [(JRBViewController *)[[[UIApplication sharedApplication] keyWindow] rootViewController] gameCenterPressed];
}

-(void)settingsButtonPressed
{

}

-(void)levelCreatorButtonPressed
{
    LevelCreator *creator = [LevelCreator sceneWithSize: winSize];
    [creator setScaleMode: SKSceneScaleModeAspectFit];
    [self.view presentScene: creator transition: [SKTransition fadeWithDuration: 0.8]];
}


#pragma mark - Game Center
-(void)gameCenterAvailable
{
    // Unhide and enable the game center button
    if (gameCenterButton.alpha == 0 && !gameCenterButton.enabled) {
        [gameCenterButton setEnabled: YES];
        // Only fade in if the other buttons aren't hidden
        if (!showingLevelSelect)
            [gameCenterButton setAlpha: 1];
    }
}

-(void)gameCenterNotAvailable
{
    // Hide and disable the game center button
    [gameCenterButton runAction: [SKAction fadeOutWithDuration: 0.2]];
    [gameCenterButton setEnabled: NO];
}

#pragma mark - Going To/From Level Select
-(void)goToLevelSelect
{
    showingLevelSelect = YES;
    
    // Get rid of the logo and buttons
    [logo setScale: 0.5];
    [logo setPosition: ccp(winSize.width/2, winSize.height - 35)];
    [playButton setAlpha: 0];
    [shopButton setAlpha: 0];
    [gameCenterButton setAlpha: 0];
    [settingsButton setAlpha: 0];
    [levelCreatorButton setAlpha: 0];
    
    if (IS_IPAD)
        [logo setPosition: ccp(winSize.width/2, winSize.height - 70)];
    
    // Remove the level buttons
    if (levelButtons) {
        [logo setAlpha: 1];
        for (SKSpriteNodeButton *button in levelButtons)
            [button removeFromParent];
        
        levelButtons = nil;
    }
    
    // Bring level select up front
    [scroller setZPosition: 2.5];
    [backButton setZPosition: 2.6];
    for (SKSpriteNode *dot in scroller.points)
        [dot setZPosition: 2.5];
}

-(void)goFromLevelSelect
{
    showingLevelSelect = NO;
    
    // Show the logo and buttons
    [logo setScale: 1];
    [logo setPosition: ccp(winSize.width/2, winSize.height - 60)];
    [playButton setAlpha: 1];
    [shopButton setAlpha: 1];
    [settingsButton setAlpha: 1];
    [levelCreatorButton setAlpha: 1];
    
    // Only show gamecenter button if we're authenticated
    if ([[GKLocalPlayer localPlayer] isAuthenticated]) {
        [gameCenterButton setAlpha: 1];
        [gameCenterButton setEnabled: YES];
    }
    
    if (IS_IPAD)
        [logo setPosition: ccp(winSize.width/2, winSize.height - 110)];
    
    // Send the dimmer and level select buttons to the back
    [scroller setZPosition: -10];
    [backButton setZPosition: -10];
    for (SKSpriteNode *dot in scroller.points)
        [dot setZPosition: -10];
}

#pragma mark - Level Select Buttons
-(void)levelPackButtonPressed:(id)sender
{
    // Hide the level group selection and logo
    [scroller setZPosition: -10];
    for (SKSpriteNode * dot in scroller.points)
        [dot setZPosition: -10];
    
    [logo setAlpha: 0];
    
    // Create arrays of x and y coordinates for the level buttons
    float initialY = winSize.height - winSize.height * 0.17;
    float spacingY = winSize.height * 0.2;
    NSArray *yCoords = @[[NSNumber numberWithFloat: initialY], [NSNumber numberWithFloat: initialY - spacingY], [NSNumber numberWithFloat: initialY - spacingY*2], [NSNumber numberWithFloat: initialY - spacingY*3]];
    
    float initialX = winSize.width * 0.18;
    float spacingX = winSize.width * 0.16;
    NSArray *xCoords = @[[NSNumber numberWithFloat: initialX], [NSNumber numberWithFloat: initialX + spacingX], [NSNumber numberWithFloat: initialX + spacingX*2], [NSNumber numberWithFloat: initialX + spacingX*3], [NSNumber numberWithFloat: initialX + spacingX*4]];
    
    // Initialize the buttons array
    levelButtons = [[NSMutableArray alloc] init];
    
    // To determine the level number
    int levelNumberIncrement = 1;
    if (sender == levels21_40)
        levelNumberIncrement = 20;
    else if (sender == levels41_60)
        levelNumberIncrement = 40;
    else if (sender == levels61_80)
        levelNumberIncrement = 60;
    
    // Create all 20 buttons
    for (int i = 0; i < 20; i++) {
        SKTexture *texture;
        if (i + levelNumberIncrement <= furthestLevelReached)
            texture = [SKTexture textureWithImageNamed: @"LevelButton.png"];
        else
            texture = [SKTexture textureWithImageNamed: @"LockedLevel.png"];
        
        // Make the button, add an action, and assign its number in the userData
        SKSpriteNodeButton *button = [SKSpriteNodeButton spriteNodeWithTexture: texture];
        [button setUserData: [NSMutableDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt: i + levelNumberIncrement], @"levelNumber", nil]];
        [button addTarget: self action: @selector(levelButtonPressed:)];
        [button setZPosition: 2.5];
        
        // Add a number label if it's unlocked
        SKLabelNode *levelNumberLabel;
        SKLabelNode *scoreTextLabel;
        SKLabelNode *scoreIntLabel;
        if (i + levelNumberIncrement <= furthestLevelReached) {
            int fontSize;
            int scoreTextFontSize;
            int scoreIntFontSize;
            
            CGPoint position;
            CGPoint scoreTextPosition;
            CGPoint scoreIntPosition;
            
            // Adjust font and position for ipad or iphone
            if (IS_IPAD) {
                fontSize = 60;
                position = ccp(0, -15);
                
                if ([[LevelInfoStore sharedStore] scoreForLevel: i + levelNumberIncrement] != 0) {
                    position = ccp(-3, 0);
                    scoreTextPosition = ccp(2, -20);
                    scoreIntPosition = ccp(0, -45);
                    
                    scoreTextFontSize = 20;
                    scoreIntFontSize = 30;
                }
            } else {
                fontSize = 30;
                position = ccp(0, -10);
                
                if ([[LevelInfoStore sharedStore] scoreForLevel: i + levelNumberIncrement] != 0) {
                    position = ccp(-2, 0);
                    scoreTextPosition = ccp(0, -10);
                    scoreIntPosition = ccp(0, -25);
                    
                    scoreTextFontSize = 10;
                    scoreIntFontSize = 15;
                }
            }
            
            // Level Number Label
            levelNumberLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
            [levelNumberLabel setFontColor: [UIColor whiteColor]];
            [levelNumberLabel setFontSize: fontSize];
            [levelNumberLabel setText: [NSString stringWithFormat: @"%i", i + levelNumberIncrement]];
            [levelNumberLabel setPosition: position];
            [button addChild: levelNumberLabel];
            
            if ([[LevelInfoStore sharedStore] scoreForLevel: i + levelNumberIncrement] != 0) {
                // Score Text Label
                scoreTextLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
                [scoreTextLabel setFontColor: [UIColor whiteColor]];
                [scoreTextLabel setFontSize: scoreTextFontSize];
                [scoreTextLabel setText: @"Score:"];
                [scoreTextLabel setPosition: scoreTextPosition];
                [button addChild: scoreTextLabel];

                // Score Int Label
                scoreIntLabel = [SKLabelNode labelNodeWithFontNamed: @"AvenirNextCondensed-Bold"];
                [scoreIntLabel setFontColor: [UIColor whiteColor]];
                [scoreIntLabel setFontSize: scoreIntFontSize];
                [scoreIntLabel setText: [NSString stringWithFormat: @"%i", [[LevelInfoStore sharedStore] scoreForLevel: i + levelNumberIncrement]]];
                [scoreIntLabel setPosition: scoreIntPosition];
                [button addChild: scoreIntLabel];
            }
        }
        
        // Disable the locked level buttons
        if (i + levelNumberIncrement > furthestLevelReached)
            [button setEnabled: NO];
        
        if (i < 5)
            [button setPosition: ccp([[xCoords objectAtIndex: i] floatValue], [[yCoords objectAtIndex: 0] floatValue])];
        else if (i > 4 && i < 10)
            [button setPosition: ccp([[xCoords objectAtIndex: i - 5] floatValue], [[yCoords objectAtIndex: 1] floatValue])];
        else if (i > 9 && i < 15)
            [button setPosition: ccp([[xCoords objectAtIndex: i - 10] floatValue], [[yCoords objectAtIndex: 2] floatValue])];
        else if (i > 14 && i < 20)
            [button setPosition: ccp([[xCoords objectAtIndex: i - 15] floatValue], [[yCoords objectAtIndex: 3] floatValue])];
        
        [self addChild: button];
        [levelButtons addObject: button];
    }
}

-(void)levelButtonPressed:(id)sender
{
    int levelNumber = [[[(SKSpriteNodeButton *)sender userData] objectForKey: @"levelNumber"] intValue];
    
    SKScene *levelScene;
    if (levelNumber == 1)
        levelScene = [Level1 sceneWithSize: winSize];
    else if (levelNumber == 2)
        levelScene = [Level2 sceneWithSize: winSize];
    
    [levelScene setScaleMode: SKSceneScaleModeAspectFit];
    [[self view] presentScene: levelScene transition: [SKTransition fadeWithDuration: 0.8]];
}

-(void)backButtonPressed
{
    if (scroller.zPosition != -10)
        [self goFromLevelSelect];
    else
        [self goToLevelSelect];
}

#pragma mark - Other
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)dealloc
{
    NSLog(@"deallocating main menu");
}

@end
