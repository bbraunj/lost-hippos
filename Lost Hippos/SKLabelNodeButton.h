//
//  SKLabelNodeButton.h
//  Lost Hippos
//
//  Created by Josh Braun on 10/18/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKLabelNodeButton : SKLabelNode
{
    NSMutableArray *targetActions;
    float originalXScale, originalYScale;
    float originalColorBlendFactor;

    id delegate;
    BOOL sendTouchEvents;
}

@property (nonatomic) UITouch *touch;
@property (readonly) BOOL selected;
@property (assign) BOOL enabled;
@property (assign) BOOL zoomOnTouchDown;
@property (assign) BOOL darkenOnTouchDown;

// If you want to send the parent touch events, put the delegate as nil
-(void)sendDelegate:(id)theDelegate touchEvents:(BOOL)sendEvents;
-(void)stopSendingDelegateTouchEvents;

-(void)addTarget:(id)target action:(SEL)action;
-(void)removeTarget:(id)target action:(SEL)action;

-(void)addBlock: (void (^)(void))block;
-(void)removeBlocks;

@end
