//
//  LevelInfoStore.h
//  Lost Hippos
//
//  Created by Josh Braun on 9/29/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelInfoStore : NSObject 
{
    NSMutableDictionary *scores;
}
@property (nonatomic) int furthestLevelReached;

+(LevelInfoStore *)sharedStore;

-(void)setScore:(int)score forLevel:(int)levelNumber;
-(int)scoreForLevel:(int)levelNumber;

// Loading/saving
-(void)saveLevelInfo;
-(void)loadLevelInfo;
@end
