//
//  GCHelper.m
//  Lost Hippos
//
//  Created by Josh Braun on 8/8/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import "GCHelper.h"
#import "MainMenu.h"

@implementation GCHelper
@synthesize gameCenterAvailable, view;

#pragma mark Initialization

static GCHelper *sharedHelper = nil;
+ (GCHelper *) sharedHelper {
    if (!sharedHelper) {
        sharedHelper = [[GCHelper alloc] init];
    }
    return sharedHelper;
}

- (id)init {
    if ((self = [super init])) {
        NSNotificationCenter *nc =
            [NSNotificationCenter defaultCenter];
            [nc addObserver:self
                   selector:@selector(authenticationChanged)
                       name:GKPlayerAuthenticationDidChangeNotificationName
                     object:nil];
    }
    return self;
}

- (void)authenticationChanged {
    
    if ([GKLocalPlayer localPlayer].isAuthenticated && !userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        userAuthenticated = TRUE;
        
        // Let the main menu know we're authenticated
        if ([[view scene] isKindOfClass: [MainMenu class]])
            [(MainMenu *)[view scene] gameCenterAvailable];
    } else if (![GKLocalPlayer localPlayer].isAuthenticated && userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated");
        userAuthenticated = FALSE;
        
        // Let the main menu know we aren't authenticated
        if ([[view scene] isKindOfClass: [MainMenu class]])
            [(MainMenu *)[view scene] gameCenterNotAvailable];
    }
    
}

-(void)authenticateLocalUser
{
    NSLog(@"Authenticating local user...");
    if ([GKLocalPlayer localPlayer].authenticated == NO) {
        [[GKLocalPlayer localPlayer] setAuthenticateHandler: ^void(UIViewController *vc, NSError *err) {
            if (vc) {
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController: vc animated: YES completion: nil];
            } else if (err) {
                NSLog(@"%@", [err localizedDescription]);
                
                // Authentication failed.
                // Let the user know
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Game Center Disabled" message: @"You will not be able to use Game Center features." delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
                [alert show];
                
                // Let the main menu know
                if ([[view scene] isKindOfClass: [MainMenu class]])
                    [(MainMenu *)[view scene] gameCenterNotAvailable];
            }
        }];
    } else {
        NSLog(@"Already authenticated!");
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        // Ok -- dismiss
        [alertView dismissWithClickedButtonIndex: buttonIndex animated: YES];
}
@end
