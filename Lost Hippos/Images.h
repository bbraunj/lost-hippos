// ---------------------------------------
// Sprite definitions for Images
// Generated with TexturePacker 3.2.1
//
// http://www.codeandweb.com/texturepacker
// ---------------------------------------

#ifndef __IMAGES_ATLAS__
#define __IMAGES_ATLAS__

// ------------------------
// name of the atlas bundle
// ------------------------
#define IMAGES_ATLAS_NAME @"Images"

// ------------
// sprite names
// ------------

// --------
// textures
// --------

// ----------
// animations
// ----------

#endif // __IMAGES_ATLAS__
