// ---------------------------------------
// Sprite definitions for Level Creator
// Generated with TexturePacker 3.2.1
//
// http://www.codeandweb.com/texturepacker
// ---------------------------------------

#ifndef __LEVEL_CREATOR_ATLAS__
#define __LEVEL_CREATOR_ATLAS__

// ------------------------
// name of the atlas bundle
// ------------------------
#define LEVEL_CREATOR_ATLAS_NAME @"Level Creator"

// ------------
// sprite names
// ------------

// --------
// textures
// --------

// ----------
// animations
// ----------

#endif // __LEVEL_CREATOR_ATLAS__
