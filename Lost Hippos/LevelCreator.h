//
//  LevelCreator.h
//  Lost Hippos
//
//  Created by Josh Braun on 10/14/13.
//  Copyright (c) 2013 The Supremacy. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SKScrollLayer.h"

@class SKSpriteNodeButton;

typedef enum {
    DropTab,
    MoveableTab,
    StationaryTab,
    BackgroundTab,
    PlaySaveTab
} TabType;

@interface LevelCreator : SKScene <SKScrollLayerDelegate>
{
    CGSize winSize;
    
    SKView *skView;
    
    SKTextureAtlas *atlas;
    SKTextureAtlas *atlas2;
    
    // Physics ground y coordinate
    int groundY;
    
    // Hippos
    NSMutableArray *littleHippos;
    SKSpriteNodeButton *mamaHippo;
    
    // Level Objects
    NSMutableArray *dropObjects;
    NSMutableArray *stationaryObjects;
    NSMutableArray *moveableObjects;
    
    // Background(s) [may be multiple layers of background]
    NSMutableArray *backgrounds;
    
    // Dimmer for pause menu
    SKShapeNode *dimmer;
    
    // Buttons
    SKSpriteNodeButton *optionsButton;
    SKSpriteNodeButton *addObjectButton;
    SKSpriteNodeButton *pauseButton;
    SKSpriteNodeButton *dropQueue;
    
    // Drop Object Editor
    SKSpriteNode *dropObjectEditor;
    
    // Popover and Tabs
    SKSpriteNode *popoverArrow, *popoverBody;
    SKSpriteNodeButton *backgroundTab, *playSaveTab, *dropTab, *moveableTab, *stationaryTab;
    TabType lastOptionsTab, lastObjectTab;
    int lastDropCenterPage, lastBackgroundCenterPage, lastMoveableCenterPage, lastStationaryCenterPage;
    
    SKSpriteNodeButton *selectedTab;
    SKSpriteNodeButton *selectedDropObject; // In drop object editor
    SKNode *selectedSceneObject;
    
    BOOL movingObjectIntersecting; // If the moving object's intersecting another object
    NSMutableArray *movingSceneObjects;
    
    UITouch *firstTouch, *secondTouch;
    int numberOfTouchesOnScreen;
    
    // Scaling objects
    float scaleLevel;
    float startingDistanceBetweenTouches;
    BOOL objectsNeedPhysicsBodies; // physics bodies are cleared while scaling
    BOOL hipposNeedPositionAdjustments;
    
    // Rotating scene objects
    BOOL rotatingSceneObject;
    CGPoint initialVector; // the vector at which the touch started
    float initialZRotation; // the rotation of the node when the touch started
    SKLabelNode *rotationLabel;
    
    // For selecting multiple objects
    CGPoint touchStart;
    SKShapeNode *selectionRect;
}


#pragma mark - Pause Menu
-(void)pauseButtonPressed;
-(void)returnToLevel;
-(void)showConfirmationWithText:(NSString *)text yesBlock:(void(^)(void))block;

#pragma mark - Scrollers
-(void)saveScrollersCenterPages;

#pragma mark - Popovers
-(void)hideShowPopoverOnButton:(SKSpriteNodeButton *)button;

#pragma mark - Add Objects
-(void)addObjectsViewsToPopoverBody;

#pragma mark - - Drop Objects Tab
-(void)dropObjectsTabPressed;
-(void)hideShowDropObjectsViews;
-(void)addObjectToDropQueue:(SKSpriteNodeButton *)object;
-(void)updateDropQueue;

#pragma mark - - Moveable Objects Tab
-(void)moveableTabPressed;
-(void)hideShowMoveableObjectsViews;
-(void)addObjectToScene:(SKSpriteNodeButton *)object;

#pragma mark - - Stationary Objects Tab
-(void)stationaryTabPressed;
-(void)hideShowStationaryObjectViews;

#pragma mark - Options Menu
-(void)addOptionsViewsToPopoverBody;

#pragma mark - - Background Tab
-(void)backgroundTabPressed;
-(void)hideShowBackgroundViews;
-(void)backgroundImageTapped:(SKSpriteNodeButton *)image;
-(void)changeBackgroundZPosition:(SKSpriteNodeButton *)button;
-(void)addOrRemoveBackground:(SKSpriteNodeButton *)image;

#pragma mark - - Play/Save Tab
-(void)playSaveTabPressed;
-(void)hideShowPlaySaveViews;
-(void)playLevel;
-(void)showSaveLevelMenu;
-(void)saveLevel;

#pragma mark - Edit Drop Objects
-(void)editDropQueue;
-(void)editDropObject:(SKSpriteNodeButton *)dropObject;

#pragma mark - Edit Scene Objects
-(void)sceneObjectTapped:(SKSpriteNodeButton *)sender;
-(void)showRotateTools;
-(void)showPrecisionMovingTools;
-(void)deleteSceneObject;
-(void)makeObjectStationaryOrMoveable:(SKSpriteNodeButton *)button;
-(void)arrowPressed:(SKSpriteNodeButton *)sender;

#pragma mark - Other
-(void)scaleObjectsWithScaleDifference:(float)scaleDifference;
-(void)giveObjectsPhysicsBodies;
@end
